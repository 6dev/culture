<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", 0, LANGUAGE_ID);
$userProp = array();
if (!empty($arRes))
{
	foreach ($arRes as $key => $val)
		$userProp[$val["FIELD_NAME"]] = ($val["EDIT_FORM_LABEL"] <> '' ? $val["EDIT_FORM_LABEL"] : $val["FIELD_NAME"]);
}
$arComponentParameters = array(
	"PARAMETERS" => array(
			"BLOCKS_SHOW" =>  array(
            "PARENT"    =>  "LIST",
            "NAME"      =>  "Блоки для публичной версии страницы профиля",
            "TYPE"      =>  "LIST",
            "VALUES"    =>  array(
                "7"     =>  "Достижения",
                "8"     =>  "Посещенные места",
				"9"     =>  "Маршруты",
				"10"     =>  "Культурные памятники",
				
            ),
            "MULTIPLE"  =>  "Y",
        ),
	
		"BLOCKS_FOR_PUBLIC" =>  array(
            "PARENT"    =>  "LIST",
            "NAME"      =>  "Блоки для публичной версии страницы профиля",
            "TYPE"      =>  "LIST",
            "VALUES"    =>  array(
			"1"     =>  "Фото и логин",
			"2"     =>  "Рейтинг",
			"3"     =>  "Мест посещено",
			"4"     =>  "Комментариев оставлено",
			"5"     =>  "Вопросов задано наставнику",
                "7"     =>  "Достижения",
                "8"     =>  "Посещенные места",
				"9"     =>  "Маршруты",
				"10"     =>  "Культурные памятники",
				"11"     =>  "Ближайшие мероприятия",
				"12"     =>  "Избранное",
				"13"     =>  "Стена",
				
            ),
            "MULTIPLE"  =>  "Y",
        ),
	
	
	
	
		"SET_TITLE" => array(),
		"USER_PROPERTY"=>array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("USER_PROPERTY"),
			"TYPE" => "LIST",
			"VALUES" => $userProp,
			"MULTIPLE" => "Y",
			"DEFAULT" => array(),
		),
		"SEND_INFO"=>array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("SEND_INFO"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		"CHECK_RIGHTS"=>array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("CHECK_RIGHTS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		),
		
	
		
		
	),
);
?>