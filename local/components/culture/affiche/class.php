<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;
use Custom\Culture\CultureHelpers;

Loader::includeModule('iblock');

class Affiche extends CBitrixComponent
{

    private $params = array();

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "FILTER" => $arParams["FILTER"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
        );
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        if ($this->params['TYPE'] == 'LIST') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getList();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        if ($this->params['TYPE'] == 'DETAIL') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getDetail();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        return null;
    }


    public function getDetail() {
        $fields = array();
        $query = \Bitrix\Iblock\ElementTable::query();
        $query->registerRuntimeField('ELEMENT', [
                'data_type' => 'Bitrix\Iblock\ElementTable',
                'reference' => [
                    '=this.CODE' => 'ref.CODE',
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ID' => 'ref.IBLOCK_ID',
                ],
            ]
        );
        $f = array('IBLOCK_ID' => 21);
        if (!empty($this->params['CODE'])) $f['CODE'] = $this->params['CODE'];
        else $f['ID'] = (!empty($this->params['ID']))? (int)$this->params['ID'] : 0;
        $query->setFilter($f);
        $query->setSelect([
            'ID',
            'DETAIL_TEXT',
            'PREVIEW_TEXT',
            'NAME',
            'CODE',
            'DETAIL_PICTURE',
            'PREVIEW_PICTURE',
            'ACTIVE_FROM'
        ]);
        $db_res = $query->exec();
        if ($dbItem = $db_res->fetch()) {
            global $APPLICATION;
            Custom\Culture\CultureHelpers::setPageTitle($dbItem['NAME']);
            $APPLICATION->SetPageProperty("og:type", "article");

            $APPLICATION->SetPageProperty("og:url", SITE_BASE_URL.'/affiche/detail/'.$dbItem['CODE'].'/');
            $APPLICATION->SetPageProperty("og:title", $dbItem['NAME']);
            $APPLICATION->SetPageProperty("og:description", $dbItem['PREVIEW_TEXT']);
            $fields['ELEMENT'] = $dbItem;
            //$fields['ELEMENT']['PUB_DATE'] = (!empty($dbItem['ACTIVE_FROM']))? $dbItem['ACTIVE_FROM']->toString(new \Bitrix\Main\Context\Culture(array())) : '';
            $fields['ELEMENT']['IMAGE_SRC'] = (!empty($dbItem['DETAIL_PICTURE']))? CFile::GetPath((int)$dbItem['DETAIL_PICTURE']) : '';
            if (($fields['ELEMENT']['IMAGE_SRC'] == '') && !empty($dbItem['PREVIEW_PICTURE'])) $fields['ELEMENT']['IMAGE_SRC'] = CFile::GetPath((int)$dbItem['PREVIEW_PICTURE']);
            $APPLICATION->SetPageProperty("og:image", SITE_BASE_URL.$fields['ELEMENT']['IMAGE_SRC']);
            $queryProps = \Bitrix\Iblock\ElementPropertyTable::query();
            $queryProps->registerRuntimeField('PROPERTIES', array(
                "data_type" => '\Bitrix\Iblock\ElementPropertyTable',
                'reference' => array(
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ELEMENT_ID' => 'ref.IBLOCK_ELEMENT_ID'
                )
            ));

            $queryProps->registerRuntimeField('PROPERTIES_CODE', array(
                "data_type" => '\Bitrix\Iblock\PropertyTable',
                'reference' => array('=this.PROPERTIES.IBLOCK_PROPERTY_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            ));

            $queryProps->registerRuntimeField('PROPERTIES_BIND_ELEMS', [
                    'data_type' => 'Bitrix\Iblock\ElementTable',
                    'reference' => [
                        '=this.PROPERTIES.VALUE' => 'ref.ID',
                    ],
                    'join_type' => "LEFT"
                ]
            );

            $queryProps->registerRuntimeField('PROPERTIES_ENUM', [
                    'data_type' => 'Bitrix\Iblock\PropertyEnumerationTable',
                    'reference' => [
                        '=this.PROPERTIES.VALUE_ENUM' => 'ref.ID',
                    ],
                    'join_type' => "LEFT"
                ]
            );

            $queryProps->setFilter([
                'IBLOCK_ELEMENT_ID' => $dbItem['ID']
            ]);
            $queryProps->setSelect([
                'VALUE' => 'PROPERTIES.VALUE',
                'VALUE_TYPE' => 'PROPERTIES.VALUE_TYPE',
                'ENUM_' => 'PROPERTIES_ENUM',
                'VALUE_NUM' => 'PROPERTIES.VALUE_NUM',
                'VALUE_BIND_ELEMS_ID' => 'PROPERTIES_BIND_ELEMS.ID',
                'VALUE_BIND_ELEMS_IBLOCK_ID' => 'PROPERTIES_BIND_ELEMS.IBLOCK_ID',
                'VALUE_BIND_ELEMS' => 'PROPERTIES_BIND_ELEMS.NAME',
                'CODE' => 'PROPERTIES_CODE.CODE'
            ]);
            $db_res = $queryProps->exec();
            $fields['PROPERTIES'] = array();
            $ctypes = array();
            while ($dbItem2 = $db_res->fetch()) {
                if ($dbItem2['CODE'] == 'CULTURE_TYPES') {
                    $citem = array();
                    $tab_color = CIBlockElement::GetProperty($dbItem2['VALUE_BIND_ELEMS_IBLOCK_ID'], $dbItem2['VALUE_BIND_ELEMS_ID'], array(), Array("CODE"=>"TAB_COLOR"));
                    if ($tab_color_arr = $tab_color->Fetch()) {
                        $citem['TAB_COLOR'] = $tab_color_arr['VALUE'];
                    }
                    $citem['VALUE'] = $dbItem2['VALUE_BIND_ELEMS'];
                    $ctypes[] = $citem;
                }
                else $fields['PROPERTIES'][$dbItem2['CODE']] = $dbItem2;
            }
            $fields['PROPERTIES']['CULTURE_TYPES'] = $ctypes;
        }
        $fields['IS_USER_AUTH'] = $this->params['IS_USER_AUTH'];
        return $fields;
    }


    public function getList() {
        Bitrix\Main\Page\Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/affiche.js");
        $nPageSize = (int)$this->params['nPageSize'];
        $arFilter = (!empty($this->params['FILTER']))? $this->params['FILTER'] : array();
        $page = (!empty($this->params['PAGE']))? (int)$this->params['PAGE'] : 1;
        $sort = (!empty($this->params['SORT']))? $this->params['SORT'] : array();
        $res = CIBlockElement::GetList($sort, $arFilter, false, Array("nPageSize"=>$nPageSize, "iNumPage" =>$page), Array("*", "PROPERTY_*"));
        $items = array();
        $fields = array();


        $res->NavStart();
        $fields['ITEMS_COUNT'] =  $res->NavRecordCount;
        $fields['PAGE_COUNT'] =  $fields['ITEMS_COUNT'] / $nPageSize;
        if ($fields['PAGE_COUNT'] < 1) $fields['PAGE_COUNT'] = 1;
        $fields['CURRENT_PAGE'] =  $page;

        while($ob = $res->GetNextElement()){
            $item = array();
            $item['FIELDS'] = $ob->GetFields();
            $item['PROPERTIES'] = $ob->GetProperties();
            $items[] = $item;
        }
        $fields['ITEMS'] = $items;
        $fields['FILTERS'] = $this->getFilters();
        return $fields;
    }



    private function getFilters() {
        $filters['city'] = CultureHelpers::getCities();
        $filters['interests'] = CultureHelpers::getCultureCategoryList();
        return $filters;
    }


}?>