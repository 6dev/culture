<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="poster-popup--content">
    <span class="close"></span>
    <div class="top-img">
        <img src="<?=$arResult['fields']['ELEMENT']['IMAGE_SRC']?>" alt="img" />

        <?foreach ($arResult['fields']['PROPERTIES']['CULTURE_TYPES'] as $item):?>
            <span class="label" style="background: <?=(!empty($item['TAB_COLOR']))? '#'.$item['TAB_COLOR'] : 'red'?>">
                <?=$item['VALUE']?>
            </span>
        <?endforeach;?>


    </div>
    <div class="content-info--popup">
          <span class="title">
            <?=$arResult['fields']['ELEMENT']['NAME']?>
          </span>
        <div class="payment--info">
            <?if ((double)$arResult['fields']['PROPERTIES']['PRICE']['VALUE'] > 0):?>
                <p>Платно</p>
                <span item_id="<?=$arResult['fields']['ELEMENT']['ID']?>" id="buy_ticket">
                    Купить билет
                </span>
            <?else:?>
                <p class="free-button">Бесплатно</p>
            <?endif;?>
        </div>
        <div class="info-post">
            <span class="location">
              г. <?=$arResult['fields']['PROPERTIES']['CITY']['VALUE']?>, <?=$arResult['fields']['PROPERTIES']['ADDRESS']['VALUE']?>
            </span>
            <span class="time">
              <?=FormatDate("d F Y", new \Bitrix\Main\Type\DateTime($arResult['fields']['PROPERTIES']['START_EVENT']['VALUE'], "Y-m-d H:i:s"))?>
            </span>
            <span class="age">
                <?if (!empty($arResult['fields']['PROPERTIES']['AGE_MIN'])):?>
                <?print_r($arResult['fields']['PROPERTIES']['AGE_MIN']['ENUM_VALUE'])?>+
                <?endif;?>
            </span>
            <?if ($arResult['fields']['IS_USER_AUTH']):?>
                <span item_id="<?=$arResult['fields']['ELEMENT']['ID']?>" id="agreement_btn" class="agreement">
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="17" viewBox="0 0 10 17" fill="none">
                    <path
                        d="M5.76923 3.16279C6.61538 3.16279 7.30769 2.45116 7.30769 1.5814C7.30769 0.711628 6.61538 0 5.76923 0C4.92308 0 4.23077 0.711628 4.23077 1.5814C4.23077 2.45116 4.92308 3.16279 5.76923 3.16279ZM2.92308 5.85116L0.769231 17H2.38462L3.76923 10.6744L5.38462 12.2558V17H6.92308V11.0698L5.30769 9.48837L5.76923 7.11628C6.76923 8.30233 8.30769 9.09302 10 9.09302V7.51163C8.53846 7.51163 7.30769 6.72093 6.69231 5.61395L5.92308 4.34884C5.61538 3.87442 5.15385 3.55814 4.61538 3.55814C4.38462 3.55814 4.23077 3.63721 4 3.63721L0 5.37674V9.09302H1.53846V6.40465L2.92308 5.85116Z"
                        fill="#BDBDBD"
                    />
                    </svg>
                </span>
            <?endif;?>
        </div>
        <div class="description">
            <p>
                <?=$arResult['fields']['ELEMENT']['PREVIEW_TEXT']?>
            </p>
        </div>
        <div class="footer-popup">
            <?if ($arResult['fields']['IS_USER_AUTH']):?>
                <span class="bookmark--news">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px">
                    <path d="M0 0h24v24H0V0z" fill="none"></path>
                    <path
                        fill="#BDBDBD"
                        d="M19 18l2 1V3c0-1.1-.9-2-2-2H8.99C7.89 1 7 1.9 7 3h10c1.1 0 2 .9 2 2v13zM15 5H5c-1.1 0-2 .9-2 2v16l7-3 7 3V7c0-1.1-.9-2-2-2z"
                    ></path>
                    </svg>
                </span>
            <?else:?>
                <div></div>
            <?endif;?>
            <div class="share">
                <p><i class="material-icons">reply</i>Поделиться</p>
                <div class="social-links">
                    <?$APPLICATION->IncludeComponent(
                        "culture:share",
                        "buttons",
                        Array(
                            'ID' => $arResult['fields']['ELEMENT']['ID'],
                            'PAGE_URL' => SITE_BASE_URL.'/affiche/detail/'.$arResult['fields']['ELEMENT']['CODE'].'/'
                        )
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>

