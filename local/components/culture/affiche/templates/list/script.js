$( document ).ready(function() {

    let afficheList = null;

    function getLimit() {
        return 10;
    }

    function getRequestFields() {
        let formData = new FormData(document.getElementById("search_filter"));
        let data = {action: 'list', offset: 1, limit: getLimit()};
        for (let entry of formData.entries()) data[entry[0]] = entry[1];
        return data;
    }

    function listRequest(initData) {
        this.state = {send: 0, clearHtml: 0, page_count: 1};
        this.param = {
            data: initData
        };
        this.callback_success;
        this.callback_error;
        this.request = function (param, state) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: '/affiche/ajax.php',
                    dataType: "json",
                    data: param.data,
                    success: (function (response) {
                        console.log(response);
                        if (response.hasOwnProperty('data') && response.data.hasOwnProperty('list_html')) {
                            if (response.data.hasOwnProperty('page_count')) {
                                state.page_count = parseInt(response.data.page_count, 10);
                            }
                            let h = $("#affiche_list").html();
                            if (state.clearHtml == 1) h = response.data.list_html;
                            else h += response.data.list_html;

                            if (!$('.not-found').hasClass('is-active')) {
                                if (h == '') $('.not-found').addClass('is-active');
                            }
                            else {
                                if (h != '') $('.not-found').removeClass('is-active');
                            }

                            $("#affiche_list").html(h);
                            if ((param.data.offset == state.page_count) || (state.page_count == 0))  {
                                $("#more_btn").hide();
                            }
                            else $("#more_btn").show();
                            param.data.offset++;
                        }
                        state.clearHtml = 0;
                        state.send = 0;
                    }),
                    error: (function (error) {
                        state.send = 0;
                        state.clearHtml = 0;
                        reject(error);
                    })
                });
            });
        };
        this.getList = function (param) {
            if (this.state.send == 0) {
                this.state.send = 1;
                if (param.hasOwnProperty('offset')) this.param.data.offset = param.offset;
                if (param.hasOwnProperty('clearHtml')) this.state.clearHtml = param.clearHtml;
                this.request(this.param, this.state)
                    .then(this.callback_success,this.callback_error);
            }
            else console.log('send proccess');
        };
    };

    function updateList() {
        $("#more_btn").hide();
        afficheList = new listRequest(getRequestFields());
        afficheList.getList({offset: 1, clearHtml: 1});
    }

    $('#search_filter').on('submit', function() {
        updateList();
        return false;
    });


    $(".interest_select").on("click", function() {
        let data = {};
        data['UF_INTERESTS'] = [];
        data['UF_INTERESTS'].push(parseInt($(this).attr('value'), 10));
        let h = '';
        for (let item of data['UF_INTERESTS']) {
            h += '<input type="hidden" name="CTYPES[]" value="' + item + '">';
        }
        $("#hidden_filters").html(h);
        updateList();
    });

    $('#city_select').on('change', function() {
        $("input[name='CITY']").val($(this).val());
        updateList();
    });

    $("body").on("click", "#more_btn", function() {
        if (afficheList) afficheList.getList({});
    });

    updateList();

});