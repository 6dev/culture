<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<main class="main poster-pages">
    <div class="title-pages">
        <h2>Культурная афиша</h2>
    </div>
    <div class="container">
        <div class="poster-pages--content">
            <div class="filter-box">
                <div class="city-select">
                    <p>
                        Город
                    </p>
                    <select id="city_select">
                        <option value="" selected>Все города</option>
                        <?foreach($arResult["fields"]['FILTERS']['city'] as $item):?>
                            <option value="<?=$item['NAME']?>"<?if (!empty($_REQUEST['CITY']) && ($_REQUEST['CITY'] == $item['NAME'])):?> selected<?endif;?>><?=$item['NAME']?></option>
                        <?endforeach;?>
                    </select>
                </div>
                <form id="search_filter">
                    <input type="search" name="search" value="<?=(!empty($_REQUEST['search']))? $_REQUEST['search'] : ''?>" placeholder="Поиск по мероприятиям" />
                    <input type="hidden" name="CITY" value="<?=(!empty($_REQUEST['CITY']))? $_REQUEST['CITY'] : ''?>">
                    <div id="hidden_filters">
                        <?if(!empty($_REQUEST['CTYPES'])):?>
                            <?foreach($_REQUEST['CTYPES'] as $item):?>
                                <input type="hidden" name="CTYPES[]" value="<?=$item?>">
                            <?endforeach;?>
                        <?endif;?>
                     </div>
                </form>
            </div>
            <div class="poster-pages-tab">
                <div class="poster-pages--tabs-menu">
                    <ul>
                        <?foreach($arResult["fields"]['FILTERS']['interests'] as $item):?>
                            <?if (!empty($_REQUEST['CTYPES']) && in_array($item['ID'], $_REQUEST['CTYPES'])):?>
                                <li class="interest_select is-active" value="<?=$item['ID']?>"><?=$item['NAME']?></li>
                            <?else:?>
                                <li class="interest_select" value="<?=$item['ID']?>"><?=$item['NAME']?></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </div>
                <div class="tab-content">
                    <ul id="affiche_list" style="display: flex;">

                    </ul>
                    <p id="more_btn" class="news__load">
                        Загрузить ещё
                        <i class="material-icons arrow_downward__icon">arrow_downward</i>
                    </p>

                    <div class="not-found">
                        К сожалению, нет ближайших мероприятий по выбранной теме
                    </div>


                </div>
            </div>
        </div>
    </div>
</main>