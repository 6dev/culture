<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult['fields']['ITEMS'] as $item):?>
    <li>
        <a class="affiche_item" item_id="<?=$item['FIELDS']['ID']?>" href="javascript:void(0);" lnk="/affiche/detail/<?=$item['FIELDS']['CODE']?>/">
                                    <span class="date-poster">
                                        <?=FormatDate("d F", MakeTimeStamp($item['PROPERTIES']['START_EVENT']['VALUE']))?>
                                    </span>
            <div class="img-box">
                <?
                $imgId = (!empty($item['FIELDS']['PREVIEW_PICTURE']))? (int)$item['FIELDS']['PREVIEW_PICTURE'] : (int)$item['FIELDS']['DETAIL_PICTURE'];
                $imgSrc = ($imgId > 0)? CFile::GetPath($imgId) : '';
                ?>
                <img src="<?=$imgSrc?>" alt="poster" />
            </div>
            <p><?=$item['FIELDS']['NAME']?></p>
        </a>
    </li>
<?endforeach;?>