<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult['fields']['ITEMS'] as $item):?>
    <a href="javascript:void(0);" item_id="<?=$item['FIELDS']['ID']?>" class="swiper-slide affiche_item">
        <div class="img-box">
            <span class="date-post"><?=FormatDate("d F", MakeTimeStamp($item['PROPERTIES']['START_EVENT']['VALUE']))?></span>
            <?
            $imgId = (!empty($item['FIELDS']['PREVIEW_PICTURE']))? (int)$item['FIELDS']['PREVIEW_PICTURE'] : (int)$item['FIELDS']['DETAIL_PICTURE'];
            $imgSrc = ($imgId > 0)? CFile::GetPath($imgId) : '';
            ?>
            <img src="<?=$imgSrc?>" alt="" />
        </div>
        <p>
            <?=$item['FIELDS']['NAME']?>
        </p>
        <span class="city">
            <?if(!empty($item['PROPERTIES']['CITY']['VALUE'])):?>
                г. <?=$item['PROPERTIES']['CITY']['VALUE']?>
            <?endif;?>
              </span>
    </a>
<?endforeach;?>