<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<? if($arResult['SUCCESS'] == 'Y') : ?>
    <main class="main password-changed">
      <div class="container">
        <div class="password-changed--content">
          <h2>Изменить пароль</h2>
          <p>Ваш пароль успешно изменен</p>
          <a href="/personal/settings/">Вернуться к настройкам</a>
        </div>
      </div>
    </main>
<? else : ?> 
    <main class="main change-password">
      <div class="container">
        <div class="change-password--content"> 
          <h2>Изменить пароль</h2>

          	<?if($arResult['ERROR']){?>
				<span class="error"><?=$arResult['ERROR'];?></span>
			<?}?>

          <form action="" method="post">
          	<input type="hidden" name="do" value="send" />
            <div class="input-container">
              <div class="top-box">
                <label data-text="Старый пароль">
                  <input required placeholder=" Введите старый пароль" name="old_password" value="<?=$_REQUEST['old_password'];?>" type="password" />
                </label>
                <label data-text="Новый пароль">
                  <input required placeholder=" Придумайте новый пароль" value="<?=$_REQUEST['password'];?>" name="password" type="password" />
                </label>
              </div>
              <p>
                - минимум 6 символов
              </p>
              <p>- только латиница</p>
              <div class="bottom-box">
                <label data-text="Повторите пароль">
                  <input required placeholder=" Повторите новый пароль" value="<?=$_REQUEST['confirm_password'];?>" name="confirm_password"  type="password" />
                </label>
              </div>

              <span class="explanation-filling">* — поля, обязательные для заполнения</span>
            </div>
            <button type="submit">Сохранить изменения</button>
          </form>
        </div>
      </div>
    </main>
<? endif; ?>


