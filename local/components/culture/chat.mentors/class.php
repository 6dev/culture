<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Custom\Culture\CultureHelpers;
use Custom\Chat\MainChat;
global $USER;


class ChatMentors extends CBitrixComponent
{

    const CHAT_MODULE_NAME = 'mentors';

    private $params = array();

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "FILTER" => $arParams["FILTER"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
        );
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        $this->arResult["fields"] = array();
        if($this->startResultCache())
        {
            $this->arResult["fields"] = $this->getList();
            $this->includeComponentTemplate();
        }
        return $this->arResult["fields"];
    }


    public function getList() {
        $param = array();
        $filter = (!empty($this->params['FILTER']))? $this->params['FILTER'] : array();
        $param['filter'] = array(
            'MODULE' => self::CHAT_MODULE_NAME,
            $filter,
        );
        $param['order'] = (!empty($this->params['ORDER']))? $this->params['ORDER'] : array();
        if (!empty($this->params['OFFSET'])) $param['offset'] = (int)$this->params['OFFSET'];
        if (!empty($this->params['LIMIT'])) $param['limit'] = (int)$this->params['LIMIT'];
        $param['answer'] = true;
        $items = MainChat::getMessages($param)['LIST'];
        return array('ITEMS' => $items, 'MENTOR_ID' => $this->params['MENTOR_ID']);
    }


}?>