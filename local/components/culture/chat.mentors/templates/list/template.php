<?foreach($arResult['fields']['ITEMS'] as $item):?>

    <?if (((int)$item['SRC_USER_ID'] > 0) && ((int)$item['SRC_USER_ID'] == $arResult['fields']['MENTOR_ID'])):?>

        <div class="reviews--content-item mentor-el">
            <div class="reviews--content-item__header">
                <span class="post--video_user">
                  <i class="material-icons">
                    person
                  </i>
                    <?=$item['SRC_USER_NAME']?> <?=$item['SRC_USER_LAST_NAME']?>, г. <?=$item['SRC_USER_PERSONAL_CITY']?>
                </span>
                <span class="label">
                  Наставник
                </span>
            </div>
            <p>
                <?=$item['MESSAGE']?>
            </p>
        </div>

    <?else:?>
    <div class="reviews--content-item">
        <div class="reviews--content-item__header">
            <img src="<?=(!empty($item['SRC_USER_PERSONAL_PHOTO']))? CFile::GetPath((int)$item['SRC_USER_PERSONAL_PHOTO']) : '/img/svg/account-default.svg'?>" alt="avatar" />
            <?if ((int)$item['SRC_USER_ID'] > 0):?>
            <span class="name-user"><?=$item['SRC_USER_LOGIN']?></span>
            <?else:?>
                <span class="name-user">anonymous</span>
            <?endif;?>
        </div>
        <p>
            <?=$item['MESSAGE']?>
        </p>
    </div>
    <?endif;?>
<?endforeach;?>