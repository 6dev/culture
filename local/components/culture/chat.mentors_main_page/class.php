<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Custom\Culture\CultureHelpers;
use Custom\Chat\MainChat;
global $USER;


class ChatMentorsMainPage extends CBitrixComponent
{

    const CHAT_MODULE_NAME = 'mentors';

    private $params = array();

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "FILTER" => $arParams["FILTER"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
        );
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        $this->arResult["fields"] = array();
        if($this->startResultCache())
        {
            $this->arResult["fields"] = $this->getList();
            $this->includeComponentTemplate();
        }
        return $this->arResult["fields"];
    }


    public function getList() {
        $param = array();
        $param['filter'] = array('MODERATED' => 1, 'SHOW_MAIN_PAGE' => 1, 'REF_ID' => null);
        $param['order'] = (!empty($this->params['ORDER']))? $this->params['ORDER'] : array();
        if (!empty($this->params['OFFSET'])) $param['offset'] = (int)$this->params['OFFSET'];
        if (!empty($this->params['LIMIT'])) $param['limit'] = (int)$this->params['LIMIT'];
        $rsData = MainChat::getMessages($param)['LIST'];
        $messageList = array();
        foreach($rsData as $item) {
            $ms = array();
            $ms['qid'] = $item['ID'];
            $ms['qmessage'] = $item['MESSAGE'];
            $ms['answer'] = Custom\Chat\ChatTable::getRow(array('filter' => array('MODERATED' => 1, 'SHOW_MAIN_PAGE' => 1, 'REF_ID'=>$item['ID'])));
            if (!empty($ms['answer']['ID'])) {
                $query = new \Bitrix\Main\Entity\Query(Bitrix\Main\UserTable::getEntity());
                $query->registerRuntimeField('USER', [
                        'data_type' => 'Bitrix\Main\UserTable',
                        'reference' => [
                            '=this.ID' => 'ref.ID',
                        ],
                    ]
                );
                $query->setSelect(Array(
                    'NAME',
                    'ID',
                    'EMAIL',
                    'PERSONAL_PHOTO',
                    'SECOND_NAME',
                    'LAST_NAME',
                    'PERSONAL_STATE',
                    'PERSONAL_CITY',
                    'PERSONAL_STREET',
                    'WORK_PROFILE',
                    'UF_SCHOOL',
                    'UF_INTERESTS',
                ));
                $query->setFilter(Array("ID"=>$ms['answer']['SRC_USER_ID']));
                $db_res = $query->exec();
                if ($ms['answer']['USER'] = $db_res->fetch()) {
                    $ms['answer']['USER']['PERSONAL_PHOTO_SRC'] = CFile::GetPath((int)$ms['answer']['USER']['PERSONAL_PHOTO']);
                }
            }
            $messageList[] = $ms;
        }





        return array('ITEMS' => $messageList, 'MENTOR_ID' => $this->params['MENTOR_ID']);
    }


}?>