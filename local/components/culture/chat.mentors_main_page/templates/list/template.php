<?foreach($arResult['fields']['ITEMS'] as $item):?>
    <li class="question-answer__item">

        <p class="question-answer__image-wrap">
            <img src="<?=$item['answer']['USER']['PERSONAL_PHOTO_SRC']?>" class="question-answer__image">
        </p>

        <p class="question-answer-list__dialog">

						<span class="question-answer-list__question">
							— <?=$item['qmessage']?>
						</span>

            <span class="question-answer-list__answer-text-wrap">
							<span class="question-answer-list__answer-mentor-name">
								<?=$item['answer']['USER']['LAST_NAME']?> <?=$item['answer']['USER']['NAME']?> <?=$item['answer']['USER']['SECOND_NAME']?>
							</span>

							<span class="question-answer-list__answer-text-mentor-address">
								г. <?=$item['answer']['USER']['PERSONAL_CITY']?>, <?=$item['answer']['USER']['UF_SCHOOL']?>
							</span>

							<span class="question-answer-list__answer-text">
								<?=$item['answer']['MESSAGE']?>
							</span>
						</span>
        </p>
    </li>



<?endforeach;?>

