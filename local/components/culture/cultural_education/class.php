<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;
use Custom\Culture\CultureHelpers;
global $USER;


Loader::includeModule('iblock');

class CulturalEducation extends CBitrixComponent
{

    private $params = array();

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "FILTER" => $arParams["FILTER"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
        );
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        if ($this->params['TYPE'] == 'LIST') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getList();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        if ($this->params['TYPE'] == 'DETAIL') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getDetail();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        return null;
    }


    public function getList() {
        $nPageSize = (!empty($this->params['nPageSize']))? (int)$this->params['nPageSize'] : 20;
        $page_count = (!empty($this->params['PAGE_COUNT']))? (int)$this->params['PAGE_COUNT'] : 1;
        $nPageSize *=  $page_count;
        $arFilter = (!empty($this->params['FILTER']))? $this->params['FILTER'] : array();
        $page = (!empty($this->params['PAGE']))? (int)$this->params['PAGE'] : 1;
        $sort = (!empty($this->params['SORT']))? $this->params['SORT'] : array();
        $res = CIBlockElement::GetList($sort, $arFilter, false, Array("nPageSize"=>$nPageSize, "iNumPage" =>$page), Array("*", "PROPERTY_*"));
        $items = array();
        $fields = array();
        $fields['SHOW_BUTTON_MORE'] = true;
        $res->NavStart();
        $fields['ITEMS_COUNT'] =  $res->NavRecordCount;
        $fields['PAGE_COUNT'] =  ceil($fields['ITEMS_COUNT'] / $nPageSize);
        if ($fields['PAGE_COUNT'] < 1) $fields['PAGE_COUNT'] = 1;
        $fields['CURRENT_PAGE'] =  $page;
        if ($fields['CURRENT_PAGE'] == $fields['PAGE_COUNT']) $fields['SHOW_BUTTON_MORE'] = false;
        $fields['nPageSize'] = $nPageSize;
        while($ob = $res->GetNextElement()){
            $item = array();
            $item['FIELDS'] = $ob->GetFields();
            $item['PROPERTIES'] = $ob->GetProperties();
            $item['USER'] = $this->getUser($item['PROPERTIES']['UID']['VALUE']);
            $resThemes = CIBlockElement::GetList(array(), array('ID'=>$item['PROPERTIES']['CULTURE_TYPES']['VALUE'], 'IBLOCK_ID' => 18), false, Array(), Array("ID", "NAME", "PROPERTY_TAB_COLOR"));
            $item['CULTURE_TYPES'] = array();
            while($ob2 = $resThemes->GetNextElement()){
                $item['CULTURE_TYPES'][] = $ob2->GetFields();
            }
            $items[] = $item;
        }
        $fields['ITEMS'] = $items;
        $fields['SEARCH_FILTERS'] = $this->getFilters($arFilter, $sort);
        $fields['BUTTON_MORE_PARAM'] = 'PAGE_COUNT='.($page_count+1).$fields['SEARCH_FILTERS']['BUTTON_MORE'];
        return $fields;
    }


    public function getDetail() {
        $code = $this->params['CODE'];
        $fields = array();
        $query = \Bitrix\Iblock\ElementTable::query();
        $query->registerRuntimeField('ELEMENT', [
                'data_type' => 'Bitrix\Iblock\ElementTable',
                'reference' => [
                    '=this.CODE' => 'ref.CODE',
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ID' => 'ref.IBLOCK_ID',
                ],
            ]
        );
        $query->registerRuntimeField('PARENT', array(
            "data_type"   => '\Bitrix\Iblock\Iblock',
            'reference'    => array('=this.IBLOCK_ID' => 'ref.ID'),
        ));
        $query->setFilter([
            'CODE' => $code,
            array(
                'LOGIC'=>'OR',
                array("PARENT.CODE" => Custom\Culture\Posts::IBLOCK_CODE),
                array("PARENT.CODE" => Custom\Culture\Lectures::IBLOCK_CODE),
            ),
        ]);
        $query->setSelect([
            'ID',
            'CODE',
            'IBLOCK_ID',
            'IBLOCK_CODE'  => 'PARENT.CODE',
            'IBLOCK_NAME'  => 'PARENT.NAME',
            'DETAIL_TEXT',
            'PREVIEW_TEXT',
            'NAME',
            'DETAIL_PICTURE',
            'PREVIEW_PICTURE',
            'ACTIVE_FROM',
        ]);
        $db_res = $query->exec();
        if ($dbItem = $db_res->fetch()) {
            global $APPLICATION;
            if (!empty($dbItem['NAME'])) $APPLICATION->SetPageProperty('title', $dbItem['NAME']);
            $fields['ELEMENT'] = $dbItem;
            $fields['ELEMENT']['PUB_DATE'] = (!empty($dbItem['ACTIVE_FROM']))? $dbItem['ACTIVE_FROM']->toString(new \Bitrix\Main\Context\Culture(array())) : '';
            $fields['ELEMENT']['IMAGE_SRC'] = (!empty($dbItem['DETAIL_PICTURE']))? CFile::GetPath((int)$dbItem['DETAIL_PICTURE']) : '';
            if (($fields['ELEMENT']['IMAGE_SRC'] == '') && !empty($dbItem['PREVIEW_PICTURE'])) $fields['ELEMENT']['IMAGE_SRC'] = CFile::GetPath((int)$dbItem['PREVIEW_PICTURE']);
            $queryProps = \Bitrix\Iblock\ElementPropertyTable::query();
            $queryProps->registerRuntimeField('PROPERTIES', array(
                "data_type" => '\Bitrix\Iblock\ElementPropertyTable',
                'reference' => array(
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ELEMENT_ID' => 'ref.IBLOCK_ELEMENT_ID'
                )
            ));

            $queryProps->registerRuntimeField('PROPERTIES_CODE', array(
                "data_type" => '\Bitrix\Iblock\PropertyTable',
                'reference' => array('=this.PROPERTIES.IBLOCK_PROPERTY_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            ));
            $queryProps->setFilter([
                'IBLOCK_ELEMENT_ID' => $dbItem['ID']
            ]);
            $queryProps->setSelect([
                'VALUE' => 'PROPERTIES.VALUE',
                'VALUE_TYPE' => 'PROPERTIES.VALUE_TYPE',
                'VALUE_ENUM' => 'PROPERTIES.VALUE_ENUM',
                'VALUE_NUM' => 'PROPERTIES.VALUE_NUM',
                'CODE' => 'PROPERTIES_CODE.CODE'
            ]);
            $db_res = $queryProps->exec();
            $fields['PROPERTIES'] = array();
            while ($dbItem2 = $db_res->fetch()) {
                $fields['PROPERTIES'][$dbItem2['CODE']] = $dbItem2;
            }

            $views = (!empty($fields['PROPERTIES']['VIEWS']) && !empty($fields['PROPERTIES']['VIEWS']['VALUE']))? (int)$fields['PROPERTIES']['VIEWS']['VALUE'] : 0;
            $views++;
            CIBlockElement::SetPropertyValues($dbItem['ID'], $dbItem['IBLOCK_ID'], $views, 'VIEWS');
        }
        return $fields;
    }

    private function getFilters($arFilter=array(), $arSort=array()) {
        $filters = array();
        $btn_more = array();

        $filters['CULTURE_TYPES'] = array();
        $post_ctypes = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>18, 'ACTIVE' => 'Y'), false, Array(), Array("ID", "NAME", "PROPERTY_TAB_COLOR"));
        while($ob = $post_ctypes->GetNextElement()){
            $item = $ob->GetFields();
            $item['IS_SELECTED'] = false;
            if (!empty($arFilter['PROPERTY_CULTURE_TYPES']) && ($item['ID'] == $arFilter['PROPERTY_CULTURE_TYPES'])) {
                $item['IS_SELECTED'] = true;
                $btn_more[] = 'CULTURE_TYPES='.$item['ID'];
            }
            $filters['CULTURE_TYPES'][] = $item;
        }

        $filters['THEMES'] = array();
        /*$post_themes = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>24, 'ACTIVE' => 'Y'), false, Array(), Array("ID", "NAME", "PROPERTY_TAB_COLOR"));
        while($ob = $post_themes->GetNextElement()){
            $item = $ob->GetFields();
            $item['IS_SELECTED'] = false;
            if (!empty($arFilter['PROPERTY_THEMES']) && ($item['ID'] == $arFilter['PROPERTY_THEMES'])) {
                $item['IS_SELECTED'] = true;
                $btn_more[] = 'THEMES='.$item['ID'];
            }
            $filters['THEMES'][] = $item;
        }*/

        $filters['POST_TYPE'] = array();
        $filters['POST_TYPE'][Custom\Culture\Posts::IBLOCK_CODE] = array("NAME" => "Посты", "IS_SELECTED" => false);
        $filters['POST_TYPE'][Custom\Culture\Lectures::IBLOCK_CODE] = array("NAME" => "Лекции", "IS_SELECTED" => false);
        if (!empty($arFilter['IBLOCK_CODE']) && !empty($filters['POST_TYPE'][$arFilter['IBLOCK_CODE']])) {
            $filters['POST_TYPE'][$arFilter['IBLOCK_CODE']]['IS_SELECTED'] = true;
            $btn_more[] = 'POST_TYPE='.$arFilter['IBLOCK_CODE'];
        }


        $filters['SORT'] = array(
            "ACTIVE_FROM" => array("NAME" => "Дате", "IS_SELECTED" => false),
            "PROPERTY_VIEWS" => array("NAME" => "Просмотрам", "IS_SELECTED" => false),
            "PROPERTY_THEMES" => array("NAME" => "Теме", "IS_SELECTED" => false),
        );
        if (sizeof($arSort)) {
            $sortKey = '';
            foreach ($arSort as $k=>$item) {
                $sortKey = $k;
                break;
            }
            if (($sortKey != '') && !empty($filters['SORT'][$sortKey])) {
                $filters['SORT'][$sortKey]["IS_SELECTED"] = true;
                $btn_more[] = 'SORT='.$sortKey;
            }
        }

        $filters['BUTTON_MORE'] = (sizeof($btn_more) > 0)? '&'.join("&", $btn_more) : '';
        return $filters;
    }

    private function getUser($uid) {
        $query = new \Bitrix\Main\Entity\Query(Bitrix\Main\UserTable::getEntity());
        $query->registerRuntimeField('USER', [
                'data_type' => 'Bitrix\Main\UserTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );

        $query->setSelect(Array(
            'NAME',
            'ID',
            'EMAIL',
            'PERSONAL_PHOTO',
            'SECOND_NAME',
            'LAST_NAME',
            'PERSONAL_STATE',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
            'WORK_PROFILE',
            'UF_SCHOOL',
            'UF_INTERESTS',
        ));
        $query->setFilter(array("ID"=>(int)$uid));
        $db_res = $query->exec();
        return $db_res->fetch();
    }


}?>