$( document ).ready(function() {

    let posts = null;

    function getLimit() {
        return parseInt($("#nPageSize").val(), 10);
    }

    function getRequestFields() {
        let formData = new FormData(document.getElementById("search_filter"));
        let formData2 = new FormData(document.getElementById("theme_form"));
        let data = {action: 'list', offset: 1, limit: getLimit()};
        for (let entry of formData.entries()) data[entry[0]] = entry[1];
        for (let entry of formData2.entries()) {
            if (!data.hasOwnProperty(entry[0])) data[entry[0]] = [];
            data[entry[0]].push(entry[1]);
        }
        return data;
    }

    function postsRequest(initData) {
        this.state = {send: 0, clearHtml: 0, page_count: 1};
        this.param = {
            data: initData
        };
        this.callback_success;
        this.callback_error;
        this.request = function (param, state) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: '/cultural-education/ajax.php',
                    dataType: "json",
                    data: param.data,
                    success: (function (response) {
                        if (response.hasOwnProperty('data') && response.data.hasOwnProperty('list_html')) {
                            if (response.data.hasOwnProperty('page_count')) {
                                state.page_count = parseInt(response.data.page_count, 10);
                            }
                            let h = $("#post_list").html();
                            if (state.clearHtml == 1) h = response.data.list_html;
                            else h += response.data.list_html;

                            if (!$('.not-found').hasClass('is-active')) {
                                if (h == '') $('.not-found').addClass('is-active');
                            }
                            else {
                                if (h != '') $('.not-found').removeClass('is-active');
                            }


                            $("#post_list").html(h);
                            if ((param.data.offset == state.page_count) || (state.page_count == 0))  {
                                $("#more_btn").hide();
                            }
                            else $("#more_btn").show();
                            param.data.offset++;
                        }
                        state.clearHtml = 0;
                        state.send = 0;
                    }),
                    error: (function (error) {
                        state.send = 0;
                        state.clearHtml = 0;
                        reject(error);
                    })
                });
            });
        };
        this.getList = function (param) {
            if (this.state.send == 0) {
                this.state.send = 1;
                if (param.hasOwnProperty('offset')) this.param.data.offset = param.offset;
                if (param.hasOwnProperty('clearHtml')) this.state.clearHtml = param.clearHtml;
                this.request(this.param, this.state)
                    .then(this.callback_success,this.callback_error);
            }
            else console.log('send proccess');
        };
    };

    function updateList() {
        $("#more_btn").hide();
        posts = new postsRequest(getRequestFields());
        posts.getList({offset: 1, clearHtml: 1});
    }

    $("#search_filter").on("submit", function() {
        updateList();
        return false;
    });

    $(".theme_check").on("change", function() {
        let parent = $(this).closest('.input-el');
        let active_color = parent.attr('active_color');
        if ($(this).is(':checked')) {
            if (active_color != '') {
                $('label[for="'+ $(this).attr('id') +'"]').css({background: active_color, color: "#fff"});
            }
        }
        else {
            $('label[for="'+ $(this).attr('id') +'"]').css({background: "#f2f2f2", color: "#101010"});
        }
        updateList();
    });

    $(".theme_check_all").on("change", function() {
        $('.theme_check').prop('checked', false);
        $('.input-el').removeClass('active');
        $('.container-input2 label').css({background: "#f2f2f2", color: "#101010"});
        updateList();
    });

    $("#posts_sort").on("change", function() {
        updateList();
    });

    $("body").on("click", "#more_btn", function() {
        if (posts) posts.getList({});
    });

    updateList();

});