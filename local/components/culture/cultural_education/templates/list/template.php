<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<main class="main cultural-education">
    <div class="title-pages">
        <h2>Культурное образование</h2>
    </div>
    <section class="header--cultural-education">
        <div class="container">


            <form id="theme_form" style="width: 101%;">
            <div class="category-filter">
                <div class="all-box">
                    <p>Категории:</p>

                    <input class="theme_check_all" type="checkbox" id="inputE0" />
                    <label for="inputE0">Все</label>
                </div>
                <div class="container-input2">
                    <?foreach ($arResult['fields']['SEARCH_FILTERS']['CULTURE_TYPES'] as $item):?>
                        <div active_color="<?=(!empty($item['PROPERTY_TAB_COLOR_VALUE']))? '#'.$item['PROPERTY_TAB_COLOR_VALUE'] : '#f2f2f2'?>" style="background: #f2f2f2; color: #101010;" class="input-el<?=($item['IS_SELECTED'])? ' active' : ''?>">
                            <input class="theme_check" name="CULTURE_TYPES_LIST[]" type="checkbox" value="<?=$item['ID']?>" id="inputE<?=$item['ID']?>" />
                            <label for="inputE<?=$item['ID']?>"><?=$item['NAME']?></label>
                        </div>
                    <?endforeach;?>
                </div>
            </div>
            </form>



            <div class="header--filter">
                <form id="search_filter">
                    <div class="filter-box">
                        <p>
                            Сортировать по
                        </p>
                        <select id="posts_sort" name="SORT">
                            <option value="">Сортировка по</option>
                            <?foreach ($arResult['fields']['SEARCH_FILTERS']['SORT'] as $k=>$item):?>
                                <option value="<?=$k?>"<?=($item['IS_SELECTED'])? ' selected' : ''?>><?=$item['NAME']?></option>
                            <?endforeach;?>
                        </select>
                    </div>
                </form>
                <input type="hidden" id="nPageSize" value="<?=$arResult['fields']['nPageSize']?>">
            </div>
        </div>
    </section>
    <div class="container">
        <div id="post_list" style="width: 100%;"></div>
        <p id="more_btn" class="news__load">
            Загрузить ещё
            <i class="material-icons arrow_downward__icon">arrow_downward</i>
        </p>
    </div>
    <div class="not-found">
        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="53" viewBox="0 0 53 53" fill="none">
            <path d="M42.049 21.5619C42.049 32.9196 32.8582 42.1239 21.5245 42.1239C10.1908 42.1239 1 32.9196 1 21.5619C1 10.2043 10.1908 1 21.5245 1C32.8582 1 42.049 10.2043 42.049 21.5619Z" stroke="#8C57FC" stroke-width="2" stroke-miterlimit="10"></path>
            <path d="M52 52.0002L36.1111 36.1113" stroke="#8C57FC" stroke-width="2" stroke-miterlimit="10"></path>
            <path d="M26.7222 17.6297L25.7039 16.6113L21.6666 20.6486L17.6294 16.6113L16.6111 17.6297L20.6483 21.6669L16.6111 25.7041L17.6294 26.7224L21.6666 22.6852L25.7039 26.7224L26.7222 25.7041L22.685 21.6669L26.7222 17.6297Z" fill="#F1795C"></path>
        </svg>
        К сожалению, по Вашему запросу ничего не найдено
    </div>
</main>