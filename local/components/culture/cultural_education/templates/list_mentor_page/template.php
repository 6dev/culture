<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult['fields']['ITEMS_COUNT'] > 0):?>


    <section class="pagination-component">
        <div class="container">
            <p>
                Страницы
            </p>
            <ul>
                <?$i = 1;?>
                <?while ($i < ($arResult['fields']['PAGE_COUNT'] + 1)):?>
                    <?if ($i == $arResult['fields']['CURRENT_PAGE']):?>
                        <li class="post_page_link is-active" value="<?=$i?>"><span><?=$i?></span></li>
                    <?else:?>
                        <li class="post_page_link" value="<?=$i?>"><span><?=$i?></span></li>
                    <?endif;?>
                    <?$i++;?>
                <?endwhile;?>
            </ul>
        </div>
    </section>
    <div class="container">

        <?foreach($arResult['fields']['ITEMS'] as $item):?>
            <?
            $imgId = (!empty($item['FIELDS']['PREVIEW_PICTURE']))? (int)$item['FIELDS']['PREVIEW_PICTURE'] : (int)$item['FIELDS']['DETAIL_PICTURE'];
            $imgSrc = ($imgId > 0)? CFile::GetPath($imgId) : '';
            ?>
            <?if ($item['FIELDS']['IBLOCK_CODE'] == Custom\Culture\Posts::IBLOCK_CODE):?>
                <section class="post-pages">
                    <div class="post-pages--content">
                        <div class="post-pages-left">
              <span class="label">
                  <?=$item['FIELDS']['IBLOCK_NAME']?>
              </span>
                            <h3 class="name-post">
                                <?=$item['FIELDS']['NAME']?>
                            </h3>
                            <div class="show-text">
                                <p><?=$item['FIELDS']['PREVIEW_TEXT']?></p>
                            </div>
                            <span><a style="text-decoration: none; color: #5cadff;" target="_blank" href="/posts/detail/<?=$item['FIELDS']['CODE']?>/">Читать далее</a> <i class="material-icons keyboard_arrow_right__icon">keyboard_arrow_right</i></span>
                        </div>
                        <div class="post-pages-right">
                            <?if ($imgSrc != ''):?>
                                <img src="<?=$imgSrc?>" alt="post-img" />
                            <?else:?>
                                <img src="/img/event-image.png" alt="post-img">
                            <?endif;?>
                        </div>
                    </div>
                </section>
            <?endif;?>
            <?if ($item['FIELDS']['IBLOCK_CODE'] == Custom\Culture\Lectures::IBLOCK_CODE):?>
                <section class="post-video">
                    <div class="post-video--container">
                        <div class="post-video--content">
              <span class="label">
                <?=$item['FIELDS']['IBLOCK_NAME']?>
              </span>
                            <h3>
                                <?=$item['FIELDS']['NAME']?>
                            </h3>
                            <div class="video-box">
                                <iframe
                                        width="1280"
                                        height="720"
                                        src="https://www.youtube.com/embed/sn-S82mF_gw"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen
                                ></iframe>
                            </div>
                            <span class="post--video_user">
                <i class="material-icons">
                  person
                </i>
                                <?if (!empty($item['USER'])):?>
                                    <?=$item['USER']['NAME']?> <?=$item['USER']['LAST_NAME']?>
                                    <?if (!empty($item['USER']['PERSONAL_CITY'])):?>
                                        , г. <?=$item['USER']['PERSONAL_CITY']?>
                                    <?endif;?>
                                <?endif;?>
              </span>
                            <div class="tag-box">
                                <?foreach ($item['THEMES'] as $theme):?>
                                    <a href="#"><?=$theme['NAME']?></a>
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>
                </section>
            <?endif;?>
        <?endforeach;?>
    </div>


    <section class="pagination-component">
        <div class="container">
            <p>
                Страницы
            </p>
            <ul>
                <?$i = 1;?>
                <?while ($i < ($arResult['fields']['PAGE_COUNT'] + 1)):?>
                    <?if ($i == $arResult['fields']['CURRENT_PAGE']):?>
                        <li class="post_page_link is-active" value="<?=$i?>"><span><?=$i?></span></li>
                    <?else:?>
                        <li class="post_page_link" value="<?=$i?>"><span><?=$i?></span></li>
                    <?endif;?>
                    <?$i++;?>
                <?endwhile;?>
            </ul>
        </div>
    </section>
<?else:?>
    <div class="not-found is-active">
        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="53" viewBox="0 0 53 53" fill="none">
            <path d="M42.049 21.5619C42.049 32.9196 32.8582 42.1239 21.5245 42.1239C10.1908 42.1239 1 32.9196 1 21.5619C1 10.2043 10.1908 1 21.5245 1C32.8582 1 42.049 10.2043 42.049 21.5619Z" stroke="#8C57FC" stroke-width="2" stroke-miterlimit="10"></path>
            <path d="M52 52.0002L36.1111 36.1113" stroke="#8C57FC" stroke-width="2" stroke-miterlimit="10"></path>
            <path d="M26.7222 17.6297L25.7039 16.6113L21.6666 20.6486L17.6294 16.6113L16.6111 17.6297L20.6483 21.6669L16.6111 25.7041L17.6294 26.7224L21.6666 22.6852L25.7039 26.7224L26.7222 25.7041L22.685 21.6669L26.7222 17.6297Z" fill="#F1795C"></path>
        </svg>
        К сожалению, по Вашему запросу ничего не найдено
    </div>
<?endif;?>