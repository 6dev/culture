<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
 
use Bitrix\Main\Engine\Controller;
use Bitrix\Main\Loader;
 
class CultureMapController extends Controller
{
	/**
	 * @return array
	 */
	public function configureActions()
	{
		return [
			'getObjects' => [
				'prefilters' => []
			],
			'addObject' => [
				'prefilters' => []
			],	
			'getPlacemarkInfo' => [
				'prefilters' => []
			],	
			'addComment' => [
				'prefilters' => []
			],			
			'changeLike' => [
				'prefilters' => []
			],	
			'addFile' => [
				'prefilters' => []
			],					
		];
	}

	public static function addFileAction() {
		global $USER;

		if (!$USER->isAuthorized() || !$_FILES['file']) {
			return [
				'success' => false
			];
		}	

		if ( 0 < $_FILES['file']['error'] ) {
		    return [
				'success' => false
			];
		}
		else {
			$arFile = CFile::SaveFile($_FILES['file'], "map");

			if ($arFile) {
				return [
						'success' => true,
						'id' => $arFile,
						'uploadURL' => CFile::GetPath($arFile)
					];				
			}
		}

	    return [
			'success' => false
		];

	} 

	public static function addObjectAction() {

		Loader::includeModule("iblock");

		global $USER;

		if (!$USER->isAuthorized()) {
			return [
				'success' => false
			];
		}

		$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

		$lt 				= $request->getPost("coordX");  
		$lng 				= $request->getPost("coordY");  
		$name 				= $request->getPost("objectName");  
		$address 			= $request->getPost("objectAddress");  
		$description 		= $request->getPost("describe");  
		$section 			= $request->getPost("categorieId");  

        $el = new \CIBlockElement;

		if ($lt && $lng && $name && $address && $description && $section) {

	        $arrProp = [];
	        $arrProp['LT']               		= $lt;
	        $arrProp['LNG']                 	= $lng;
	        $arrProp['ADDRESS']                 = $address;
	        $arrProp['USER']					= $USER->getId();

	        $arLoadProductArray = Array(
	          "IBLOCK_SECTION_ID"   => $section,   
	          "IBLOCK_ID"           => 20,
	          "NAME"                => $name,
	          "ACTIVE"              => 'N',  
	          "PREVIEW_TEXT"        => $description,
	          "PROPERTY_VALUES"     => $arrProp,
	        );

	        $res = $el->Add($arLoadProductArray);

	        return [
				'success' => $res
			];

		}
		else {
			return [
				'success' => false
			];
		}
	}

	public static function addCommentAction($id, $comment) {
		global $USER;

		if (!$USER->isAuthorized()) {
			return [
				'success' => false
			];
		}

		MapCommentsTable::add([
			'UF_PLACEMARK_ID' => $id,
			'UF_USER_ID' => $USER->getId(),
			'UF_COMMENT' => $comment
			]);

		return [
			'success' => true,
		];

	}

	public static function changeLikeAction($id) {
		global $USER;

		if (!$USER->isAuthorized()) {
			return [
				'success' => false
			];
		}

		MapLikesTable::changeLike($USER->getId(), $id);
 
		return [
			'success' => true,
		];		
	}

	public static function getPlacemarkInfoAction($id) {
		global $USER;

		if (!$USER->isAuthorized()) {
			return [
				'success' => false
			];
		}

		$data = [];

		$data['comments'] 		= MapCommentsTable::getCommentsByPlacemark($id);
		$data['like'] 			= MapLikesTable::checkLike($USER->getId(), $id);
		$data['likes_count']	= MapLikesTable::getCountByPlacemark($id);

		return [
			'success' => true,
			'data' => $data
		];

	}

	public static function getObjectsAction()
	{

		global $USER;

		if (!$USER->isAuthorized()) {
			return [
				'success' => false
			];
		}

		Loader::includeModule("iblock");

		$objects = [];

		$mapFilter = ['ACTIVE' => 'Y', 'IBLOCK_ID' => 20];

	    $dbMaps = \CIBlockElement::getList(
            [],
            $mapFilter,
            false,
            false,
            [
                'ID',
                'IBLOCK_SECTION_ID',
                'NAME',
                'PREVIEW_TEXT',
                'PROPERTY_LT',
                'PROPERTY_LNG',
                'PROPERTY_ADDRESS'
            ]
        );

        while ($arMap = $dbMaps->Fetch()) {
            $objects[] = [
            	'coordX' 			=> $arMap['PROPERTY_LT_VALUE'],
            	'coordY' 			=> $arMap['PROPERTY_LNG_VALUE'],
            	'objectName' 		=> $arMap['NAME'],
            	'objectAddress' 	=> $arMap['PROPERTY_ADDRESS_VALUE'],
            	'describe'			=> $arMap['PREVIEW_TEXT'],
            	'routeCategorieId'	=> $arMap['IBLOCK_SECTION_ID'],
            	'active'			=> 'Y',
            	'objectId'			=> $arMap['ID']
            ];
        }

		$mapFilter = ['ACTIVE' => 'N', 'IBLOCK_ID' => 20, 'PROPERTY_USER' => $USER->getId()];

	    $dbMaps = \CIBlockElement::getList(
            [],
            $mapFilter,
            false,
            false,
            [
                'ID',
                'IBLOCK_SECTION_ID',
                'NAME',
                'PREVIEW_TEXT',
                'PROPERTY_LT',
                'PROPERTY_LNG',
                'PROPERTY_ADDRESS'
            ]
        );

        while ($arMap = $dbMaps->Fetch()) {
            $objects[] = [
            	'coordX' 			=> $arMap['PROPERTY_LT_VALUE'],
            	'coordY' 			=> $arMap['PROPERTY_LNG_VALUE'],
            	'objectName' 		=> $arMap['NAME'],
            	'objectAddress' 	=> $arMap['PROPERTY_ADDRESS_VALUE'],
            	'describe'			=> $arMap['PREVIEW_TEXT'],
            	'routeCategorieId'	=> $arMap['IBLOCK_SECTION_ID'],
            	'active'			=> 'N',
            	'objectId'			=> $arMap['ID']
            ];
        }		

		return [
			'success' => true,
			'objects' => $objects
		];
	} 	

} 