<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();}

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class CultureMap extends CBitrixComponent {

	public function executeComponent() {

		Loader::includeModule("iblock");

		$this->arResult['SECTIONS'] = [];

		$arFilter = ['IBLOCK_ID' => 20, 'GLOBAL_ACTIVE' => 'Y'];
	   	$rsSect = CIBlockSection::GetList(array('name' => 'asc'), $arFilter, false, ['ID', 'NAME']);
	   	while ($arSect = $rsSect->GetNext()) {
	   		$this->arResult['SECTIONS'][$arSect['ID']] = $arSect['NAME'];
	   	}
	   	
		$this->includeComponentTemplate();

	}

}