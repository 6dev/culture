let selectedPoint = {
    type: 'Feature',
    geometry: {
        type: 'Point',
        coordinates: null,
    },
    options: {
        categorieId: null,
        objectNameForSearch: null,
        objectName: null,
        objectAddress: null,
        describe: null,
    },
}

function addSelectedPointBalloon() {
    selectedPoint.properties = {
        balloonContentHeader: '<div class="route_name">' + selectedPoint.options.categorieId ? categories[selectedPoint.options.categorieId] : "" + '</div>',
        balloonContentBody: '<div class="object_describe">' + selectedPoint.options.describe +
            '</div><div class="object_photo_gallery"></div>',
        balloonContentFooter: '<div class="object_name">' +
            selectedPoint.options.objectName +
            '</div><div class="object_address">' + selectedPoint.options.objectAddress + '</div>',
    }
};

let clickedPlacemark;
let clusterer;
let searchValue = null;
let map;
let searchControl;
let uppy;

let addMap = false;

let myArr = [],
    allPoints = [];


document.addEventListener("DOMContentLoaded", function() {
    let addPointButton = document.querySelector('.addPointButton');
    let addPointWindow = document.querySelector('#add-monument--form');

    let selectedCheckboxes = Object.keys(categories); //содержит все категории маршрутов categorieId, выбранные в форме 
    selectedCheckboxes.forEach(val => {
        val = +val;
    });

    let chooseRouteTypeBlock = document.querySelector('.chooseRouteType');
    let addRoute = document.querySelector('.addRoute');
    //let pedestrianRoute = chooseRouteTypeBlock.querySelector('#pedestrian');
    //let autoRoute = chooseRouteTypeBlock.querySelector('#auto');
    //let masstransitRoute = chooseRouteTypeBlock.querySelector('#masstransit');

    var request = new XMLHttpRequest();
    request.open('GET', '/bitrix/services/main/ajax.php?c=culture:map&action=getObjects&mode=ajax', true);
    request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

    request.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
            let data = JSON.parse(request.response);

            for (var i in data.data.objects) {
                allPoints.push(data.data.objects[i]);
            }

            allPoints.forEach(obj => {
                myArr.push({
                    type: 'Feature',
                    geometry: {
                        type: 'Point',
                        coordinates: [obj.coordX, obj.coordY],
                    },
                    options: {
                        categorieId: obj.routeCategorieId,
                        objectNameForSearch: obj.objectName.toLowerCase(),
                        objectName: obj.objectName,
                        objectAddress: obj.objectAddress,
                        describe: obj.describe,
                        preset: (obj.active == 'Y' ? 'islands#blueDotIcon' : 'islands#grayDotIcon'),
                        objectId: obj.objectId
                    },
                    properties: {
                        balloonContentHeader: '<div class="route_name">' + categories[obj.routeCategorieId] + '</div>',
                        balloonContentBody: '<div class="object_describe">' + obj.describe +
                            '</div><div class="object_photo_gallery"></div>',
                        balloonContentFooter: '<div class="object_name">' +
                            obj.objectName +
                            '</div><div class="object_address">' + obj.objectAddress + '</div>',
                    }
                })
            });

            ymaps.ready(init);
        }
    };

    request.send();

    function init() {
        map = new ymaps.Map('map', {
            center: [59.94, 30.32],
            zoom: 12,
            controls: ['zoomControl'],
            behaviors: ['drag'],
        });

        searchControl = new ymaps.control.SearchControl({
            options: {
                provider: 'yandex#search',
                noPlacemark: true,
            }
        });
        //map.controls.add(searchControl);

        window.myObjects = ymaps.geoQuery({
            type: "FeatureCollection",
            features: myArr,
        });

        checkState();

        pointsArr = [];
        allPoints.forEach(obj => {
            pointsArr.push([obj.coordX, obj.coordY]);
        });

         map.events.add('click', function(e) {
            if (addMap) {
                let coords = e.get('coords');
                selectedPoint.geometry.coordinates = coords.slice(0);
                if (clickedPlacemark) {
                    clickedPlacemark.geometry.setCoordinates(coords);

                } else {
                    clickedPlacemark = new ymaps.Placemark(coords, {
                        address: ""
                    }, {
                        preset: 'islands#redDotIcon',
                        draggable: true,
                    });
                    clickedPlacemark.events.add('dragend', function() {
                        getAddress(clickedPlacemark.geometry.getCoordinates());
                    });
                }
                map.geoObjects.add(clickedPlacemark);
                getAddress(coords);

                $('.add-monument--popup').closest('.popup-bg').addClass('is-active')
                $('body').addClass('no-scroll')
            }
        })

       
        uppy = Uppy.Core({
            autoProceed: true,
            locale: Uppy.locales.ru_RU
          })
            .use(Uppy.DragDrop, {
                inline: true,
                target: '#drag-drop-area',
                height: 300,
                width: 317,
                locale: {
                  strings: {
                    dropPaste: 'Перетащите файл сюда или',
                    browse: 'загрузите с компьютера'
                  }
                }
            });

        uppy.use(Uppy.XHRUpload, {
            endpoint: '/bitrix/services/main/ajax.php?c=culture:map&action=addFile&mode=ajax',
            formData: true,
            fieldName: 'file',
            validateStatus(status, responseText, response) {
                let data = JSON.parse(responseText);

                return data.data.success;
            },
        });

        uppy.on('upload-success', (file, response) => {
            let fileInput = document.querySelector('input[name="uppy"]');

            fileInput.value = response.body.data.id;

            var url = response.body.data.uploadURL
            var fileName = file.name

            document.querySelector('.uploaded-files ol').innerHTML =
              '<li><a href="' + url + '" target="_blank">' + fileName + '</a></li>'
        })

    }

    /*
    chooseRouteTypeBlock.addEventListener('click', e => {
        if (e.target.tagName == "INPUT") {
            multiRoute.model.setParams({
                routingMode: e.target.value,
            });

        }
    })

    addRoute.addEventListener('click', e => {
        e.preventDefault();
        if (chooseRouteTypeBlock.classList.contains('hide')) {
            chooseRouteTypeBlock.classList.remove('hide');
            multiRoute = new ymaps.multiRouter.MultiRoute({
                referencePoints: pointsArr,
                params: {
                    routingMode: 'pedestrian'
                }
            }, {
                boundsAutoApply: true
            });
            map.geoObjects.add(multiRoute);
        } else {
            e.preventDefault();
        }
    })*/

    /*
    addPointButton.addEventListener('click', function(e) {
        e.preventDefault();
        addPointButton.classList.add('hide');
        addPointWindow.classList.remove('hide');

        searchControl.events.add('resultselect', function(e) {

            var index = e.get('index');
            searchControl.getResult(index).then(function(res) {

                let coords = res.geometry.getCoordinates();

                if (clickedPlacemark) {
                    clickedPlacemark.geometry.setCoordinates(coords);
                } else {
                    clickedPlacemark = new ymaps.Placemark(coords, {
                        address: ""
                    }, {
                        preset: 'islands#redDotIcon',
                        draggable: true,
                    });
                    clickedPlacemark.events.add('dragend', function() {
                        getAddress(clickedPlacemark.geometry.getCoordinates());
                    });
                }
                map.geoObjects.add(clickedPlacemark);
                selectedPoint.geometry.coordinates = coords.slice(0);
                getAddress(coords);


            });
        })


        map.events.add('click', function(e) {
            if (addPointButton.classList.contains('hide')) {
                let coords = e.get('coords');
                selectedPoint.geometry.coordinates = coords.slice(0);
                if (clickedPlacemark) {
                    clickedPlacemark.geometry.setCoordinates(coords);

                } else {
                    clickedPlacemark = new ymaps.Placemark(coords, {
                        address: ""
                    }, {
                        preset: 'islands#redDotIcon',
                        draggable: true,
                    });
                    clickedPlacemark.events.add('dragend', function() {
                        getAddress(clickedPlacemark.geometry.getCoordinates());
                    });
                }
                map.geoObjects.add(clickedPlacemark);
                getAddress(coords);
            }
        })

    })
    
    */

    //Блок фильтрации по категориям
    let filterForm = document.getElementById('form');
    filterForm.addEventListener('click', function(e) {
        if (selectedCheckboxes.includes(e.target.value)) {
            selectedCheckboxes.splice(selectedCheckboxes.indexOf(e.target.value), 1);
            e.target.checked = false;
        } else {
            e.target.checked = true;
            selectedCheckboxes.push(e.target.value);
        }
        checkState();
    });


    $('#set_in_map').click(function() {
        $(this).closest('.popup-bg').removeClass('is-active')
        $('body').removeClass('no-scroll')

        addMap = true

        return false
    });

    // Добавление метки
    $('.add-monument--popup #add-monument--form').on('submit', function (e) {
        e.preventDefault()

        if (addPointWindow.querySelector('.object_title').value !== "" && addPointWindow.querySelector('.object_describe').value !== "" && addPointWindow.querySelector('.object_address').value !== "") {

            selectedPoint.options.objectName = addPointWindow.querySelector('.object_title').value;
            selectedPoint.options.objectNameForSearch = addPointWindow.querySelector('.object_title').value.toLowerCase();
            selectedPoint.options.objectAddress = addPointWindow.querySelector('.object_address').value;
            selectedPoint.options.describe = addPointWindow.querySelector('.object_describe').value;
            for (let key in categories) {
                if (categories[key].toLowerCase() === addPointWindow.querySelector('.route-category').value.toLowerCase()) {
                    selectedPoint.options.categorieId = key;
                }
            }

            // Загружаем изображения
            uppy.upload().then((result) => {

                if (result.successful) {
                    var request = new XMLHttpRequest();
                    request.open('POST', '/bitrix/services/main/ajax.php?c=culture:map&action=addObject&mode=ajax', true);
                    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

                    request.onreadystatechange = function() {
                        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {

                            addSelectedPointBalloon();
                            let selectedPointCopy = {
                                ...selectedPoint,
                                geometry: {
                                    ...selectedPoint.geometry
                                },
                                options: {
                                    ...selectedPoint.options
                                }
                            }
                            myArr.push(selectedPointCopy);
                            myObjects = myObjects.add(selectedPointCopy);
                            checkState();
                            map.controls.remove(searchControl);

                            $(this).closest('.popup-bg').removeClass('is-active')
                            $('.monument-added').closest('.popup-bg').addClass('is-active')
                            $('body').addClass('no-scroll')

                            clearForm();
                            map.geoObjects.remove(clickedPlacemark);

                            uppy.reset();
                        }
                    };

                    let fileInput = document.querySelector('input[name="uppy"]');

                    request.send(new URLSearchParams(selectedPoint.options).toString() + '&coordX=' + selectedPoint.geometry.coordinates[0] + '&coordY=' + selectedPoint.geometry.coordinates[1] + '&file=' + fileInput.value);

                }



            });

        }

        addMap = false;

        return false
    })

    $('.add-monument--popup .close').click(function() {

        addMap = false

    });

    //Блок поиска
    let searchWindow = document.getElementById('search');
    searchWindow.addEventListener('keyup', function(e) {
        let value = searchWindow.value.toLowerCase();
        searchValue = value;
        checkState();
    });

    function clearForm() {
        var inputFields = addPointWindow.querySelectorAll('input');
        inputFields.forEach(obj => {
            obj.value = "";
        })

        var textareaField = addPointWindow.querySelectorAll('textarea');
        textareaField.forEach(obj => {
            obj.value = "";
        })

        document.querySelector('.uploaded-files ol').innerHTML = '';
    }


    let addressString;

    function getAddress(coords) {

        ymaps.geocode(coords).then(function(res) {

            var firstGeoObject = res.geoObjects.get(0);

            let addressCoords = coords;
            let addressString = firstGeoObject.getAddressLine();
            document.querySelector('.object_address').value = firstGeoObject.getAddressLine();
            clickedPlacemark.properties.set('address', addressString);
        });

    }

    function checkState() {
        let shownObjects;
        let byCategorie = new ymaps.GeoQueryResult();
        let byValue = new ymaps.GeoQueryResult();

        function escapeRegExp(string) {
            return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
        }

        selectedCheckboxes.forEach(val => {
            byCategorie = myObjects.search('options.categorieId = ' + val).add(byCategorie);
        });
        if (searchValue === null || searchValue === "") {
            byValue = myObjects;
        } else {
            byValue = myObjects.search('options.objectNameForSearch regexp  "' + escapeRegExp(searchValue) + '"')
        }


        shownObjects = byCategorie.intersect(byValue);
        map.geoObjects.remove(clusterer);
        clusterer = ymaps.geoQuery(shownObjects).clusterize();
        map.geoObjects.add(clusterer);
        map.geoObjects.remove(ymaps.geoQuery(myObjects.remove(shownObjects)).clusterize());
        ymaps.geoQuery(shownObjects).search('geometry.type="Point"').addEvents('click', function(e) {
            showRouteInfo(e)
        });
    }

    let selectedObject;

    function showRouteInfo(e) {
        coords = e.originalEvent.target.geometry._coordinates;

        myArr.forEach(obj => {
            if (obj.geometry.coordinates == coords) {
                selectedObject = {
                    ...obj
                };
                return;
            }
        })

        var objectId = selectedObject.options.objectId;
        var request = new XMLHttpRequest();
        request.open('GET', '/bitrix/services/main/ajax.php?c=culture:map&action=getPlacemarkInfo&mode=ajax&id=' + objectId, true);
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

        request.onreadystatechange = function() {
            if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {

                let data = JSON.parse(request.response);

                for (var comment of data.data.data.comments) {
                    var newNode = document.createElement('div');
                    newNode.innerHTML = comment.UF_COMMENT;

                    document.querySelector('.showComments').append(newNode);
                }



                routeInfoWindow = document.querySelector('.showRouteInfo');
                if (routeInfoWindow.classList.contains('hide')) {
                    routeInfoWindow.classList.remove('hide');
                }

                routeInfoWindow.addEventListener('click', function(e) {
                    if (e.target.classList.contains('close')) {
                        routeInfoWindow.classList.add('hide');
                    }
                });

                routeInfoWindow.querySelector('.likeCount').innerHTML = data.data.data.likes_count;

                if (data.data.data.like) {
                    routeInfoWindow.querySelector('.likeIcon').classList.add('active');
                }

                routeInfoWindow.querySelector('.routeCategorie').innerHTML = categories[selectedObject.options.categorieId];
                routeInfoWindow.querySelector('.objectName').innerHTML = selectedObject.options.objectName;
                routeInfoWindow.querySelector('.objectDescribe').innerHTML = selectedObject.options.describe;
                routeInfoWindow.querySelector('.objectAddress').innerHTML = selectedObject.options.objectAddress;

                routeInfoWindow.dataset.id = objectId;


            }
        };

        request.send();
    }

    /*
    let addCommentWindow = document.querySelector('.addComment');
    let showCommentsWindow = document.querySelector('.showComments');
    let messageText = document.getElementById('messageText');
    addCommentWindow.addEventListener('click', function(e) {
        if (e.target.tagName == 'BUTTON') {
            e.preventDefault();
            div = document.createElement('div');
            if (messageText.value != "") {

                var request = new XMLHttpRequest();
                request.open('POST', '/bitrix/services/main/ajax.php?c=culture:map&action=addComment&mode=ajax', true);
                request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

                request.onreadystatechange = function() {
                    if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
                        div.innerHTML = messageText.value;
                        showCommentsWindow.appendChild(div);
                        messageText.value = "";
                    }
                };

                routeInfoWindow = document.querySelector('.showRouteInfo');
                request.send('id=' + routeInfoWindow.dataset.id + '&comment=' + messageText.value);

            }
        }
    });

    let likeIcon = document.querySelector('.likeIcon');
    let likeCount = document.querySelector('.likeCount');
    likeIcon.addEventListener('click', function(e) {
        let like = "&#128153";
        let dislike = "&#128420";


        var request = new XMLHttpRequest();
        request.open('POST', '/bitrix/services/main/ajax.php?c=culture:map&action=changeLike&mode=ajax', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

        request.onreadystatechange = function() {
            if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {}
        };

        routeInfoWindow = document.querySelector('.showRouteInfo');
        request.send('id=' + routeInfoWindow.dataset.id);

        if (likeIcon.classList.contains('active')) {
            likeIcon.innerHTML = dislike;
            likeIcon.classList.remove('active');
            likeCount.innerHTML = +likeCount.innerHTML - 1;
        } else {
            likeIcon.innerHTML = like;
            likeIcon.classList.add('active');
            likeCount.innerHTML = +likeCount.innerHTML + 1;
        }
    });*/


})