let selectedPoint = {
    coordX: null,
    coordY: null,
    objectName: null,
    objectAddress: null,
    describe: null,
    routeCategorieId: null,
};

let map, myObjects;

let allPoints = [];

function showMarks (points, mapName) {
    let geoObjects = [];
    for (let i = 0; i < points.length; i++) {
        geoObjects[i] = new ymaps.Placemark([points[i].coordX, points[i].coordY], {
            balloonContentHeader: '<div class="route_name">' + points[i].objectName + '</div>',
            balloonContentBody: '<div class="object_describe">' + points[i].describe +
                '</div><div class="object_photo_gallery"></div>',
            balloonContentFooter: '<div class="object_name">' +
                categories[points[i].routeCategorieId] +
                '</div><div class="object_address">' + points[i].objectAddress + '</div>',
        }, {
            name: points[i].objectName.toLowerCase(),
            category: points[i].routeCategorieId
        });
    }

    myObjects = ymaps.geoQuery(geoObjects).addToMap(map);
}

document.addEventListener("DOMContentLoaded", function() {
    let addPointButton = document.querySelector('.addPointButton');
    let addPointWindow = document.querySelector('.addPointWindow');

    var request = new XMLHttpRequest();
    request.open('GET', '/bitrix/services/main/ajax.php?c=culture:map&action=getObjects&mode=ajax', true);
    request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

    request.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
            let data = JSON.parse(request.response);

            for (var i in data.data.objects) {
                allPoints.push(data.data.objects[i]);
            }

            ymaps.ready(init);
        }
    };

    request.send();

    function init() {

        map = new ymaps.Map('map', {
            center: [59.94, 30.32],
            zoom: 12,
            controls: ['zoomControl'],
            behaviors: ['drag'],
        });

        showMarks(allPoints, map);
    }

    addPointButton.addEventListener('click', function(e) {
        
        e.preventDefault();
        addPointButton.classList.add('hide');
        addPointWindow.classList.remove('hide');

        let searchControl = new ymaps.control.SearchControl({
            options: {
                provider: 'yandex#search',
                noPlacemark: true,
            }
        });
        map.controls.add(searchControl);

        let clickedPlacemark;

        searchControl.events.add('resultselect', function(e) {

            var index = e.get('index');
            searchControl.getResult(index).then(function(res) {

                var coords = res.geometry.getCoordinates();

                clickedPlacemark = new ymaps.Placemark(coords, {
                    address: ""
                }, {
                    preset: 'islands#redDotIcon',
                    draggable: true,
                });

                map.geoObjects.add(clickedPlacemark);
                clickedPlacemark.events.add('dragend', function() {
                    getAddress(clickedPlacemark.geometry.getCoordinates());
                });

                getAddress(coords);


            });
        })

        map.events.add('click', function(e) {
            var coords = e.get('coords');

            if (clickedPlacemark) {
                clickedPlacemark.geometry.setCoordinates(coords);
                map.geoObjects.add(clickedPlacemark);
            } else {

                clickedPlacemark = new ymaps.Placemark(coords, {
                    address: ""
                }, {
                    preset: 'islands#redDotIcon',
                    draggable: true,
                });

                map.geoObjects.add(clickedPlacemark);
                clickedPlacemark.events.add('dragend', function() {
                    getAddress(clickedPlacemark.geometry.getCoordinates());
                });
            }
            getAddress(coords);
        })

    })

    let closeWindow = document.querySelector('.close');
    let addPoint = addPointWindow.querySelector('button');
    closeWindow.addEventListener('click', function(e) {
        addPointWindow.classList.add('hide');
        addPointButton.classList.remove('hide');
        clearForm();
    });

    addPoint.addEventListener('click', function(e) {

        e.preventDefault();
        if (addPointWindow.querySelector('.object_title').value !== "" && addPointWindow.querySelector('.object_describe').value !== "" && addPointWindow.querySelector('.object_address').value !== "") {
            selectedPoint.objectName = addPointWindow.querySelector('.object_title').value;
            selectedPoint.objectAddress = addPointWindow.querySelector('.object_address').value;
            selectedPoint.describe = addPointWindow.querySelector('.object_describe').value;
            for (let key in categories) {
                if (categories[key].toLowerCase() === addPointWindow.querySelector('.route-category').value.toLowerCase()) {
                    selectedPoint.routeCategorieId = key;
                }
            }

            var request = new XMLHttpRequest();
            request.open('POST', '/bitrix/services/main/ajax.php?c=culture:map&action=addObject&mode=ajax', true);
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

            request.onreadystatechange = function() {
                if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {

                    clearForm();
                    addPointWindow.classList.add('hide');
                    addPointButton.classList.remove('hide');

                    selectedPoint.coordX = selectedPoint.coordY = selectedPoint.objectName = selectedPoint.objectAddress = selectedPoint.describe = selectedPoint.routeCategorieId = null;

                }
            };

            request.send(new URLSearchParams(selectedPoint).toString());


        } else {
            document.querySelector('.warning').classList.remove('hide');
        }
    })


    let searchWindow = document.getElementById('search');
    searchWindow.addEventListener('keyup', function(e) {
        let value = searchWindow.value.toLowerCase();

        var shownObjects, byName = new ymaps.GeoQueryResult(),
            byCategory = new ymaps.GeoQueryResult();

        byName = myObjects.search('options.name rlike "' + value + '"');

        if (selectedCheckboxes.length > 0) {
            for (let key in selectedCheckboxes) {
                byCategory = myObjects.search('options.category = ' + selectedCheckboxes[key]).add(byCategory);
            }

            shownObjects = byName.intersect(byCategory).addToMap(map);
        } else {
            shownObjects = byName.addToMap(map);
        }

        myObjects.remove(shownObjects).removeFromMap(map);
    });

    document.querySelectorAll('.categorie').forEach(item => {
        item.addEventListener('change', function(e) {
            if (addPointButton.classList.contains('hide')) {
                addPointButton.classList.remove('hide');
            }

            if (this.checked) {
                selectedCheckboxes.push(this.value);
            } else {
                selectedCheckboxes.splice(selectedCheckboxes.indexOf(this.value), 1);
            }

            let value = searchWindow.value.toLowerCase();

            var shownObjects, byName = new ymaps.GeoQueryResult(),
                byCategory = new ymaps.GeoQueryResult();

            for (let key in selectedCheckboxes) {
                byCategory = myObjects.search('options.category = ' + selectedCheckboxes[key]).add(byCategory);
            }

            if (value) {
                byName = myObjects.search('options.name rlike "' + value + '"');
                shownObjects = byCategory.intersect(byName).addToMap(map);
            } else {
                shownObjects = byCategory.addToMap(map);
            }

            myObjects.remove(shownObjects).removeFromMap(map);

        }); 
    });

    function clearForm() {
        var inputFields = addPointWindow.querySelectorAll('input');
        inputFields.forEach(obj => {
            obj.value = "";
        })
        var textareaField = addPointWindow.querySelector('textarea').value = "";
    }


    let addressString;

    function getAddress(coords) {

        ymaps.geocode(coords).then(function(res) {
            var firstGeoObject = res.geoObjects.get(0);
            addressCoords = coords;
            addressString = firstGeoObject.getAddressLine();

            document.querySelector('.object_address').value = firstGeoObject.getAddressLine();

            selectedPoint.coordX = coords[0];
            selectedPoint.coordY = coords[1];

            clickedPlacemark.properties.set('address', addressString);

        });

    }
})