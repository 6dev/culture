<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>

<script src="https://api-maps.yandex.ru/2.1/?apikey=b72e32d6-86a6-447e-b4c5-cf053e65893a&lang=ru_RU" type="text/javascript"></script>

<script>
	let categories = <?=CUtil::PhpToJSObject($arResult['SECTIONS'])?>;
	let selectedCheckboxes = <?=CUtil::PhpToJSObject(array_keys($arResult['SECTIONS']))?>;


</script>

<main class="main my_russia">
  <aside>
    <div class="my_russia-filter">
      <div class="tab-menu">
        <ul>
          <li class="is-active">
            <span>Памятники культуры</span>
          </li>
          <li>
            <span>Маршруты</span>
          </li>
        </ul>
      </div>
      <div class="tab-container">
        <div class="tab-el is-active">
          <form action="/" id="form">
            <div class="scroll-container">
              <label class="search-label">
                <input type="search" id="search" placeholder="Поиск по памятникам культуры" />
              </label>
              <div class="select-box">
                <p>Регион</p>
                <select>
                  <option value="1">Российская Федерация</option>
                  <option value="2">Центральный федеральный округ</option>
                  <option value="3">Северо-Западный федеральный округ</option>
                  <option value="4">Южный федеральный округ</option>
                  <option value="5">Приволжский федеральный округ</option>
                </select>
              </div>
              <div class="select-box">
                <p>Населенный пункт</p>
                <select>
                  <option value="1">Москва</option>
                  <option value="2">Санкт-Петербург</option>
                  <option value="3">Новосибирск</option>
                  <option value="4">Екатеринбург</option>
                  <option value="5">Нижний Новгород</option>
                </select>
              </div>
              <div class="label-container">
              	<?php foreach ($arResult['SECTIONS'] as $key => $name) : ?>
                <div class="label-el">
                  <input type="checkbox" checked value="<?=$key?>" name="<?=$name?>" class="categorie" id="object_<?=$key?>" />
                  <label for="object_<?=$key?>"><?=$name?></label>
                </div>
            	<?php endforeach; ?>

              </div>
              <div class="nothing-found__content" style="display: none">
                <div class="img-box">
                  <svg xmlns="http://www.w3.org/2000/svg" width="53" height="53" viewBox="0 0 53 53" fill="none">
                    <path
                      d="M42.049 21.5619C42.049 32.9196 32.8582 42.1239 21.5245 42.1239C10.1908 42.1239 1 32.9196 1 21.5619C1 10.2043 10.1908 1 21.5245 1C32.8582 1 42.049 10.2043 42.049 21.5619Z"
                      stroke="#8C57FC"
                      stroke-width="2"
                      stroke-miterlimit="10"
                    ></path>
                    <path
                      d="M52 52.0002L36.1111 36.1113"
                      stroke="#8C57FC"
                      stroke-width="2"
                      stroke-miterlimit="10"
                    ></path>
                    <path
                      d="M26.7222 17.6297L25.7039 16.6113L21.6666 20.6486L17.6294 16.6113L16.6111 17.6297L20.6483 21.6669L16.6111 25.7041L17.6294 26.7224L21.6666 22.6852L25.7039 26.7224L26.7222 25.7041L22.685 21.6669L26.7222 17.6297Z"
                      fill="#F1795C"
                    ></path>
                  </svg>
                </div>
                <p>К сожалению, по Вашему запросу ничего не найдено</p>
              </div>
            </div>
          </form>
        </div>
        <div class="tab-el">
          <form action="/">
            <div class="scroll-container">
              <label class="search-label">
                <input type="search" placeholder="Поиск по маршрутам " />
              </label>
              <div class="select-box">
                <p>Регион</p>
                <select>
                  <option value="1">Российская Федерация</option>
                  <option value="2">Центральный федеральный округ</option>
                  <option value="3">Северо-Западный федеральный округ</option>
                  <option value="4">Южный федеральный округ</option>
                  <option value="5">Приволжский федеральный округ</option>
                </select>
              </div>
              <div class="select-box">
                <p>Населенный пункт</p>
                <select>
                  <option value="1">Москва</option>
                  <option value="2">Санкт-Петербург</option>
                  <option value="3">Новосибирск</option>
                  <option value="4">Екатеринбург</option>
                  <option value="5">Нижний Новгород</option>
                </select>
              </div>
              <div class="nothing-found__content" style="display: none">
                <div class="img-box">
                  <svg xmlns="http://www.w3.org/2000/svg" width="53" height="53" viewBox="0 0 53 53" fill="none">
                    <path
                      d="M42.049 21.5619C42.049 32.9196 32.8582 42.1239 21.5245 42.1239C10.1908 42.1239 1 32.9196 1 21.5619C1 10.2043 10.1908 1 21.5245 1C32.8582 1 42.049 10.2043 42.049 21.5619Z"
                      stroke="#8C57FC"
                      stroke-width="2"
                      stroke-miterlimit="10"
                    ></path>
                    <path
                      d="M52 52.0002L36.1111 36.1113"
                      stroke="#8C57FC"
                      stroke-width="2"
                      stroke-miterlimit="10"
                    ></path>
                    <path
                      d="M26.7222 17.6297L25.7039 16.6113L21.6666 20.6486L17.6294 16.6113L16.6111 17.6297L20.6483 21.6669L16.6111 25.7041L17.6294 26.7224L21.6666 22.6852L25.7039 26.7224L26.7222 25.7041L22.685 21.6669L26.7222 17.6297Z"
                      fill="#F1795C"
                    ></path>
                  </svg>
                </div>
                <p>К сожалению, по Вашему запросу ничего не найдено</p>
              </div>
              <ul>
                <li>
                  <p>Название маршрута</p>
                  <mark>5.9 км</mark>
                  <div class="toggle-box">
                    <span>
                      Подробнее
                    </span>
                  </div>
                </li>
                <li>
                  <p>Длинное название маршрута во всю строку</p>
                  <mark>5.9 км</mark>
                  <div class="toggle-box">
                    <span>
                      Подробнее
                    </span>
                  </div>
                </li>
                <li>
                  <p>Название маршрута</p>
                  <mark>5.9 км</mark>
                  <div class="toggle-box">
                    <span>
                      Подробнее
                    </span>
                  </div>
                </li>
                <li>
                  <p>Название маршрута</p>
                  <mark>5.9 км</mark>
                  <div class="toggle-box">
                    <span>
                      Подробнее
                    </span>
                  </div>
                </li>
                <li>
                  <p>Название маршрута</p>
                  <mark>5.9 км</mark>
                  <div class="toggle-box">
                    <span>
                      Подробнее
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </form>
        </div>
      </div>
    </div>
  </aside>
  <section class="map-content">
    <div class="add-to--map">
      <p>Добавить</p>
    </div>
    <div id="map"></div>
  </section>
</main>

<link rel="stylesheet" href="https://releases.transloadit.com/uppy/v1.23.2/uppy.min.css">
<script src="https://releases.transloadit.com/uppy/v1.23.2/uppy.min.js"></script>
<script src="https://releases.transloadit.com/uppy/locales/v1.16.10/ru_RU.min.js"></script>


    <div class="placemark-popup popup-bg">
      <div class="placemark-popup__content">
        <div class="title-popup">
          <h2>Название памятника культуры</h2>
          <p>ул. Вавилова, 48</p>
        </div>
        <div class="placemark-slider--container">
          <div class="placemark-slider swiper-container">
            <div class="swiper-wrapper">
              <div class="swiper-slide"><img src="img/news.jpg" alt="" /></div>
              <div class="swiper-slide"><img src="img/news.jpg" alt="" /></div>
              <div class="swiper-slide"><img src="img/news.jpg" alt="" /></div>
              <div class="swiper-slide"><img src="img/news.jpg" alt="" /></div>
              <div class="swiper-slide">
                <input type="file" id="SliderHidden" hidden />
                <label for="SliderHidden"
                  ><p>
                    <span>+</span>
                    Добавить фото
                  </p></label
                >
              </div>
            </div>
          </div>
          <div class="placemark-slider--pagination swiper-pagination"></div>
          <div class="placemark-slider--prev swiper-button-prev"></div>
          <div class="placemark-slider--next swiper-button-next"></div>
        </div>
        <div class="scroll-block">
          <p>
            Обсценная идиома, несмотря на то, что все эти характерологические черты отсылают не к единому образу
            нарратора, редуцирует гекзаметр, так как в данном случае роль наблюдателя опосредована ролью рассказчика.
            Обсценная идиома, несмотря на то, что все эти характерологические черты отсылают не к единому образу
            нарратора, редуцирует гекзаметр, так как в данном случае роль наблюдателя опосредована ролью рассказчика.
          </p>
        </div>
        <div class="footer-popup">
          <div class="input-box">
            <input type="checkbox" id="gr32" />
            <label for="gr32">
              <svg xmlns="http://www.w3.org/2000/svg" width="13" height="15" viewBox="0 0 13 15" fill="none">
                <path
                  d="M8.14667 1.76471L7.8 0H0V15H1.73333V8.82353H6.58667L6.93333 10.5882H13V1.76471H8.14667Z"
                  fill="#BDBDBD"
                />
              </svg>
              <p>Я здесь был!</p>
            </label>
          </div>
          <div class="avatar">
            <img src="img/avatar_test.jpg" alt="" />
            <span>iva_ivan_05</span>
          </div>
        </div>
        <div class="chart-popup">
          <div class="msger">
            <div class="msger-chat">
              <div class="msg left-msg">
                <div class="msg-bubble">
                  <div class="msg-img" style="background-image: url('../img/avatar4.jpg')"></div>
                  <div class="msg-info">
                    <div class="msg-info-name">iva_ivan_05</div>
                  </div>
                </div>
                <div class="msg-text">
                  Очень оригинальный комментарий школьника. Наверное, ему все понравилось))!!!
                </div>
              </div>

              <div class="msg right-msg">
                <div class="msg-bubble">
                  <div class="msg-img" style="background-image: url(../img/avatar3.jpg)"></div>
                  <div class="msg-info">
                    <div class="msg-info-name">iva_ivan_05</div>
                  </div>
                </div>
                <div class="msg-text">
                  Еще один очень оригинальный комментарий школьника. Ему тоже все понравилось, пойдет глазеть еще)
                </div>
              </div>

              <div class="msg left-msg">
                <div class="msg-bubble">
                  <div class="msg-img" style="background-image: url(../img/avatar4.jpg)"></div>
                  <div class="msg-info">
                    <div class="msg-info-name">iva_ivan_05</div>
                  </div>
                </div>
                <div class="msg-text">
                  Очень оригинальный комментарий школьника. Наверное, ему все понравилось))!!!
                </div>
              </div>

              <div class="msg left-msg">
                <div class="msg-bubble">
                  <div class="msg-img" style="background-image: url(../img/avatar4.jpg)"></div>
                  <div class="msg-info">
                    <div class="msg-info-name">iva_ivan_05</div>
                  </div>
                </div>
                <div class="msg-text">
                  Очень оригинальный комментарий школьника. Наверное, ему все понравилось))!!!
                </div>
              </div>
            </div>

            <form class="msger-inputarea">
              <input type="text" class="msger-input" placeholder="Оставить комментарий" />
              <button type="submit" class="msger-send-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="13" viewBox="0 0 15 13" fill="none">
                  <path
                    d="M0.113082 4.14044C-0.0908484 3.90515 -0.0111819 3.65266 0.289452 3.57689L14.4027 0.0199586C14.7041 -0.0559868 14.8065 0.0915402 14.6325 0.34813L6.46306 12.3937C6.28863 12.6509 6.02471 12.6391 5.87317 12.3667L3.69338 8.44784C3.54202 8.17573 3.25426 7.76469 3.05008 7.52911L0.113082 4.14044ZM3.73936 6.17022C3.84156 6.28815 4.04184 6.33797 4.18795 6.28101L7.32291 5.05886C7.6129 4.94581 7.68245 5.0426 7.47605 5.27757L5.25538 7.80546C5.15229 7.92282 5.12973 8.12767 5.20606 8.2649L6.12871 9.92365C6.20457 10.06 6.33678 10.0663 6.42289 9.93935L11.5451 2.38686C11.6317 2.25916 11.5797 2.18642 11.431 2.22391L2.58206 4.45408C2.43244 4.49179 2.39336 4.61723 2.4962 4.73589L3.73936 6.17022Z"
                    fill="#101010"
                  />
                </svg>
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="popup-bg add-to-map__popup">
      <div class="popup-map--content">
        <i class="close material-icons">
          close
        </i>
        <h2>Добавить на карту</h2>
        <div class="roting-box">
          <div class="left-box">
            <div class="img-box">
              <svg xmlns="http://www.w3.org/2000/svg" width="109" height="112" viewBox="0 0 109 112" fill="none">
                <path
                  d="M36.6201 98.0809L37.0001 100.721L43.5901 99.7709L43.2001 97.1409L36.6201 98.0809Z"
                  fill="#101010"
                />
                <path d="M23.46 99.8509L23.8 102.491L30.4 101.621L30 98.9909L23.46 99.8509Z" fill="#101010" />
                <path
                  d="M32.1102 52.6009V52.4209L29.4502 52.5509C29.4502 52.6509 29.4502 52.7509 29.4502 52.8609C29.6602 55.0109 30.9802 57.0509 33.3502 58.9209L35.0002 56.8309C33.2302 55.4209 32.2502 54.0009 32.1102 52.6009Z"
                  fill="#101010"
                />
                <path
                  d="M75.7701 28.4209L74.9401 25.8909C72.7801 26.6042 70.6735 27.3109 68.6201 28.0109L69.4801 30.5309C71.5201 29.8575 73.6168 29.1542 75.7701 28.4209Z"
                  fill="#101010"
                />
                <path
                  d="M63.2296 32.7709L62.3196 30.2709C60.1529 31.0375 58.0662 31.8209 56.0596 32.6209L57.0596 35.1009C59.0196 34.3209 61.0762 33.5442 63.2296 32.7709Z"
                  fill="#101010"
                />
                <path
                  d="M88.4303 24.4209L87.6403 21.8809L81.2803 23.8809L82.0903 26.4209L88.4303 24.4209Z"
                  fill="#101010"
                />
                <path
                  d="M50.9198 37.6009L49.8598 35.1609C47.6298 36.1609 45.5798 37.0809 43.7598 38.0009L44.9998 40.4209C46.7298 39.4809 48.7398 38.5509 50.9198 37.6009Z"
                  fill="#101010"
                />
                <path
                  d="M39.2899 43.5809L37.8499 41.3509C35.8674 42.5752 34.0436 44.0396 32.4199 45.7109L34.3699 47.5209C35.8453 46.0142 37.4972 44.6913 39.2899 43.5809Z"
                  fill="#101010"
                />
                <path
                  d="M39.3701 62.4209C41.2001 63.2609 43.2601 64.0809 45.6801 64.9209L46.5601 62.4209C44.2201 61.6109 42.2401 60.8209 40.4901 60.0209L39.3701 62.4209Z"
                  fill="#101010"
                />
                <path
                  d="M88.3096 87.5609L89.3096 90.0409C91.5232 89.2458 93.6035 88.1196 95.4796 86.7009L93.7896 84.6409C92.1172 85.8796 90.2705 86.8637 88.3096 87.5609Z"
                  fill="#101010"
                />
                <path
                  d="M49.75 96.1409L50.16 98.7709C52.46 98.4109 54.65 98.0543 56.73 97.7009L56.29 95.0809C54.2167 95.4276 52.0367 95.7809 49.75 96.1409Z"
                  fill="#101010"
                />
                <path
                  d="M75.7695 91.2609L76.3795 93.8509C78.7395 93.3009 80.9195 92.7309 82.8695 92.1709L82.1395 89.6109C80.2295 90.1609 78.0895 90.7209 75.7695 91.2609Z"
                  fill="#101010"
                />
                <path
                  d="M91.5098 75.1509L90.5098 77.6009C93.7698 78.9709 94.9598 80.0309 95.3798 80.6909L97.6098 79.2509C96.6498 77.7909 94.7198 76.4909 91.5098 75.1509Z"
                  fill="#101010"
                />
                <path
                  d="M77.9404 73.6609C80.4204 74.3109 82.5004 74.9009 84.3104 75.4509L85.0804 72.9009C83.2404 72.3409 81.0804 71.7509 78.6104 71.0909L77.9404 73.6609Z"
                  fill="#101010"
                />
                <path
                  d="M52.0996 66.9609C54.0996 67.5309 56.1596 68.1209 58.5596 68.7509L59.2396 66.1809C56.8696 65.5509 54.7796 64.9709 52.8496 64.4109L52.0996 66.9609Z"
                  fill="#101010"
                />
                <path
                  d="M62.8203 93.9309L63.3003 96.5509C65.6403 96.1242 67.8236 95.7009 69.8503 95.2809L69.3203 92.6709C67.3003 93.0809 65.1403 93.5009 62.8203 93.9309Z"
                  fill="#101010"
                />
                <path
                  d="M65.669 67.8288L65.0264 70.41L71.4988 72.0214L72.1414 69.4402L65.669 67.8288Z"
                  fill="#101010"
                />
                <path
                  d="M10.5 81C4.695 81 0 85.8515 0 91.85C0 99.9875 10.5 112 10.5 112C10.5 112 21 99.9875 21 91.85C21 85.8515 16.305 81 10.5 81ZM10.5 95.725C8.43 95.725 6.75 93.989 6.75 91.85C6.75 89.711 8.43 87.975 10.5 87.975C12.57 87.975 14.25 89.711 14.25 91.85C14.25 93.989 12.57 95.725 10.5 95.725Z"
                  fill="#F1795C"
                />
                <path
                  d="M98.5 0C92.695 0 88 4.8515 88 10.85C88 18.9875 98.5 31 98.5 31C98.5 31 109 18.9875 109 10.85C109 4.8515 104.305 0 98.5 0ZM98.5 14.725C96.43 14.725 94.75 12.989 94.75 10.85C94.75 8.711 96.43 6.975 98.5 6.975C100.57 6.975 102.25 8.711 102.25 10.85C102.25 12.989 100.57 14.725 98.5 14.725Z"
                  fill="#F1795C"
                />
              </svg>
            </div>
            <span class="button-routing">
              Маршрут
            </span>
          </div>
          <div class="right-box">
            <div class="img-box">
              <svg xmlns="http://www.w3.org/2000/svg" width="98" height="92" viewBox="0 0 98 92" fill="none">
                <path
                  d="M81.5 60C75.695 60 71 64.8515 71 70.85C71 78.9875 81.5 91 81.5 91C81.5 91 92 78.9875 92 70.85C92 64.8515 87.305 60 81.5 60ZM81.5 74.725C79.43 74.725 77.75 72.989 77.75 70.85C77.75 68.711 79.43 66.975 81.5 66.975C83.57 66.975 85.25 68.711 85.25 70.85C85.25 72.989 83.57 74.725 81.5 74.725Z"
                  fill="#F1795C"
                />
                <path
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                  d="M71.949 49V22H67.949V11H63.949V0H41.949V11H37.949V22H33.949V41H11.949V50H14.949V89H8.03895V84C9.02898 83.7698 9.95235 83.3138 10.7368 82.6675C11.5214 82.0212 12.1457 81.2022 12.5611 80.2745C12.9766 79.3469 13.1718 78.3357 13.1315 77.32C13.0912 76.3044 12.8165 75.3119 12.329 74.42C11.8902 73.6462 11.6658 72.7694 11.679 71.88C11.5496 70.611 10.9531 69.4354 10.0052 68.5817C9.05735 67.7281 7.82595 67.2574 6.55036 67.2611C5.27478 67.2649 4.04615 67.7428 3.10332 68.602C2.1605 69.4612 1.57083 70.6403 1.44895 71.91C1.46208 72.7994 1.2377 73.6762 0.798951 74.45C0.317021 75.3374 0.0454594 76.3236 0.00523186 77.3326C-0.0349957 78.3416 0.157187 79.3463 0.566943 80.2692C0.976698 81.1922 1.59307 82.0086 2.36846 82.6554C3.14385 83.3023 4.05753 83.7623 5.03895 84V89H0.948951V92H97.949V89H85.859H79.9689H71.9689L71.989 74.51L71.929 67.11V53.48L71.949 49ZM60.949 3H44.949V11H60.949V3ZM64.949 14H40.949V22H64.949V14ZM3.38895 75.89C3.10059 76.4174 2.9493 77.0089 2.94895 77.61C2.94048 78.162 3.0594 78.7084 3.29648 79.207C3.53356 79.7055 3.88241 80.1426 4.31592 80.4844C4.74942 80.8261 5.2559 81.0633 5.79599 81.1775C6.33607 81.2917 6.89521 81.2798 7.42995 81.1427C7.96468 81.0056 8.4606 80.7471 8.87916 80.3872C9.29772 80.0273 9.62764 79.5757 9.84328 79.0675C10.0589 78.5593 10.1545 78.0083 10.1225 77.4572C10.0905 76.9061 9.93191 76.3698 9.65895 75.89C8.98185 74.6669 8.63707 73.2878 8.65895 71.89C8.62191 71.349 8.38087 70.8423 7.98455 70.4722C7.58824 70.1021 7.06621 69.8962 6.52395 69.8962C5.9817 69.8962 5.45966 70.1021 5.06335 70.4722C4.66703 70.8423 4.426 71.349 4.38895 71.89C4.41084 73.2878 4.06605 74.6669 3.38895 75.89ZM33.989 44H14.989V47H33.989V44ZM17.989 50V89H33.989V50H17.989ZM36.989 25V89H68.989V25H36.989Z"
                  fill="#101010"
                />
                <path d="M43.9492 29H40.9492V85H43.9492V29Z" fill="#101010" />
                <path d="M57.9492 29H54.9492V85H57.9492V29Z" fill="#101010" />
                <path d="M50.9492 29H47.9492V85H50.9492V29Z" fill="#101010" />
                <path d="M64.9492 29H61.9492V85H64.9492V29Z" fill="#101010" />
                <path d="M29.9492 54.04H26.9492V56.95H29.9492V54.04Z" fill="#101010" />
                <path d="M24.9492 54.04H21.9492V56.95H24.9492V54.04Z" fill="#101010" />
                <path d="M29.9492 61.09H26.9492V64H29.9492V61.09Z" fill="#101010" />
                <path d="M24.9492 61.09H21.9492V64H24.9492V61.09Z" fill="#101010" />
                <path d="M29.9492 68H26.9492V70.91H29.9492V68Z" fill="#101010" />
                <path d="M24.9492 68H21.9492V70.91H24.9492V68Z" fill="#101010" />
                <path d="M29.9492 75H26.9492V77.91H29.9492V75Z" fill="#101010" />
                <path d="M24.9492 75H21.9492V77.91H24.9492V75Z" fill="#101010" />
                <path d="M29.9492 82H26.9492V84.91H29.9492V82Z" fill="#101010" />
                <path d="M24.9492 82H21.9492V84.91H24.9492V82Z" fill="#101010" />
              </svg>
            </div>
            <span class="button-routing">
              Памятник культуры
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="popup-bg ">
      <div class="add-monument--popup">
        <i class="close material-icons">
          close
        </i>
        <h2>Добавить памятник культуры</h2>
        <form action="#" id="add-monument--form">
          <div class="add-monument--popup-content">
            <div class="left-box">
              <div class="input-box">
                <p class="required-el">Название памятника культуры</p>
                <input required type="text" class="object_title" placeholder="Название памятника культуры" />
              </div>
              <div class="input-box">
                <p class="required-el">Категория</p>
                <select class="route-category">
					<?php foreach ($arResult['SECTIONS'] as $key => $name) : ?>
						<option name="<?=$key?>"><?=$name?></option>
					<?php endforeach; ?>
				</select>
              </div>
              <div class="input-box">
                <p class="required-el">Адрес</p>
                <input required type="text" class="object_address" placeholder="Адрес памятника культуры" />
                <a href="#" id="set_in_map">Указать на карте</a>
              </div>
              <div class="input-box">
                <p class="required-el">Описание</p>
                <textarea placeholder="Описание" class="object_describe"></textarea>
              </div>
              <span>* — поля, обязательные для заполнения</span>
            </div>
            <div class="right-box">
              <p>
                Добавить фотографии
              </p>
              <div id="drag-drop-area"></div>
              <div class="uploaded-files">
				  <h5>Файл загружены:</h5>
				  <ol></ol>
				</div>
              <input type="hidden" name="uppy" value=""/>
            </div>
          </div>
          <button type="submit">Добавить</button>
        </form>
      </div>
    </div>

    <div class="popup-bg">
      <div class="monument-added">
        <i class="close material-icons">
          close
        </i>
        <span class="title">
          Памятник культуры добавлен
        </span>
        <p>
          Добавленный Вами памятник культуры появится на карте после прохождения модерации
        </p>
        <span class="button">
          Понятно
        </span>
      </div>
    </div>

    <div class="popup-bg">
      <div class="add-route--popup">
        <i class="close material-icons">
          close
        </i>
        <h2>Добавить маршрут</h2>
        <div class="add-route--popup__content">
          <form action="/">
            <div class="left-box">
              <div class="input-box">
                <p class="required-el">Название маршрута</p>
                <input required type="text" placeholder="Название маршрута" />
              </div>
              <div class="input-box">
                <p class="required-el">Точки маршрута</p>
                <input required type="text" placeholder="Название памятника культуры" />
                <a href="#">Указать на карте</a>
              </div>
              <div class="input-box">
                <span></span>
                <input required type="text" placeholder="Название памятника культуры" />
                <a href="#">Указать на карте</a>
              </div>
              <span class="added-input--route">
                + Добавать точку маршрута
              </span>
            </div>
            <div class="right-box">
              <div class="input-box">
                <p class="required-el">Описание</p>
                <textarea placeholder="Описание"></textarea>
              </div>
              <p>
                Добавить фотографии
              </p>
              <div class="load-img-box">
                <p>
                  Перетащите файл сюда или
                  <label><input value="" type="file" />загрузите с компьютера</label>
                </p>
              </div>
            </div>
            <span class="explanation-filling">* — поля, обязательные для заполнения</span>
            <button class="submit-form-popup" type="submit">Добавить</button>
          </form>
        </div>
      </div>
    </div>

    <div class="popup-bg">
      <div class="info-router--content">
        <i class="close material-icons">
          close
        </i>
        <div class="info-router--header">
          <div class="title-popup">
            <h2>Название маршрута</h2>
            <span>5,8 км</span>
          </div>
          <div class="edits-this--popup">
            Редактировать маршрут
          </div>
        </div>
        <div class="header-popup">
          <div class="avatar">
            <img src="img/avatar3.jpg" alt="" />
            <span>iva_ivan_05</span>
          </div>
          <div class="input-box">
            <span>132</span>
            <input type="checkbox" id="likeInfoPopup" />
            <label for="likeInfoPopup">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 16 14" fill="none">
                <path
                  d="M8 14L6.84 12.9929C2.72 9.42997 0 7.08011 0 4.19619C0 1.84632 1.936 0 4.4 0C5.792 0 7.128 0.617984 8 1.59455C8.872 0.617984 10.208 0 11.6 0C14.064 0 16 1.84632 16 4.19619C16 7.08011 13.28 9.42997 9.16 13.0005L8 14Z"
                  fill="#BDBDBD"
                />
              </svg>
              <p>Мне нравится</p>
            </label>
          </div>
        </div>
        <div class="left-box--container">
          <div class="left-el">
            <div class="left-box--content">
              <div class="text-block">
                <p>
                  Обсценная идиома, несмотря на то, что все эти характерологические черты отсылают не к единому образу
                  нарратора, редуцирует гекзаметр, так как в данном случае роль наблюдателя опосредована ролью
                  рассказчика.
                </p>
              </div>
              <div class="info-slider-container">
                <div class="popup--info-slider swiper-container">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide"><img src="img/news.jpg" alt="" /></div>
                    <div class="swiper-slide"><img src="img/news.jpg" alt="" /></div>
                    <div class="swiper-slide"><img src="img/news.jpg" alt="" /></div>
                    <div class="swiper-slide"><img src="img/news.jpg" alt="" /></div>
                  </div>
                </div>
                <div class="popup--info--pagination swiper-pagination"></div>
                <div class="popup--info--prev swiper-button-prev"></div>
                <div class="popup--info--next swiper-button-next"></div>
              </div>
              <ul class="info-list--router">
                <li>
                  <h5>Название точки маршрута</h5>
                  <p>ул. Вавилова, 48</p>
                  <span>1,2 км</span>
                </li>
                <li>
                  <h5>Название точки маршрута</h5>
                  <p>ул. Вавилова, 48</p>
                  <span>1,2 км</span>
                </li>
                <li>
                  <h5>Название точки маршрута</h5>
                  <p>ул. Вавилова, 48</p>
                  <span>1,2 км</span>
                </li>
                <li>
                  <h5>Название точки маршрута</h5>
                  <p>ул. Вавилова, 48</p>
                  <span>1,2 км</span>
                </li>
              </ul>
            </div>
            <div class="share">
              <p><i class="material-icons">reply</i>Поделиться</p>
              <div class="social-links">
                <a href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26" fill="none">
                    <path
                      d="M0 0V26H26V0H0ZM18.9649 14.4883L21.2229 17.0828C21.5116 17.4145 21.276 17.9319 20.8363 17.9319H18.2881C17.8854 17.9319 17.4976 17.7795 17.2027 17.5053L16.4481 16.8036C15.5966 16.0585 15.2346 15.803 14.894 15.803C14.5534 15.803 14.4682 16.0584 14.4682 16.7184C14.4682 17.3783 14.3405 17.9319 12.3819 17.9319C10.4234 17.9319 8.8906 16.463 7.16624 14.1852C5.44189 11.9073 4.67549 9.45909 4.67549 9.45909C4.58035 9.15267 4.80942 8.84174 5.13031 8.84174H7.16945C7.55731 8.84174 7.91056 9.06588 8.0732 9.41795C8.24013 9.77934 8.52727 10.337 9.01842 11.1621C9.71538 12.333 10.3849 12.8604 10.7642 13.085C10.9054 13.1687 11.0835 13.0653 11.0835 12.9012V10.0927C11.0835 9.72524 10.9507 9.37016 10.7096 9.09288L10.4221 8.76234C10.3477 8.67684 10.4085 8.54367 10.5217 8.54367H13.7445C14.2383 8.54367 14.6386 8.94395 14.6386 9.43777V12.7038C14.6386 12.9142 14.9117 12.9965 15.0279 12.8212L17.3475 9.32549C17.5304 9.04992 17.8392 8.88417 18.1698 8.88417H20.8926C21.2647 8.88417 21.491 9.29399 21.2929 9.60899L18.8836 13.4384C18.6763 13.7681 18.7093 14.1945 18.9649 14.4883Z"
                      fill="#44678D"
                    ></path>
                  </svg>
                </a>
                <a href="#">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    aria-hidden="true"
                    focusable="false"
                    data-prefix="fab"
                    data-icon="facebook-f"
                    class="svg-inline--fa fa-facebook-f fa-w-10"
                    role="img"
                    viewBox="0 0 320 512"
                  >
                    <path
                      fill="currentColor"
                      d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"
                    ></path>
                  </svg>
                </a>
                <a href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26" fill="none">
                    <path d="M26 0H0V26H26V0Z" fill="#50ABF1"></path>
                    <path
                      d="M3.44457 19.2731C5.17493 20.3706 7.22752 21.0057 9.42781 21.0057C15.6093 21.0057 20.6201 15.9942 20.6201 9.81337C20.6201 9.65905 20.6169 9.50586 20.6109 9.35314C20.8345 9.17719 22.2173 8.06886 22.5563 7.35635C22.5563 7.35635 21.4333 7.82303 20.3343 7.93296C20.3321 7.93296 20.3294 7.93348 20.3273 7.93348C20.3273 7.93348 20.3294 7.93239 20.3332 7.92971C20.4345 7.86205 21.8503 6.89828 22.0419 5.74996C22.0419 5.74996 21.2477 6.1739 20.1356 6.54479C19.9515 6.60595 19.7588 6.66608 19.56 6.72182C18.8432 5.95677 17.8243 5.47925 16.6932 5.47925C14.5237 5.47925 12.7658 7.23671 12.7658 9.40454C12.7658 9.71045 12.801 10.0082 12.8675 10.2941C12.5644 10.2816 7.94762 10.0228 4.80683 6.21932C4.80683 6.21932 2.92974 8.78188 5.92544 11.417C5.92544 11.417 5.01479 11.3807 4.22913 10.9113C4.22913 10.9113 3.94057 14.0158 7.33312 14.8095C7.33312 14.8095 6.66556 15.0624 5.60059 14.8821C5.60059 14.8821 6.19614 17.3905 9.21027 17.6249C9.21027 17.6249 6.82685 19.778 3.44348 19.2729L3.44457 19.2731Z"
                      fill="white"
                    ></path>
                  </svg>
                </a>
              </div>
            </div>
          </div>

          <div class="right-box--content">
            <div class="msger">
              <div class="msger-chat">
                <div class="msg left-msg">
                  <div class="msg-bubble">
                    <div class="msg-img" style="background-image: url('../img/avatar4.jpg')"></div>
                    <div class="msg-info">
                      <div class="msg-info-name">iva_ivan_05</div>
                    </div>
                  </div>
                  <div class="msg-text">
                    Очень оригинальный комментарий школьника. Наверное, ему все понравилось))!!!
                  </div>
                </div>

                <div class="msg right-msg">
                  <div class="msg-bubble">
                    <div class="msg-img" style="background-image: url(../img/avatar3.jpg)"></div>
                    <div class="msg-info">
                      <div class="msg-info-name">iva_ivan_05</div>
                    </div>
                  </div>
                  <div class="msg-text">
                    Еще один очень оригинальный комментарий школьника. Ему тоже все понравилось, пойдет глазеть еще)
                  </div>
                </div>
                <div class="msg left-msg">
                  <div class="msg-bubble">
                    <div class="msg-img" style="background-image: url(../img/avatar4.jpg)"></div>
                    <div class="msg-info">
                      <div class="msg-info-name">iva_ivan_05</div>
                    </div>
                  </div>
                  <div class="msg-text">
                    Очень оригинальный комментарий школьника. Наверное, ему все понравилось))!!!
                  </div>
                </div>
                <div class="msg left-msg">
                  <div class="msg-bubble">
                    <div class="msg-img" style="background-image: url(../img/avatar4.jpg)"></div>
                    <div class="msg-info">
                      <div class="msg-info-name">iva_ivan_05</div>
                    </div>
                  </div>
                  <div class="msg-text">
                    Очень оригинальный комментарий школьника. Наверное, ему все понравилось))!!!
                  </div>
                </div>
                <div class="msg right-msg">
                  <div class="msg-bubble">
                    <div class="msg-img" style="background-image: url(../img/avatar3.jpg)"></div>
                    <div class="msg-info">
                      <div class="msg-info-name">iva_ivan_05</div>
                    </div>
                  </div>
                  <div class="msg-text">
                    Еще один очень оригинальный комментарий школьника. Ему тоже все понравилось, пойдет глазеть еще)
                  </div>
                </div>
                <div class="msg left-msg">
                  <div class="msg-bubble">
                    <div class="msg-img" style="background-image: url(../img/avatar4.jpg)"></div>
                    <div class="msg-info">
                      <div class="msg-info-name">iva_ivan_05</div>
                    </div>
                  </div>
                  <div class="msg-text">
                    Очень оригинальный комментарий школьника. Наверное, ему все понравилось))!!!
                  </div>
                </div>
                <div class="msg left-msg">
                  <div class="msg-bubble">
                    <div class="msg-img" style="background-image: url(../img/avatar4.jpg)"></div>
                    <div class="msg-info">
                      <div class="msg-info-name">iva_ivan_05</div>
                    </div>
                  </div>
                  <div class="msg-text">
                    Очень оригинальный комментарий школьника. Наверное, ему все понравилось))!!!
                  </div>
                </div>
              </div>

              <form class="msger-inputarea">
                <textarea class="msger-input txtstuff" placeholder="Оставить комментарий"></textarea>
                <button type="submit" class="msger-send-btn">
                  <svg xmlns="http://www.w3.org/2000/svg" width="15" height="13" viewBox="0 0 15 13" fill="none">
                    <path
                      d="M0.113082 4.14044C-0.0908484 3.90515 -0.0111819 3.65266 0.289452 3.57689L14.4027 0.0199586C14.7041 -0.0559868 14.8065 0.0915402 14.6325 0.34813L6.46306 12.3937C6.28863 12.6509 6.02471 12.6391 5.87317 12.3667L3.69338 8.44784C3.54202 8.17573 3.25426 7.76469 3.05008 7.52911L0.113082 4.14044ZM3.73936 6.17022C3.84156 6.28815 4.04184 6.33797 4.18795 6.28101L7.32291 5.05886C7.6129 4.94581 7.68245 5.0426 7.47605 5.27757L5.25538 7.80546C5.15229 7.92282 5.12973 8.12767 5.20606 8.2649L6.12871 9.92365C6.20457 10.06 6.33678 10.0663 6.42289 9.93935L11.5451 2.38686C11.6317 2.25916 11.5797 2.18642 11.431 2.22391L2.58206 4.45408C2.43244 4.49179 2.39336 4.61723 2.4962 4.73589L3.73936 6.17022Z"
                      fill="#101010"
                    />
                  </svg>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="popup-bg">
      <div class="popup-edits--route">
        <i class="close material-icons">
          close
        </i>
        <h2>Редактировать маршрут</h2>
        <form action="/">
          <div class="popup-edits--content">
            <div class="left-box">
              <div class="input-box">
                <p>Название маршрута</p>
                <input required type="text" placeholder="Название памятника культуры" />
              </div>
              <div class="bottom-scroll">
                <div class="input-box">
                  <p>Точки маршрута</p>
                  <input required type="text" placeholder="Название маршрута" />
                  <span>1,2 км</span>
                </div>
                <div class="input-box">
                  <input required type="text" placeholder="Название маршрута" />
                  <span>1,2 км</span>
                </div>
                <div class="input-box">
                  <input required type="text" placeholder="Название маршрута" />
                  <span>1,2 км</span>
                </div>
                <div class="input-box">
                  <input required type="text" placeholder="Название маршрута" />
                  <span>1,2 км</span>
                </div>
                <div class="input-box">
                  <input required type="text" placeholder="Название памятника культуры" />
                  <span>1,2 км</span>
                </div>
              </div>
            </div>
            <div class="right-box">
              <div class="input-box">
                <p>Описание</p>
                <textarea placeholder="Описание"></textarea>
              </div>

              <div class="popup-edits--sliders">
                <p>
                  Фотографии
                </p>
                <div class="edits--sliders swiper-container">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide">
                      <span class="delete-slide"> <i class=" material-icons"> close </i></span>
                      <img src="img/news.jpg" alt="" />
                    </div>

                    <div class="swiper-slide">
                      <span class="delete-slide"> <i class=" material-icons"> close </i></span>
                      <img src="img/news.jpg" alt="" />
                    </div>

                    <div class="swiper-slide">
                      <span class="delete-slide"> <i class=" material-icons"> close </i></span>
                      <img src="img/news.jpg" alt="" />
                    </div>

                    <div class="swiper-slide">
                      <input type="file" id="editsSliders" hidden />
                      <label for="editsSliders"
                        ><p>
                          <span>+</span>
                          Добавить фото
                        </p></label
                      >
                    </div>
                  </div>
                </div>
                <div class="edits--sliders--pagination swiper-pagination"></div>
                <div class="edits--sliders--prev swiper-button-prev"></div>
                <div class="edits--sliders--next swiper-button-next"></div>
              </div>
            </div>
          </div>
          <button type="submit">Сохранить</button>
        </form>
      </div>
    </div>