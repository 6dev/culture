<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;
use Custom\Culture\CultureHelpers;
global $USER;


Loader::includeModule('iblock');

class Mentors extends CBitrixComponent
{

    private $params = array();

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "LIMIT" => $arParams["LIMIT"],
            "FILTER" => $arParams["FILTER"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
        );
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        if ($this->params['TYPE'] == 'LIST') {
            $this->arResult["fields"] = array();
            $this->arResult["fields"]["list"] = array();
            $this->arResult["fields"]["filters"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"]["list"] = $this->getList();
                $this->arResult["fields"]["filters"] = $this->getFilters();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        if ($this->params['TYPE'] == 'DETAIL') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getDetail();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        return null;
    }


    public function getDetail() {
        $uid = $this->params['UID'];
        $fields = array();
        $query = new \Bitrix\Main\Entity\Query(Bitrix\Main\UserTable::getEntity());
        $query->registerRuntimeField('USER', [
                'data_type' => 'Bitrix\Main\UserTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );

        $query->setSelect(Array(
            'NAME',
            'ID',
            'EMAIL',
            'PERSONAL_PHOTO',
            'SECOND_NAME',
            'LAST_NAME',
            'PERSONAL_STATE',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
            'WORK_PROFILE',
            'UF_SCHOOL',
            'UF_INTERESTS',
        ));
        $filter = Array("ID"=>$uid);
        $query->setFilter($filter);
        $db_res = $query->exec();
        $fields['POSTS'] = array();
        if ($fields['USER'] = $db_res->fetch()) {
            global $APPLICATION;

            $fio = $fields['USER']['LAST_NAME'] . ' ' . $fields['USER']['NAME'].' '.$fields['USER']['SECOND_NAME'];
            if (!empty($this->params['SET_TITLE']) && ($this->params['SET_TITLE'] == 'Y')) Custom\Culture\CultureHelpers::setPageTitle($fio);
            $fields['POSTS'] = $this->getBlockItems(Array("*", "PROPERTY_*"), Array("IBLOCK_ID"=>2, "=PROPERTY_UID" => $uid));
            $fields['USER']['INTERESTS'] = array();
            foreach ($fields['USER']['UF_INTERESTS'] as $item) {
                $cItem = Custom\Culture\CultureHelpers::getCultureCategory($item);
                if (!empty($cItem['NAME'])) $fields['USER']['INTERESTS'][] = $cItem['NAME'];
            }
        }

        return $fields;
    }

    public static function _getList() {
        $query = new \Bitrix\Main\Entity\Query(Bitrix\Main\UserTable::getEntity());
        $query->registerRuntimeField('USER', [
                'data_type' => 'Bitrix\Main\UserTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );

        $query->setSelect(Array(
            'NAME',
            'ID',
            'EMAIL',
            'SECOND_NAME',
            'LAST_NAME',
        ));
        $filter = Array("@ID"=>new \Bitrix\Main\DB\SqlExpression(self::subQueryUserGroup(7)));
        $query->setFilter($filter);
        $db_res = $query->exec();
        $list = array();
        while ($dbItem = $db_res->fetch()) {
            $list[] = $dbItem;
        }
        return $list;
    }

    public function getList() {
        if (!empty($this->params['SET_TITLE']) && ($this->params['SET_TITLE'] == 'Y')) Custom\Culture\CultureHelpers::setPageTitle('Наставники');
        return Custom\Culture\CultureMentors::getList($this->params);
    }

    private function getBlockItems($arSelect=array(), $arFilter=array()) {
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        $list = array();
        while($ob = $res->GetNextElement()){
            $item = array();
            $item['FIELDS'] = $ob->GetFields();
            $item['PROPERTIES'] = $ob->GetProperties();
            $list[] = $item;
        }
        return $list;
    }

    private static function subQueryUserGroup($group) {
        $query = new \Bitrix\Main\Entity\Query(Bitrix\Main\UserGroupTable::getEntity());
        $query->setSelect(Array("D_USER_ID"));
        $query->setFilter(Array("GROUP_ID"=>$group));
        $query->registerRuntimeField(0, new \Bitrix\Main\Entity\ExpressionField('D_USER_ID', 'DISTINCT(USER_ID)'));
        return $query->getQuery();
    }

    private function getFilters() {
        $filters['city'] = CultureHelpers::getCities();
        $filters['interests'] = CultureHelpers::getCultureCategoryList();
        return $filters;
    }


}?>