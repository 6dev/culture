$( document ).ready(function() {

    const limitMessages = 4;

    function getPostList() {
        $.ajax({
            type: 'POST',
            url: '/mentors/ajax.php',
            dataType: "json",
            data: $("#search_post").serialize(),
            success: function(result) {
                if (result.hasOwnProperty('data') && result.data.hasOwnProperty('list_html')) {
                    $("#post_list").html(result.data.list_html);
                }

                $("input[name=post_page]").val('');
            },
            error:  function(xhr, str){
                $("input[name=post_page]").val('');
            }
        });
    }

    $('#sort_param').on('change', function() {
        $("input[name=sort]").val($(this).val());
        getPostList();
    });

    $("#search_post").on("submit", function() {
        getPostList();
        return false;
    });

    $("body").on("click", ".post_page_link", function() {
        $("input[name=post_page]").val($(this).val());
        getPostList();
    });

    function messagesRequest() {
        this.state = {send: 0, clearHtml: 0, items_count: 0};
        this.param = {
            data: {
                action: 'chat_list',
                mentor_id: $("#dst_user_id").val(),
                offset: 0,
                limit: limitMessages
            }
        };
        this.callback_success;
        this.callback_error;
        this.request = function (param, state) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: '/mentors/ajax.php',
                    dataType: "json",
                    data: param.data,
                    success: (function (response) {
                        if (response.hasOwnProperty('data') && response.data.hasOwnProperty('list_html')) {
                            if (response.data.hasOwnProperty('items_count')) {
                                state.items_count = parseInt(response.data.items_count, 10);
                            }
                            let h = $("#messages_list").html();
                            if (state.clearHtml == 1) h = response.data.list_html;
                            else h += response.data.list_html;
                            $("#messages_list").html(h);
                            $("#message").val('');
                            console.log({of: param.data.offset, cnt: state.items_count});
                            param.data.offset += limitMessages;
                            if ((param.data.offset >= state.items_count) || (state.items_count == 0))  {
                                $("#news__load_btn").hide();
                            }
                            else $("#news__load_btn").show();

                        }
                        state.clearHtml = 0;
                        state.send = 0;
                    }),
                    error: (function (error) {
                        state.send = 0;
                        state.clearHtml = 0;
                        reject(error);
                    })
                });
            });
        };
        this.getList = function (param) {
            if (this.state.send == 0) {
                this.state.send = 1;
                if (param.hasOwnProperty('offset')) this.param.data.offset = param.offset;
                if (param.hasOwnProperty('clearHtml')) this.state.clearHtml = param.clearHtml;
                this.request(this.param, this.state)
                    .then(this.callback_success,this.callback_error);
            }
            else console.log('send proccess');
        };
    };

    let messages = new messagesRequest();

    messages.callback_error = function (e) {
        console.log(e);
    };



    $("#send_message").on("submit", function() {
        $.ajax({
            type: 'POST',
            url: '/mentors/ajax.php',
            dataType: "json",
            data: $(this).serialize(),
            success: function(result) {
                $("#ms_send_result").html("Отправлено на модерацию");
                messages.getList({offset: 0, clearHtml: 1});
            },
            error:  function(xhr, str){
                alert('Ошибка запроса');
            }
        });
        return false;
    });

    $("body").on("click", "#news__load_btn", function() {
        messages.getList({});
    });


    getPostList();
    $("#news__load_btn").hide();
    messages.getList({offset: 0, clearHtml: 1});


});