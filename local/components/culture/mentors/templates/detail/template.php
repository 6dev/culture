<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
    <div class="container">
        <a href="/mentors" class="back--pages_mentor">
            <i class="material-icons arrow_downward__icon">arrow_downward</i> Вернуться к списку наставников
        </a>
    </div>

    <section class="header--mentor-page">
        <div class="container">
            <div class="header--mentor-page__content">
                <div class="header--mentor-page__left">
                    <div class="imgBx">
                        <img src="<?=CFile::GetPath((int)$arResult['fields']['USER']['PERSONAL_PHOTO'])?>" alt="avatar" />
                    </div>
                </div>
                <div class="header--mentor-page__right">
              <span class="name">
                <?=$arResult['fields']['USER']['LAST_NAME']?> <?=$arResult['fields']['USER']['NAME']?> <?=$arResult['fields']['USER']['SECOND_NAME']?>
              </span>
                    <span class="location"><i class="material-icons">location_on</i> г. <?=$arResult['fields']['USER']['PERSONAL_CITY']?>, <?=$arResult['fields']['USER']['UF_SCHOOL']?></span>
                    <hr />
                    <p>
                        <?=$arResult['fields']['USER']['WORK_PROFILE']?>
                    </p>
                    <div class="tag-box">
                        <?foreach($arResult['fields']['USER']['INTERESTS'] as $item):?>
                            <a href="#"><?=$item?></a>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
            <div class="filter-box">
                <div class="city-select">
                    <p>
                        Сортировка по
                    </p>
                    <select id="sort_param">
                        <option value="PROPERTY_VIEWS">популярности</option>
                        <option value="ACTIVE_FROM">дате</option>
                        <option value="">отзывам</option>
                    </select>
                </div>
                <form id="search_post">
                    <input type="search" name="search" placeholder="Поиск по публикациям" />
                    <input type="hidden" name="action" value="search_post">
                    <input type="hidden" name="sort" value="PROPERTY_VIEWS">
                    <input type="hidden" name="mentor" value="<?=$arResult['fields']['USER']['ID']?>">
                    <input type="hidden" name="post_page">
                </form>
            </div>
        </div>
    </section>


<div id="post_list">

</div>


<div class="container">
        <section class="question-mentor">
            <div class="question-mentor--content">
                <h2>Задай вопрос наставнику!</h2>
                <form id="send_message" action="">
                    <div>
                        <textarea id="message" name="message" placeholder="Задай вопрос наставнику!"></textarea>
                    </div>
                    <input type="hidden" id="dst_user_id" name="dst_user_id" value="<?=$arResult['fields']['USER']['ID']?>">
                    <input type="hidden" name="action" value="chat">
                    <button type="submit">Отправить</button>
                </form>
            </div>
            <div id="ms_send_result" style="color: #cf1515; margin-bottom: 20px;"></div>
            <div class="reviews--content" id="messages_list">
            </div>
            <p id="news__load_btn" class="news__load">
                Загрузить ещё
                <i class="material-icons arrow_downward__icon">arrow_downward</i>
            </p>
        </section>
    </div>
