$( document ).ready(function() {

    let mentorsList = null;

    function getLimit() {
        return parseInt($("#page_items_count").val(), 10);
    }

    function getRequestFields() {
        let formData = new FormData(document.getElementById("search_filter"));
        let data = {action: 'list', offset: 0, limit: getLimit()};
        for (let entry of formData.entries()) data[entry[0]] = entry[1];
        return data;
    }

    function listRequest(initData) {
        this.state = {send: 0, clearHtml: 0, items_count: 1};
        this.param = {
            data: initData
        };
        this.callback_success;
        this.callback_error;
        this.request = function (param, state) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: '/mentors/ajax.php',
                    dataType: "json",
                    data: param.data,
                    success: (function (response) {
                        if (response.hasOwnProperty('data') && response.data.hasOwnProperty('list_html')) {
                            if (response.data.hasOwnProperty('items_count')) {
                                state.items_count = parseInt(response.data.items_count, 10);
                            }
                            let h = $("#mentor_list").html();
                            if (state.clearHtml == 1) h = response.data.list_html;
                            else h += response.data.list_html;

                            if (!$('.not-found').hasClass('is-active')) {
                                if (h == '') $('.not-found').addClass('is-active');
                            }
                            else {
                                if (h != '') $('.not-found').removeClass('is-active');
                            }


                            $("#mentor_list").html(h);
                            console.log({of: param.data.offset, cnt: state.items_count});
                            if (((param.data.offset + getLimit()) >= state.items_count) || (state.items_count == 0))  {
                                $("#more_btn").hide();
                            }
                            else $("#more_btn").show();
                            param.data.offset += getLimit();
                        }
                        state.clearHtml = 0;
                        state.send = 0;
                    }),
                    error: (function (error) {
                        state.send = 0;
                        state.clearHtml = 0;
                        reject(error);
                    })
                });
            });
        };
        this.getList = function (param) {
            if (this.state.send == 0) {
                this.state.send = 1;
                if (param.hasOwnProperty('offset')) this.param.data.offset = param.offset;
                if (param.hasOwnProperty('clearHtml')) this.state.clearHtml = param.clearHtml;
                this.request(this.param, this.state)
                    .then(this.callback_success,this.callback_error);
            }
            else console.log('send proccess');
        };
    };

    function updateList() {
        $("#more_btn").hide();
        mentorsList = new listRequest(getRequestFields());
        mentorsList.getList({offset: 0, clearHtml: 1});
    }
	
	$(".interest_select").on("click", function() {
		let data = {};
		data['UF_INTERESTS'] = [];
		data['UF_INTERESTS'].push(parseInt($(this).attr('value'), 10));
		let h = '';
		for (let item of data['UF_INTERESTS']) {
			h += '<input type="hidden" name="CTYPES[]" value="' + item + '">';
		}
		$("#hidden_filters").html(h);
        updateList();
    });

	$('#town').on('change', function() {
        updateList();
    });

    $("body").on("click", "#more_btn", function() {
        if (mentorsList) mentorsList.getList({});
    });

    updateList();
	
});