<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<main class="main mentor-pages">
    <div class="title-pages">
        <h2>Наставники</h2>
    </div>
    <div class="container">
        <div class="mentor-pages--content">
            <div class="header_mentor-pages--content">
                <div class="city-select">
                    <p>
                        Город
                    </p>
                    <form id="search_filter">
                        <select id="town" name="CITY">
                            <option value="" selected>Все города</option>
                            <?foreach($arResult["fields"]['filters']['city'] as $item):?>
                                <option value="<?=$item['NAME']?>"<?if (!empty($_REQUEST['CITY']) && ($_REQUEST['CITY'] == $item['NAME'])):?> selected<?endif;?>><?=$item['NAME']?></option>
                            <?endforeach;?>
                        </select>
                        <div id="hidden_filters"></div>
                    </form>
                    <input type="hidden" id="page_items_count" value="<?=$arParams['LIMIT']?>">
                </div>
            </div>
            <div class="mentor-tab">
                <div class="tabs-menu">
                    <ul>
                        <?foreach($arResult["fields"]['filters']['interests'] as $item):?>
                            <?if (!empty($_REQUEST['CTYPES']) && in_array($item['ID'], $_REQUEST['CTYPES'])):?>
                                <li class="interest_select is-active" value="<?=$item['ID']?>"><?=$item['NAME']?></li>
                            <?else:?>
                                <li class="interest_select" value="<?=$item['ID']?>"><?=$item['NAME']?></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </div>
                <div class="tab-content">
                    <ul id="mentor_list" style="display: flex;">

                    </ul>
                    <p id="more_btn" class="news__load">
                        Загрузить ещё
                        <i class="material-icons arrow_downward__icon">arrow_downward</i>
                    </p>

                    <div class="not-found">
                        <svg xmlns="http://www.w3.org/2000/svg" width="53" height="53" viewBox="0 0 53 53" fill="none">
                            <path d="M42.049 21.5619C42.049 32.9196 32.8582 42.1239 21.5245 42.1239C10.1908 42.1239 1 32.9196 1 21.5619C1 10.2043 10.1908 1 21.5245 1C32.8582 1 42.049 10.2043 42.049 21.5619Z" stroke="#8C57FC" stroke-width="2" stroke-miterlimit="10"></path>
                            <path d="M52 52.0002L36.1111 36.1113" stroke="#8C57FC" stroke-width="2" stroke-miterlimit="10"></path>
                            <path d="M26.7222 17.6297L25.7039 16.6113L21.6666 20.6486L17.6294 16.6113L16.6111 17.6297L20.6483 21.6669L16.6111 25.7041L17.6294 26.7224L21.6666 22.6852L25.7039 26.7224L26.7222 25.7041L22.685 21.6669L26.7222 17.6297Z" fill="#F1795C"></path>
                        </svg>
                        К сожалению, по Вашему запросу ничего не найдено
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>