<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["fields"]['list'] as $item):?>
    <li>
        <a href="/mentors/<?=$item['ID']?>/">
            <div class="img-box">
                <img src="<?=CFile::GetPath((int)$item['PERSONAL_PHOTO'])?>" alt="avatar" />
            </div>
            <p><?=$item['NAME']?> <?=$item['LAST_NAME']?></p>
            <span class="location"> г. <?=$item['PERSONAL_CITY']?>, <?=$item['UF_SCHOOL']?></span>
        </a>
    </li>
<?endforeach;?>