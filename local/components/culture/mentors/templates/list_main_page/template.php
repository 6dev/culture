<div class="mentor--slider-container">
    <div class="mentor-slider swiper-container">
        <div class="mentors-list swiper-wrapper">


            <?foreach($arResult["fields"]['list'] as $item):?>
            <div class="swiper-slide mentors-list__item">
                <a href="/mentors/<?=$item['ID']?>/" target="_blank" style="text-decoration: none;">
                <p class="mentors-list__image-wrap">
                    <img src="<?=CFile::GetPath((int)$item['PERSONAL_PHOTO'])?>" class="mentors-list__image" />
                </p>
                <p class="mentors-list__info">
                    <span class="mentors-list__name">
                      <?=$item['LAST_NAME']?> <?=$item['NAME']?>
                    </span>
                    <span class="mentors-list__address">
                      г. <?=$item['PERSONAL_CITY']?>, <?=$item['UF_SCHOOL']?>
                    </span>
                </p>
                </a>
            </div>
            <?endforeach;?>

        </div>
        <div class="mentor-slider-pagination swiper-pagination"></div>
    </div>
    <div class="swiper-button-component mentor-slider-next swiper-button-next">
        <i class="material-icons">arrow_forward</i>
    </div>
    <div class="swiper-button-component mentor-slider-prev swiper-button-prev">
        <i class="material-icons">arrow_back</i>
    </div>
</div>

