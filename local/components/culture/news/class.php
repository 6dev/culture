<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;
use Custom\Culture\CultureHelpers;
global $USER;

Loader::includeModule('iblock');

class News extends CBitrixComponent
{

    private $params = array();

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "FILTER" => $arParams["FILTER"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
        );
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        if ($this->params['TYPE'] == 'LIST') {
            $this->arResult["fields"] = array();
            //$this->arResult["fields"]["list"] = array();
           // $this->arResult["fields"]["filters"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getList();
                //$this->arResult["fields"]["filters"] = $this->getFilters();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        if ($this->params['TYPE'] == 'DETAIL') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getDetail();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        return null;
    }


    public function getList() {
        $nPageSize = (int)$this->params['nPageSize'];
        $arFilter = (!empty($this->params['FILTER']))? $this->params['FILTER'] : array();
        $page = (!empty($this->params['PAGE']))? (int)$this->params['PAGE'] : 1;
        $sort = (!empty($this->params['SORT']))? $this->params['SORT'] : array();
        $res = CIBlockElement::GetList($sort, $arFilter, false, Array("nPageSize"=>$nPageSize, "iNumPage" =>$page), Array("*", "PROPERTY_*"));
        $items = array();
        $fields = array();


        $res->NavStart();
        $fields['ITEMS_COUNT'] =  $res->NavRecordCount;
        $fields['PAGE_COUNT'] =  $fields['ITEMS_COUNT'] / $nPageSize;
        if ($fields['PAGE_COUNT'] < 1) $fields['PAGE_COUNT'] = 1;
        $fields['CURRENT_PAGE'] =  $page;

        while($ob = $res->GetNextElement()){
            $item = array();
            $item['FIELDS'] = $ob->GetFields();
            $item['FIELDS']["PREVIEW_TEXT"] = substr($item['FIELDS']["PREVIEW_TEXT"], 0, 227).'...';
            $item['PROPERTIES'] = $ob->GetProperties();
            $cat = Custom\Culture\CultureHelpers::getCultureCategory($item['PROPERTIES']['CULTURE_CATEGORY']['VALUE']);
            $item['FIELDS']['CATEGORY'] = $cat['NAME'];
            $item['FIELDS']['CATEGORY_COLOR'] = (!empty($cat['PROPERTY_TAB_COLOR_VALUE']))? '#'.$cat['PROPERTY_TAB_COLOR_VALUE'] : '#5cadff';
            $items[] = $item;
        }
        $fields['ITEMS'] = $items;
        return $fields;
    }


    public function getDetail() {


        $fields = array();
        $query = \Bitrix\Iblock\ElementTable::query();
        $query->registerRuntimeField('ELEMENT', [
                'data_type' => 'Bitrix\Iblock\ElementTable',
                'reference' => [
                    '=this.CODE' => 'ref.CODE',
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ID' => 'ref.IBLOCK_ID',
                ],
            ]
        );
        $f = array('IBLOCK_ID' => 15);
        if (!empty($this->params['CODE'])) $f['CODE'] = $this->params['CODE'];
        else $f['ID'] = (!empty($this->params['ID']))? (int)$this->params['ID'] : 0;
        $query->setFilter($f);
        $query->setSelect([
            'ID',
            'IBLOCK_ID',
            'DETAIL_TEXT',
            'PREVIEW_TEXT',
            'NAME',
            'CODE',
            'DETAIL_PICTURE',
            'PREVIEW_PICTURE',
            'ACTIVE_FROM'
        ]);
        $db_res = $query->exec();
        if ($dbItem = $db_res->fetch()) {
            global $APPLICATION;
            Custom\Culture\CultureHelpers::setPageTitle($dbItem['NAME']);
            $APPLICATION->SetPageProperty("og:type", "article");

            $APPLICATION->SetPageProperty("og:url", SITE_BASE_URL.'/news/detail/'.$dbItem['CODE'].'/');
            $APPLICATION->SetPageProperty("og:title", $dbItem['NAME']);
            $APPLICATION->SetPageProperty("og:description", $dbItem['PREVIEW_TEXT']);
            $fields['ELEMENT'] = $dbItem;
            $fields['ELEMENT']['PAGE_URL'] = SITE_BASE_URL.'/news/detail/'.$dbItem['CODE'].'/';
            //$fields['ELEMENT']['PUB_DATE'] = (!empty($dbItem['ACTIVE_FROM']))? $dbItem['ACTIVE_FROM']->toString(new \Bitrix\Main\Context\Culture(array())) : '';
            $fields['ELEMENT']['IMAGE_SRC'] = (!empty($dbItem['DETAIL_PICTURE']))? CFile::GetPath((int)$dbItem['DETAIL_PICTURE']) : '';
            if (($fields['ELEMENT']['IMAGE_SRC'] == '') && !empty($dbItem['PREVIEW_PICTURE'])) $fields['ELEMENT']['IMAGE_SRC'] = CFile::GetPath((int)$dbItem['PREVIEW_PICTURE']);
            $APPLICATION->SetPageProperty("og:image", SITE_BASE_URL.$fields['ELEMENT']['IMAGE_SRC']);

            $db_props = CIBlockElement::GetProperty(15, $dbItem['ID'], array("sort" => "asc"), Array("CODE"=>"UID"));
            $fields['USER'] = array();
            if ($uid = $db_props->Fetch()) {
                $fields['USER'] = $this->getUser($uid['VALUE']);
            }
        }
        return $fields;


    }

    private function getUser($uid) {
        $query = new \Bitrix\Main\Entity\Query(Bitrix\Main\UserTable::getEntity());
        $query->registerRuntimeField('USER', [
                'data_type' => 'Bitrix\Main\UserTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );

        $query->setSelect(Array(
            'NAME',
            'ID',
            'EMAIL',
            'PERSONAL_PHOTO',
            'SECOND_NAME',
            'LAST_NAME',
            'PERSONAL_STATE',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
            'WORK_PROFILE',
            'UF_SCHOOL',
            'UF_INTERESTS',
        ));
        $query->setFilter(array("ID"=>(int)$uid));
        $db_res = $query->exec();
        return $db_res->fetch();
    }


}?>