<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<main class="main news-page">
    <div class="container">
        <div class="news-page--content">
            <div class="header--news--content">
            <span class="bookmark--news" data-id="<?=$arResult['fields']['ELEMENT']['ID'];?>" data-iblockid="<?=$arResult['fields']['ELEMENT']['IBLOCK_ID'];?>">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px">
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path
                        fill="#BDBDBD"
                        d="M19 18l2 1V3c0-1.1-.9-2-2-2H8.99C7.89 1 7 1.9 7 3h10c1.1 0 2 .9 2 2v13zM15 5H5c-1.1 0-2 .9-2 2v16l7-3 7 3V7c0-1.1-.9-2-2-2z"
                />
              </svg>
            </span>
                <span class="data--news">
              <?=FormatDate("d F Y", MakeTimeStamp($arResult['fields']['ELEMENT']["ACTIVE_FROM"]))?>
            </span>
            </div>
            <h1><?=$arResult['fields']['ELEMENT']["NAME"];?></h1>

            <div class="block-content">
                <p>
                    <?=$arResult['fields']['ELEMENT']["PREVIEW_TEXT"];?>
                </p>
                <?if (!empty($arResult['fields']['ELEMENT']['IMAGE_SRC'])):?>
                    <div class="img-box">
                        <img src="<?=$arResult['fields']['ELEMENT']['IMAGE_SRC']?>" alt="event-image" />
                        <span><?=$arResult["DETAIL_PICTURE"]['DESCRIPTION']?></span>
                    </div>
                <?endif;?>

                <?=$arResult['fields']['ELEMENT']["DETAIL_TEXT"];?>


            </div>

            <div class="footer--news--content">
                <?if (!empty($arResult['fields']['USER']['ID'])):?>
                    <div class="user--box">
                        <i class="material-icons">
                            person
                        </i>
                        <p>
                            <?=$arResult['fields']['USER']['NAME']?> <?=$arResult['fields']['USER']['LAST_NAME']?>
                            <?if (!empty($arResult['fields']['USER']['PERSONAL_CITY'])):?>
                                , г. <?=$arResult['fields']['USER']['PERSONAL_CITY']?>
                            <?endif;?>
                        </p>
                    </div>
                <?else:?>
                    <div class="user--box"></div>
                <?endif;?>
                <div class="share">
                    <p><i class="material-icons">reply</i>Поделиться</p>
                    <div class="social-links">



                        <?$APPLICATION->IncludeComponent(
                            "culture:share",
                            "buttons",
                            Array(
                                'PAGE_URL' => $arResult['fields']['ELEMENT']['PAGE_URL']
                            )
                        );?>



                    </div>
                </div>
            </div>
        </div>
    </div>
</main>