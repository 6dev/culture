<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$i = 0;
$last = sizeof($arResult['fields']['ITEMS']);
if ($last > 0) $last--;
?>
<?foreach($arResult['fields']['ITEMS'] as $k=>$item):?>
    <?if (($i == 0) || ($i == $last)):?>
        <a href="/news/detail/<?=$item['FIELDS']["CODE"]//=$item['FIELDS']["DETAIL_PAGE_URL"]?>/" class="news__item">
            <div class="item__cover">
                <img class="item__image" src="<?=CFile::GetPath($item['FIELDS']["PREVIEW_PICTURE"])?>" />
                <?if ($item['FIELDS']["CATEGORY"]):?>
                    <p class="item__category" style="background: <?=$item['FIELDS']["CATEGORY_COLOR"]?>">
                        <?=$item['FIELDS']["CATEGORY"]?>
                    </p>
                <?endif;?>
            </div>
            <div class="item__description item__description--opened">
                <h3 class="item__title"><?=$item['FIELDS']["NAME"]?></h3>
                <p class="item__text">
                    <?=$item['FIELDS']["PREVIEW_TEXT"];?>
                </p>
                <p class="item__data"><?=FormatDate("d F Y", MakeTimeStamp($item['FIELDS']['ACTIVE_FROM']))?></p>
            </div>
        </a>
    <?else:?>
        <a href="/news/detail/<?=$item['FIELDS']["CODE"]//=$item['FIELDS']["DETAIL_PAGE_URL"]?>/" class="news__item">
            <div class="item__description">
                <h3 class="item__title"><?=$item['FIELDS']["NAME"]?></h3>
                <p class="item__data"><?=FormatDate("d F Y", MakeTimeStamp($item['FIELDS']['ACTIVE_FROM']))?></p>
            </div>
            <div class="item__cover">
                <img class="item__image" src="<?=CFile::GetPath($item['FIELDS']["PREVIEW_PICTURE"])?>" />
                <?if ($item['FIELDS']["CATEGORY"]):?>
                    <p class="item__category" style="background: <?=$item['FIELDS']["CATEGORY_COLOR"]?>"><?=$item['FIELDS']["CATEGORY"]?></p>
                <?endif;?>
            </div>
        </a>
    <?endif;?>
    <?
    $i++;
    ?>
<?endforeach;?>