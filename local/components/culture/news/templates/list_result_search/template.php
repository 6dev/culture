<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="search-result--news__container">

<?foreach($arResult['fields']['ITEMS'] as $k=>$item):?>


		
		
		<a href="<?=$item['FIELDS']["DETAIL_PAGE_URL"]?>/" class="news__item">
              <div class="item__description">
                <h3 class="item__title"><?=$item['FIELDS']["NAME"]?></h3>
                <p class="item__data"><?=FormatDate("d F Y", MakeTimeStamp($item['FIELDS']['ACTIVE_FROM']))?></p>
              </div>
              <div class="item__cover">
                <img class="item__image" src="<?=CFile::GetPath($item['FIELDS']["PREVIEW_PICTURE"])?>">
                <p class="item__category red">
                    <?=$item['FIELDS']["CATEGORY"]?>
                    
                    </p>
              </div>
            </a>

<?endforeach;?>

</div>