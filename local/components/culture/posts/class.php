<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;
use Custom\Culture\CultureHelpers;
global $USER;


Loader::includeModule('iblock');

class Posts extends CBitrixComponent
{

    private $params = array();

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "FILTER" => $arParams["FILTER"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
        );
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        if ($this->params['TYPE'] == 'LIST') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getList();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        if ($this->params['TYPE'] == 'DETAIL') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getDetail();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        if ($this->params['TYPE'] == 'COMMENTS') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getComments(array('POST_ID' => $this->params['ID']));
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        return null;
    }


    public function getList() {
        $nPageSize = (!empty($this->params['nPageSize']))? (int)$this->params['nPageSize'] : 20;
        $page_count = (!empty($this->params['PAGE_COUNT']))? (int)$this->params['PAGE_COUNT'] : 1;
        $nPageSize *=  $page_count;
        $arFilter = (!empty($this->params['FILTER']))? $this->params['FILTER'] : array();
        $page = (!empty($this->params['PAGE']))? (int)$this->params['PAGE'] : 1;
        $sort = (!empty($this->params['SORT']))? $this->params['SORT'] : array();
        $res = CIBlockElement::GetList($sort, $arFilter, false, Array("nPageSize"=>$nPageSize, "iNumPage" =>$page), Array("*", "PROPERTY_*"));
        $items = array();
        $fields = array();
        $fields['SHOW_BUTTON_MORE'] = true;
        $res->NavStart();
        $fields['ITEMS_COUNT'] =  $res->NavRecordCount;
        $fields['PAGE_COUNT'] =  ceil($fields['ITEMS_COUNT'] / $nPageSize);
        if ($fields['PAGE_COUNT'] < 1) $fields['PAGE_COUNT'] = 1;
        $fields['CURRENT_PAGE'] =  $page;
        if ($fields['CURRENT_PAGE'] == $fields['PAGE_COUNT']) $fields['SHOW_BUTTON_MORE'] = false;
        $fields['nPageSize'] = $nPageSize;
        while($ob = $res->GetNextElement()){
            $item = array();
            $item['FIELDS'] = $ob->GetFields();
            $item['PROPERTIES'] = $ob->GetProperties();
            $resThemes = CIBlockElement::GetList(array(), array('ID'=>$item['PROPERTIES']['THEMES']['VALUE'], 'IBLOCK_ID' => 24), false, Array(), Array("ID", "NAME"));
            $item['THEMES'] = array();
            while($ob2 = $resThemes->GetNextElement()){
                $item['THEMES'][] = $ob2->GetFields();
            }
            $items[] = $item;
        }
        $fields['ITEMS'] = $items;
        $fields['SEARCH_FILTERS'] = $this->getFilters($arFilter, $sort);
        $fields['BUTTON_MORE_PARAM'] = 'PAGE_COUNT='.($page_count+1).$fields['SEARCH_FILTERS']['BUTTON_MORE'];
        return $fields;
    }


    public function getDetail() {
        $code = $this->params['CODE'];
        $fields = array();
        $query = \Bitrix\Iblock\ElementTable::query();
        $query->registerRuntimeField('ELEMENT', [
                'data_type' => 'Bitrix\Iblock\ElementTable',
                'reference' => [
                    '=this.CODE' => 'ref.CODE',
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ID' => 'ref.IBLOCK_ID',
                ],
            ]
        );
        $query->registerRuntimeField('PARENT', array(
            "data_type"   => '\Bitrix\Iblock\Iblock',
            'reference'    => array('=this.IBLOCK_ID' => 'ref.ID'),
        ));
        $query->setFilter([
            'CODE' => $code,
            'PARENT.CODE'=> Custom\Culture\Posts::IBLOCK_CODE
        ]);
        $query->setSelect([
            'ID',
            'CODE',
            'IBLOCK_ID',
            'IBLOCK_CODE'  => 'PARENT.CODE',
            'IBLOCK_NAME'  => 'PARENT.NAME',
            'DETAIL_TEXT',
            'PREVIEW_TEXT',
            'NAME',
            'DETAIL_PICTURE',
            'PREVIEW_PICTURE',
            'ACTIVE_FROM',
        ]);
        $db_res = $query->exec();
        if ($dbItem = $db_res->fetch()) {
            global $APPLICATION;
            Custom\Culture\CultureHelpers::setPageTitle($dbItem['NAME']);
            $APPLICATION->SetPageProperty("og:type", "article");
            $APPLICATION->SetPageProperty("og:url", SITE_BASE_URL.'/posts/detail/'.$dbItem['CODE'].'/');
            $APPLICATION->SetPageProperty("og:title", $dbItem['NAME']);
            $APPLICATION->SetPageProperty("og:description", $dbItem['PREVIEW_TEXT']);

            $fields['ELEMENT'] = $dbItem;
            $fields['ELEMENT']['PUB_DATE'] = (!empty($dbItem['ACTIVE_FROM']))? $dbItem['ACTIVE_FROM']->toString(new \Bitrix\Main\Context\Culture(array())) : '';
            $fields['ELEMENT']['IMAGE_SRC'] = (!empty($dbItem['DETAIL_PICTURE']))? CFile::GetPath((int)$dbItem['DETAIL_PICTURE']) : '';
            if (($fields['ELEMENT']['IMAGE_SRC'] == '') && !empty($dbItem['PREVIEW_PICTURE'])) $fields['ELEMENT']['IMAGE_SRC'] = CFile::GetPath((int)$dbItem['PREVIEW_PICTURE']);

            $APPLICATION->SetPageProperty("og:image", SITE_BASE_URL.$fields['ELEMENT']['IMAGE_SRC']);

            $queryProps = \Bitrix\Iblock\ElementPropertyTable::query();
            $queryProps->registerRuntimeField('PROPERTIES', array(
                "data_type" => '\Bitrix\Iblock\ElementPropertyTable',
                'reference' => array(
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ELEMENT_ID' => 'ref.IBLOCK_ELEMENT_ID'
                )
            ));

            $queryProps->registerRuntimeField('PROPERTIES_CODE', array(
                "data_type" => '\Bitrix\Iblock\PropertyTable',
                'reference' => array('=this.PROPERTIES.IBLOCK_PROPERTY_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            ));
            $queryProps->setFilter([
                'IBLOCK_ELEMENT_ID' => $dbItem['ID']
            ]);
            $queryProps->setSelect([
                'VALUE' => 'PROPERTIES.VALUE',
                'VALUE_TYPE' => 'PROPERTIES.VALUE_TYPE',
                'VALUE_ENUM' => 'PROPERTIES.VALUE_ENUM',
                'VALUE_NUM' => 'PROPERTIES.VALUE_NUM',
                'CODE' => 'PROPERTIES_CODE.CODE'
            ]);
            $db_res = $queryProps->exec();
            $fields['PROPERTIES'] = array();
            $fields['USER'] = array();
            $fields['PAGE_URL'] = SITE_BASE_URL.'/posts/detail/'.$code.'/';
            while ($dbItem2 = $db_res->fetch()) {
                switch ($dbItem2['CODE']) {
                    case 'UID':
                        $fields['USER'] = $this->getUser($dbItem2['VALUE']);
                        break;
                    default:
                        $fields['PROPERTIES'][$dbItem2['CODE']] = $dbItem2;
                }
            }

            $views = (!empty($fields['PROPERTIES']['VIEWS']) && !empty($fields['PROPERTIES']['VIEWS']['VALUE']))? (int)$fields['PROPERTIES']['VIEWS']['VALUE'] : 0;
            $views++;
            CIBlockElement::SetPropertyValues($dbItem['ID'], $dbItem['IBLOCK_ID'], $views, 'VIEWS');
        }
        return $fields;
    }

    private function getUser($uid) {
        $query = new \Bitrix\Main\Entity\Query(Bitrix\Main\UserTable::getEntity());
        $query->registerRuntimeField('USER', [
                'data_type' => 'Bitrix\Main\UserTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );

        $query->setSelect(Array(
            'NAME',
            'ID',
            'EMAIL',
            'PERSONAL_PHOTO',
            'SECOND_NAME',
            'LAST_NAME',
            'PERSONAL_STATE',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
            'WORK_PROFILE',
            'UF_SCHOOL',
            'UF_INTERESTS',
        ));
        $query->setFilter(array("ID"=>(int)$uid));
        $db_res = $query->exec();
        return $db_res->fetch();
    }

    private function getFilters($arFilter=array(), $arSort=array()) {
        $filters = array();
        $btn_more = array();

        $filters['THEMES'] = array();
        $post_themes = CIBlockElement::GetList(array(), array("IBLOCK_ID"=>24, 'ACTIVE' => 'Y'), false, Array(), Array("ID", "NAME"));
        while($ob = $post_themes->GetNextElement()){
            $item = $ob->GetFields();
            $item['IS_SELECTED'] = false;
            if (!empty($arFilter['PROPERTY_THEMES']) && ($item['ID'] == $arFilter['PROPERTY_THEMES'])) {
                $item['IS_SELECTED'] = true;
                $btn_more[] = 'THEMES='.$item['ID'];
            }
            $filters['THEMES'][] = $item;
        }




        $filters['POST_TYPE'] = array();
        $filters['POST_TYPE'][Custom\Culture\Posts::IBLOCK_CODE] = array("NAME" => "Посты", "IS_SELECTED" => false);
        $filters['POST_TYPE'][Custom\Culture\Lectures::IBLOCK_CODE] = array("NAME" => "Лекции", "IS_SELECTED" => false);


        if (!empty($arFilter['IBLOCK_CODE']) && !empty($filters['POST_TYPE'][$arFilter['IBLOCK_CODE']])) {
            $filters['POST_TYPE'][$arFilter['IBLOCK_CODE']]['IS_SELECTED'] = true;
            $btn_more[] = 'POST_TYPE='.$arFilter['IBLOCK_CODE'];
        }


        $filters['SORT'] = array(
            "ACTIVE_FROM" => array("NAME" => "Дате", "IS_SELECTED" => false),
            "PROPERTY_VIEWS" => array("NAME" => "Просмотрам", "IS_SELECTED" => false),
            "PROPERTY_THEMES" => array("NAME" => "Теме", "IS_SELECTED" => false),
        );
        if (sizeof($arSort)) {
            $sortKey = '';
            foreach ($arSort as $k=>$item) {
                $sortKey = $k;
                break;
            }
            if (($sortKey != '') && !empty($filters['SORT'][$sortKey])) {
                $filters['SORT'][$sortKey]["IS_SELECTED"] = true;
                $btn_more[] = 'SORT='.$sortKey;
            }
        }

        $filters['BUTTON_MORE'] = (sizeof($btn_more) > 0)? '&'.join("&", $btn_more) : '';
        return $filters;
    }

    public function getComments($param=array()) {
        $fields = array();
        $cparam = array();
        if (!empty($this->params['COMMENTS_USER_BLOCK']) && ($this->params['COMMENTS_USER_BLOCK'] == 'Y')) {
            $cparam['USER'] = true;
        }
        if (!empty($this->params['COMMENTS_POST_BLOCK']) && ($this->params['COMMENTS_POST_BLOCK'] == 'Y')) {
            $cparam['POST'] = true;
        }
        $filter = array();
        if (!empty($param['POST_ID'])) $filter['UF_POST'] = (int)$param['POST_ID'];
        $filter['UF_ACTIVE'] = 1;
        $cparam['filter'] = $filter;
        $cparam['order'] = array('UF_CREATED' => 'DESC');
        $fields['ITEMS'] = Custom\Culture\Posts::getComments($cparam);
        return $fields;
    }

}?>