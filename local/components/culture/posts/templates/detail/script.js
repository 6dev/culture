$( document ).ready(function() {

    const limitComments = 4;

    function commentsRequest() {
        this.state = {send: 0, clearHtml: 0, items_count: 0};
        this.param = {
            data: {
                action: 'comments_list',
                post_id: $("#post_id").val(),
                offset: 0,
                limit: limitComments
            }
        };
        this.callback_success;
        this.callback_error;
        this.request = function (param, state) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: '/posts/ajax.php',
                    dataType: "json",
                    data: param.data,
                    success: (function (response) {
                        console.log(response);
                        if (response.hasOwnProperty('data') && response.data.hasOwnProperty('list_html')) {
                            if (response.data.hasOwnProperty('items_count')) {
                                state.items_count = parseInt(response.data.items_count, 10);
                            }
                            let h = $("#comments_list").html();
                            if (state.clearHtml == 1) h = response.data.list_html;
                            else h += response.data.list_html;
                            $("#comments_list").html(h);
                            $("#comment").val('');
                            param.data.offset += limitComments;
                           /* if ((param.data.offset >= state.items_count) || (state.items_count == 0))  {
                                $("#news__load_btn").hide();
                            }
                            else $("#news__load_btn").show();*/

                        }
                        state.clearHtml = 0;
                        state.send = 0;
                    }),
                    error: (function (error) {
                        state.send = 0;
                        state.clearHtml = 0;
                        reject(error);
                    })
                });
            });
        };
        this.getList = function (param) {
            if (this.state.send == 0) {
                this.state.send = 1;
                if (param.hasOwnProperty('offset')) this.param.data.offset = param.offset;
                if (param.hasOwnProperty('clearHtml')) this.state.clearHtml = param.clearHtml;
                this.request(this.param, this.state)
                    .then(this.callback_success,this.callback_error);
            }
            else console.log('send proccess');
        };
    };

    let comments = new commentsRequest();

    $("#send_comment").on("submit", function() {
        $("#ms_send_result").html("");
        $.ajax({
            type: 'POST',
            url: '/posts/ajax.php',
            dataType: "json",
            data: $(this).serialize(),
            success: function(result) {
                if (result.hasOwnProperty('status') && (result.status == 'success')) {
                    $("#ms_send_result").html("Отправлено на модерацию");
                    comments.getList({offset: 0, clearHtml: 1});
                }
            },
            error:  function(err){
                console.log(err);
                alert('Ошибка запроса');
            }
        });
        return false;
    });


    comments.getList({offset: 0, clearHtml: 1});

});