<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<main class="main news-page">
    <div class="container">
        <div class="news-page--content">
            <div class="header--news--content">
            <span class="bookmark--news">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px">
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path
                        fill="#BDBDBD"
                        d="M19 18l2 1V3c0-1.1-.9-2-2-2H8.99C7.89 1 7 1.9 7 3h10c1.1 0 2 .9 2 2v13zM15 5H5c-1.1 0-2 .9-2 2v16l7-3 7 3V7c0-1.1-.9-2-2-2z"
                />
              </svg>
            </span>
                <span class="data--news">
              <?=FormatDate("d F Y", MakeTimeStamp($arResult['fields']['ELEMENT']["ACTIVE_FROM"]))?>
            </span>
            </div>
            <h1><?=$arResult['fields']['ELEMENT']['NAME']?></h1>

            <div class="block-content">
                <p>
                    <?=$arResult['fields']['ELEMENT']["PREVIEW_TEXT"];?>
                </p>
                <div class="img-box">
                    <img src="<?=$arResult['fields']['ELEMENT']['IMAGE_SRC']?>" alt="event-image" />
                    <span>фото</span>
                </div>

                <?=$arResult['fields']['ELEMENT']['DETAIL_TEXT']?>


            </div>

            <div class="footer--news--content">
                <div class="user--box">
                    <i class="material-icons">
                        person
                    </i>
                    <p>
                        <?=$arResult['fields']['USER']['LAST_NAME']?> <?=$arResult['fields']['USER']['NAME']?>
                        <?if (!empty($arResult['fields']['USER']['PERSONAL_CITY'])):?>
                            , г. <?=$arResult['fields']['USER']['PERSONAL_CITY']?>
                        <?endif;?>
                    </p>
                </div>




                <div class="share">
                    <p><i class="material-icons">reply</i>Поделиться</p>
                    <div class="social-links">
                        <?$APPLICATION->IncludeComponent(
                            "culture:share",
                            "buttons",
                            Array(
                                'PAGE_URL' => $arResult['fields']['PAGE_URL']
                            )
                        );?>
                    </div>
                </div>
            </div>
        </div>





        <form id="send_comment">
            <textarea id="comment" name="comment" placeholder=""></textarea>
            <input type="hidden" value="add_comment" name="action">
            <input type="hidden" id="post_id" value="<?=$arResult['fields']['ELEMENT']['ID']?>" name="id">
            <button type="submit">отправить</button>
        </form>
        <div style="margin: 10px;" id="ms_send_result"></div>
        <div id="comments_list"></div>













    </div>
</main>