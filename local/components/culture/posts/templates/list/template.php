<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<main class="main cultural-education">
    <section class="header--cultural-education">
        <div class="container">



            <div class="category-filter">
                <div class="all-box">
                    <p>Категории:</p>

                    <input class="theme_check_all" type="checkbox" id="inputE0" />
                    <label for="inputE0">Все</label>
                </div>
                <form id="theme_form">
                <div class="container-input">
                    <?foreach ($arResult['fields']['SEARCH_FILTERS']['THEMES'] as $item):?>
                        <div class="input-el<?=($item['IS_SELECTED'])? ' active' : ''?>">
                            <input class="theme_check" name="THEMES_LIST[]" type="checkbox" value="<?=$item['ID']?>" id="inputE<?=$item['ID']?>" />
                            <label for="inputE<?=$item['ID']?>"><?=$item['NAME']?></label>
                        </div>
                    <?endforeach;?>
                </div>
                </form>
            </div>



            <div class="header--filter">
                <form id="search_filter">
                    <div class="filter-box">
                        <p>
                            Сортировка по
                        </p>
                        <select id="posts_sort" name="SORT">
                            <option value="">Сортировка по</option>
                            <?foreach ($arResult['fields']['SEARCH_FILTERS']['SORT'] as $k=>$item):?>
                                <option value="<?=$k?>"<?=($item['IS_SELECTED'])? ' selected' : ''?>><?=$item['NAME']?></option>
                            <?endforeach;?>
                        </select>
                    </div>
                </form>
                <input type="hidden" id="nPageSize" value="<?=$arResult['fields']['nPageSize']?>">
            </div>
        </div>
    </section>
    <div class="container">
        <div id="post_list" style="width: 100%;"></div>
        <p id="more_btn" class="news__load">
            Загрузить ещё
            <i class="material-icons arrow_downward__icon">arrow_downward</i>
        </p>
    </div>
</main>