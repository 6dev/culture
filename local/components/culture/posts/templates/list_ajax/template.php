<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult['fields']['ITEMS'] as $item):?>
    <?
    $imgId = (!empty($item['FIELDS']['PREVIEW_PICTURE']))? (int)$item['FIELDS']['PREVIEW_PICTURE'] : (int)$item['FIELDS']['DETAIL_PICTURE'];
    $imgSrc = ($imgId > 0)? CFile::GetPath($imgId) : '';
    ?>
    <?if ($item['FIELDS']['IBLOCK_CODE'] == Custom\Culture\Posts::IBLOCK_CODE):?>
        <section class="post-pages" style="margin-bottom: 60px;">
            <div class="post-pages--content">
                <div class="post-pages-left">
              <span class="label blue">
                  <?=$item['FIELDS']['IBLOCK_NAME']?>
              </span>
                    <h3 class="name-post">
                        <?=$item['FIELDS']['NAME']?>
                    </h3>
                    <div class="show-text">
                        <?=$item['FIELDS']['PREVIEW_TEXT']?>
                    </div>
                    <span><a style="text-decoration: none;" target="_blank" href="/posts/detail/<?=$item['FIELDS']['CODE']?>/">Читать далее</a> <i class="material-icons keyboard_arrow_right__icon">keyboard_arrow_right</i></span>
                    <div class="button-labels">
                        <?foreach ($item['THEMES'] as $theme):?>
                            <span class="blue"><?=$theme['NAME']?></span>
                        <?endforeach;?>
                    </div>
                </div>
                <div class="post-pages-right">
                    <?if ($imgSrc != ''):?>
                        <img src="<?=$imgSrc?>" alt="post-img" />
                    <?else:?>
                        <img src="/img/event-image.png" alt="post-img">
                    <?endif;?>
                </div>
            </div>
        </section>
    <?endif;?>
    <?if ($item['FIELDS']['IBLOCK_CODE'] == Custom\Culture\Lectures::IBLOCK_CODE):?>
        <section class="post-video">
            <div class="post-video--container">
                <div class="post-video--content">
              <span class="label yellow">
                <?=$item['FIELDS']['IBLOCK_NAME']?>
              </span>
                    <h3>
                        <?=$item['FIELDS']['NAME']?>
                    </h3>
                    <div class="video-box">
                        <iframe width="1280" height="720" src="https://www.youtube.com/embed/sn-S82mF_gw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    </div>
                    <div class="button-labels">
                        <?foreach ($item['THEMES'] as $theme):?>
                            <span class="blue"><?=$theme['NAME']?></span>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
        </section>
    <?endif;?>
<?endforeach;?>