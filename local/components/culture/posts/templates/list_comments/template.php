<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult['fields']['ITEMS'] as $item):?>
    <div style="margin-top: 20px;">
        <div>
            <?=(!empty($item['USER_ID']))? $item['USER_LOGIN'] : 'anonymous'?>:
        </div>
        <div style="margin-top: 5px;">
            <?=(!empty($item['UF_COMMENT']))? $item['UF_COMMENT'] : ''?>
        </div>
    </div>

<?endforeach;?>