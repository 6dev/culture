<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<section class="pagination-component">
    <div class="container">
        <p>
            Страницы
        </p>
        <ul>
            <?$i = 1;?>
            <?while ($i < ($arResult['fields']['PAGE_COUNT'] + 1)):?>
                <?if ($i == $arResult['fields']['CURRENT_PAGE']):?>
                    <li class="post_page_link is-active" value="<?=$i?>"><span><?=$i?></span></li>
                <?else:?>
                    <li class="post_page_link" value="<?=$i?>"><span><?=$i?></span></li>
                <?endif;?>
                <?$i++;?>
            <?endwhile;?>
        </ul>
    </div>
</section>
<div class="container">

<?foreach($arResult['fields']['ITEMS'] as $item):?>
    <?
    $imgId = (!empty($item['FIELDS']['PREVIEW_PICTURE']))? (int)$item['FIELDS']['PREVIEW_PICTURE'] : (int)$item['FIELDS']['DETAIL_PICTURE'];
    $imgSrc = ($imgId > 0)? CFile::GetPath($imgId) : '';
    ?>
    <?if ($item['FIELDS']['IBLOCK_CODE'] == Custom\Culture\Posts::IBLOCK_CODE):?>
    <section class="post-pages">
        <div class="post-pages--content">
            <div class="post-pages-left">
              <span class="label">
                  <?=$item['FIELDS']['IBLOCK_NAME']?>
              </span>
                <h3 class="name-post">
                    <?=$item['FIELDS']['NAME']?>
                </h3>
                <div class="show-text">
                    <?=$item['FIELDS']['PREVIEW_TEXT']?>
                </div>
    <span><a style="text-decoration: none;" href="/posts/detail/<?=$item['FIELDS']['CODE']?>/">Читать далее</a> <i class="material-icons keyboard_arrow_right__icon">keyboard_arrow_right</i></span>
            </div>
            <div class="post-pages-right">
                <?if ($imgSrc != ''):?>
                <img src="<?=$imgSrc?>" alt="post-img" />
                <?else:?>
                <img src="/img/event-image.png" alt="post-img">
                <?endif;?>
            </div>
        </div>
    </section>
    <?endif;?>
    <?if ($item['FIELDS']['IBLOCK_CODE'] == Custom\Culture\Lectures::IBLOCK_CODE):?>
        <section class="post-video">
            <div class="post-video--container">
                <div class="post-video--content">
              <span class="label">
                <?=$item['FIELDS']['IBLOCK_NAME']?>
              </span>
                    <h3>
                        <?=$item['FIELDS']['NAME']?>
                    </h3>
                    <div class="video-box">
                        <iframe
                                width="1280"
                                height="720"
                                src="https://www.youtube.com/embed/sn-S82mF_gw"
                                frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen
                        ></iframe>
                    </div>
                    <span class="post--video_user">
                <i class="material-icons">
                  person
                </i>
                Мария Иванова, г. Москва
              </span>
                    <div class="tag-box">
                        <a href="#">Греция</a>
                        <a href="#">История</a>
                        <a href="#">300 г. н. э.</a>
                        <a href="#">Рим</a>
                    </div>
                </div>
            </div>
        </section>
    <?endif;?>
<?endforeach;?>
</div>


<section class="pagination-component">
    <div class="container">
        <p>
            Страницы
        </p>
        <ul>
            <?$i = 1;?>
            <?while ($i < ($arResult['fields']['PAGE_COUNT'] + 1)):?>
                <?if ($i == $arResult['fields']['CURRENT_PAGE']):?>
                    <li class="post_page_link is-active" value="<?=$i?>"><span><?=$i?></span></li>
                <?else:?>
                    <li class="post_page_link" value="<?=$i?>"><span><?=$i?></span></li>
                <?endif;?>
                <?$i++;?>
            <?endwhile;?>
        </ul>
    </div>
</section>