<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


class Share extends CBitrixComponent
{

    private $params = array();

    public function onPrepareComponentParams($arParams)
    {
        $result = array();
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        $this->arResult["fields"] = $this->params;
        $this->includeComponentTemplate();
        return $this->arResult["fields"];
    }


}?>