<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;
use Custom\Culture\CultureHelpers;
global $USER;


Loader::includeModule('iblock');

class SliderMainPage extends CBitrixComponent
{

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
        );
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        $this->arResult["list"] = array();
        if($this->startResultCache())
        {
            $this->arResult["list"] = $this->getList();
            $this->includeComponentTemplate();
        }
        return $this->arResult["list"];
    }


    public function getList() {
        $nPageSize = (int)$this->params['LIMIT'];
        $arFilter = array("ACTIVE" => "Y", "IBLOCK_ID" => (int)$this->params["IBLOCK_ID"]);
        if (!empty($this->params["IBLOCK_SECTION_ID"])) $arFilter["IBLOCK_SECTION_ID"] = (int)$this->params["IBLOCK_SECTION_ID"];
        $sort = (!empty($this->params['SORT']))? $this->params['SORT'] : array();
        $res = CIBlockElement::GetList($sort, $arFilter, false, Array("nPageSize"=>$nPageSize), Array("*", "PROPERTY_*"));
        $list = array();
        while($ob = $res->GetNextElement()){
            $item = $ob->GetFields();
            $item['IMAGE_SRC'] = CFile::GetPath((int)$item['PREVIEW_PICTURE']);
            $props = $ob->GetProperties();
            if (!empty($props['LINK']['VALUE'])) {
                $item['LINK'] = $props['LINK']['VALUE'];
            }
            //else $item['LINK'] = $item['IMAGE_SRC'];

            if (!empty($props['VIDEO']['VALUE']['path'])) {
                $item['VIDEO'] = $props['VIDEO']['VALUE']['path'];
            }

            $list[] = $item;
        }
        return $list;
    }


}?>