<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult['list'] as $item):?>
    <div class="swiper-slide">
        <?if (!empty($item['VIDEO'])):?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:player",
                "",
                Array(
                    "ADDITIONAL_WMVVARS" => "",
                    "ADVANCED_MODE_SETTINGS" => "Y",
                    "AUTOSTART" => "Y",
                    "AUTOSTART_ON_SCROLL" => "Y",
                    "BUFFER_LENGTH" => "10",
                    "CONTROLS_BGCOLOR" => "FFFFFF",
                    "CONTROLS_COLOR" => "000000",
                    "CONTROLS_OVER_COLOR" => "000000",
                    "DOWNLOAD_LINK" => "",
                    "DOWNLOAD_LINK_TARGET" => "_self",
                    "HEIGHT" => "300",
                    "MUTE" => "N",
                    "PATH" => $item['VIDEO'],
                    "PLAYBACK_RATE" => "1",
                    "PLAYER_ID" => "",
                    "PLAYER_TYPE" => "auto",
                    "PRELOAD" => "Y",
                    "REPEAT" => "none",
                    "SCREEN_COLOR" => "000000",
                    "SHOW_CONTROLS" => "Y",
                    "SHOW_DIGITS" => "Y",
                    "SIZE_TYPE" => "absolute",
                    "SKIN" => "",
                    "SKIN_PATH" => "/bitrix/js/fileman/player/videojs/skins",
                    "START_TIME" => "0",
                    "VOLUME" => "90",
                    "WIDTH" => "400",
                    "WMODE_WMV" => "window"
                )
            );?>
        <?else:?>
        <a <?if (!empty($item['LINK'])):?>target="_blank" href="<?=$item['LINK']?>"<?else:?>href="javascript:void(0);"<?endif;?>>
        <img src="<?=$item['IMAGE_SRC']?>" alt="img" />
        </a>
        <?endif;?>
    </div>
<?endforeach;?>