<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult['list'] as $item):?>
    <div class="swiper-slide">
        <a <?if (!empty($item['LINK'])):?>target="_blank" href="<?=$item['LINK']?>"<?else:?>href="javascript:void(0);"<?endif;?>>
            <img src="<?=$item['IMAGE_SRC']?>" alt="img"/>
        </a>
    </div>
<?endforeach;?>