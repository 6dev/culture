<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;
use Custom\Culture\CultureHelpers;

Loader::includeModule('iblock');

class Users extends CBitrixComponent
{

    private $params = array();

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "FILTER" => $arParams["FILTER"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ?$arParams["CACHE_TIME"]: 36000000,
        );
        $this->params = $arParams;
        return $result;
    }

    public function executeComponent()
    {
        if ($this->params['TYPE'] == 'LIST') {
            $this->arResult["fields"] = array();
            if($this->startResultCache())
            {
                $this->arResult["fields"] = $this->getList();
                $this->includeComponentTemplate();
            }
            return $this->arResult["fields"];
        }
        return null;
    }


    public function getList() {
        global $users_count_all;
        $users_count_all = $this->getCount();
        $fields = array();
        $query = $this->getQuery();
        $query->setSelect(Array(
            'NAME',
            'ID',
            'EMAIL',
            'LOGIN',
            'PERSONAL_PHOTO',
            'SECOND_NAME',
            'LAST_NAME',
            'PERSONAL_STATE',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
            'UF_SCHOOL',
            'UF_INTERESTS',
            'QUESTION_COUNT',
            'POST_COMMENTS_COUNT',
            'MAP_POINTS_COUNT'
        ));

        $query->setFilter($this->getFilter());

        if (!empty($this->params['ORDER'])) $query->setOrder($this->params['ORDER']);
        if (!empty($this->params['LIMIT'])) $query->setLimit((int)$this->params['LIMIT']);
        if (!empty($this->params['OFFSET'])) $query->setOffset((int)$this->params['OFFSET']);

        $db_res = $query->exec();

        $list = array();

        $item_num = (!empty($this->params['OFFSET']))? (int)$this->params['OFFSET'] : 0;
        $item_num++;
        while ($dbItem = $db_res->fetch()) {
            $item = $dbItem;
            $item['ITEM_NUM'] = $item_num++;
            $item['PHOTO_SRC'] = (!empty($item['PERSONAL_PHOTO']))? CFile::GetPath((int)$item['PERSONAL_PHOTO']) : '/img/svg/account-default.svg';
            $list[] = $item;
        }
        $fields['ITEMS'] = $list;
        return $fields;
    }

    public function getCount() {
        $query = $this->getQuery();
        $query->registerRuntimeField('ELEMENTS_COUNT', [
            'data_type'=>'integer',
            'expression' => ['COUNT(%s)', 'ID']
        ]);
        $query->setSelect([
            'ELEMENTS_COUNT'
        ]);
        $query->setFilter($this->getFilter());
        $db_res = $query->exec();
        $result = 0;
        if ($res = $db_res->fetch()) $result = (!empty($res['ELEMENTS_COUNT']))? (int)$res['ELEMENTS_COUNT'] : 0;
        return $result;
    }

    private function getFilter() {
        $filter = array(
            array(
                'LOGIC' => 'AND',
                array("!@ID"=>new \Bitrix\Main\DB\SqlExpression($this->subQueryUserGroup(1))),
                array("!@ID"=>new \Bitrix\Main\DB\SqlExpression($this->subQueryUserGroup(7)))
            )
        );

        if (!empty($this->params['FILTER'])) {
            foreach($this->params['FILTER'] as $k=>$v) $filter[$k] = $v;
        }
        return $filter;
    }

    private function getQuery() {
        $query = new \Bitrix\Main\Entity\Query(\Bitrix\Main\UserTable::getEntity());
        $query->registerRuntimeField('USER', [
                'data_type' => '\Bitrix\Main\UserTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );
        $query->registerRuntimeField('QUESTION_COUNT', [
            'expression' => ['(' . $this->getQuestionSubQuery() . ')', 'ID']
        ]);
        $query->registerRuntimeField('POST_COMMENTS_COUNT', [
            'expression' => ['(' . $this->getPostCommentsSubQuery() . ')', 'ID']
        ]);
        $query->registerRuntimeField('MAP_POINTS_COUNT', [
            'expression' => ['(' . $this->getMapPointsSubQuery() . ')', 'ID']
        ]);
        return $query;
    }

    private function getQuestionSubQuery() {
        $query = new \Bitrix\Main\Entity\Query(\Custom\Chat\ChatTable::getEntity());
        $query->registerRuntimeField('CHAT', [
                'data_type' => '\Custom\Chat\ChatTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );
        $query->registerRuntimeField('ELEMENTS_COUNT', [
            'data_type'=>'integer',
            'expression' => ['COUNT(%s)', 'ID']
        ]);
        $query->setFilter([
            'MODULE' => 'mentors',
            'MODERATED' => 1,
            'SRC_USER_ID'=> new \Bitrix\Main\DB\SqlExpression('%s')
        ]);
        $query->setSelect([
            'ELEMENTS_COUNT'
        ]);
        return $query->getQuery();
    }

    private function getPostCommentsSubQuery() {
        $query = new \Bitrix\Main\Entity\Query(\Custom\Culture\PostCommentsTable::getEntity());
        $query->registerRuntimeField('CHAT', [
                'data_type' => '\Custom\Culture\PostCommentsTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );
        $query->registerRuntimeField('ELEMENTS_COUNT', [
            'data_type'=>'integer',
            'expression' => ['COUNT(%s)', 'ID']
        ]);
        $query->setFilter([
            'UF_ACTIVE' => 1,
            'UF_UID'=> new \Bitrix\Main\DB\SqlExpression('%s')
        ]);
        $query->setSelect([
            'ELEMENTS_COUNT'
        ]);
        return $query->getQuery();
    }

    private function getMapPointsSubQuery() {
        $query = \Bitrix\Iblock\ElementTable::query();
        $query->registerRuntimeField('ELEMENT', [
                'data_type' => '\Bitrix\Iblock\ElementTable',
                'reference' => [
                    '=this.CODE' => 'ref.CODE',
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ID' => 'ref.IBLOCK_ID',
                ],
            ]
        );
        $query->registerRuntimeField('PROPERTIES', array(
            "data_type"   => '\Bitrix\Iblock\ElementPropertyTable',
            'reference'    => array('=this.ID' => 'ref.IBLOCK_ELEMENT_ID'),
        ));

        $query->registerRuntimeField('PROPERTIES_CODE', array(
            "data_type"   => '\Bitrix\Iblock\PropertyTable',
            'reference'    => array('=this.PROPERTIES.IBLOCK_PROPERTY_ID' => 'ref.ID'),
        ));
        $query->registerRuntimeField('ELEMENTS_COUNT', [
            'data_type'=>'integer',
            'expression' => ['COUNT(%s)', 'ID']
        ]);
        $query->setFilter([
            'IBLOCK_ID' => 20,
            'PROPERTIES_CODE.CODE'=>'USER',
            'PROPERTIES.VALUE'=> new \Bitrix\Main\DB\SqlExpression('%s')
        ]);
        $query->setSelect([
            'ELEMENTS_COUNT'
        ]);
        return $query->getQuery();
    }

    private function subQueryUserGroup($group) {
        $query = new \Bitrix\Main\Entity\Query(\Bitrix\Main\UserGroupTable::getEntity());
        $query->setSelect(array("D_USER_ID"));
        $query->setFilter(array("GROUP_ID"=>$group));
        $query->registerRuntimeField(0, new \Bitrix\Main\Entity\ExpressionField('D_USER_ID', 'DISTINCT(USER_ID)'));
        return $query->getQuery();
    }

}?>