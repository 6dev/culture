$( document ).ready(function() {
    let users = null;

    function getLimit() {
        return 10;
    }

    function getRequestFields() {
        let formData = new FormData(document.getElementById("search_filter"));
        let data = {action: 'list', offset: 0, limit: getLimit()};
        for (let entry of formData.entries()) data[entry[0]] = entry[1];
        return data;
    }

    function usersRequest(initData) {
        this.state = {send: 0, clearHtml: 0, items_count: 1};
        this.param = {
            data: initData
        };
        this.callback_success;
        this.callback_error;
        this.request = function (param, state) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: '/users/ajax.php',
                    dataType: "json",
                    data: param.data,
                    success: (function (response) {
                        if (response.hasOwnProperty('data') && response.data.hasOwnProperty('list_html')) {
                            if (response.data.hasOwnProperty('items_count')) {
                                state.items_count = parseInt(response.data.items_count, 10);
                            }
                            let h = $("#user_list").html();
                            if (state.clearHtml == 1) h = response.data.list_html;
                            else h += response.data.list_html;
                            $("#user_list").html(h);
                            if (((param.data.offset + getLimit()) >= state.items_count) || (state.items_count == 0))  {
                                $("#more_btn").hide();
                            }
                            else $("#more_btn").show();
                            param.data.offset += getLimit();
                        }
                        state.clearHtml = 0;
                        state.send = 0;
                    }),
                    error: (function (error) {
                        state.send = 0;
                        state.clearHtml = 0;
                        reject(error);
                    })
                });
            });
        };
        this.getList = function (param) {
            if (this.state.send == 0) {
                this.state.send = 1;
                if (param.hasOwnProperty('offset')) this.param.data.offset = param.offset;
                if (param.hasOwnProperty('clearHtml')) this.state.clearHtml = param.clearHtml;
                this.request(this.param, this.state)
                    .then(this.callback_success,this.callback_error);
            }
            else console.log('send proccess');
        };
    };

    function updateList() {
        $("#more_btn").hide();
        users = new usersRequest(getRequestFields());
        users.getList({offset: 0, clearHtml: 1});
    }

    $(".period_select").on("change", function() {
        updateList();
    });

    $("body").on("click", "#more_btn", function() {
        if (users) users.getList({});
    });

    updateList();
});