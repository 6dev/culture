<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<main class="main rating-pages">
    <section class="header--rating--ages">
        <form id="search_filter">
        <div class="container">
            <div class="header--rating--ages__content">
                <div class="location-box">
              <span class="title">
                Местоположение
              </span>
                    <div class="input-row">
                        <div class="input-row--el">
                            <input type="radio" id="location1" checked name="location" />
                            <label for="location1"> Страна </label>
                        </div>
                        <div class="input-row--el">
                            <input type="radio" id="location2" name="location" />
                            <label for="location2"> Мой регион </label>
                        </div>
                    </div>
                </div>
                <div class="period">
              <span class="title">
                Период
              </span>
                    <div class="input-row">
                        <div class="input-row--el">
                            <input type="radio" id="period1" class="period_select" value="weak" name="period" />
                            <label for="period1"> Неделя </label>
                        </div>
                        <div class="input-row--el">
                            <input type="radio" id="period2" class="period_select" value="month" name="period" />
                            <label for="period2"> Месяц </label>
                        </div>
                        <div class="input-row--el">
                            <input type="radio" id="period3" class="period_select" value="year" name="period" />
                            <label for="period3"> Год </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </section>
    <section class="rating--result-filter">
        <div class="container">
            <div class="result-filter--content">
                <ul id="user_list">

                </ul>
                <p class="news__load" id="more_btn">
                    Загрузить ещё
                    <i class="material-icons arrow_downward__icon">arrow_downward</i>
                </p>
            </div>
        </div>
    </section>
</main>