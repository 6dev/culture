<?
IncludeModuleLangFile(__FILE__); // в menu.php точно так же можно использовать языковые файлы

if($APPLICATION->GetGroupRight("form")>"D") // проверка уровня доступа к модулю веб-форм
{
    // сформируем верхний пункт меню
    $aMenu = array(
        "parent_menu" => "global_menu_services", // поместим в раздел "Сервис"
        "sort"        => 100,                    // вес пункта меню
        "url"         => "affiche_orders.php?",  // ссылка на пункте меню
        "text"        => "Записи на мероприятия",       // текст пункта меню
        "title"       => "Записи на мероприятия", // текст всплывающей подсказки
        "icon"        => "form_menu_icon", // малая иконка
        "page_icon"   => "form_page_icon", // большая иконка
        "items_id"    => "menu_affiche_orders",  // идентификатор ветви
        "items"       => array(),          // остальные уровни меню сформируем ниже.
    );

    return $aMenu;
}
// если нет доступа, вернем false
return false;
?>