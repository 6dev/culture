<?php
namespace Custom\Afficheorders;

use Bitrix\Main\Loader;
use Bitrix\Catalog\PriceTable;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\ElementPropertyTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;

Loader::includeModule('iblock');
Loader::includeModule('catalog');

class Orders
{

    public static function getList($param=array()) {
        $filter = Array();
        if (!empty($param['filter'])) $filter = $param['filter'];

        $queryCnt = self::getQuery();
        $queryCnt->registerRuntimeField('ELEMENTS_COUNT', [
            'data_type'=>'integer',
            'expression' => ['COUNT(%s)', 'ID']
        ]);
        $queryCnt->setFilter($filter);
        $queryCnt->setSelect([
            'ELEMENTS_COUNT'
        ]);
        $allCntRes = $queryCnt->exec();
        $allCnt = 0;
        if ($item = $allCntRes->fetch()) $allCnt = (int)$item['ELEMENTS_COUNT'];

        $query = self::getQuery();
		$query->setSelect(Array(  
            'ID',
			'EVENT_ID',
			'USER_ID',
            'EVENT_NAME' => 'EVENT.NAME',
            'USER_NAME' => 'USER.NAME',
            'USER_LAST_NAME' => 'USER.LAST_NAME',
            'USER_PERSONAL_CITY' => 'USER.PERSONAL_CITY',
			'USER_LOGIN' => 'USER.LOGIN'
        ));

        $query->setFilter($filter);		
		if (!empty($param['order'])) $query->setOrder($param['order']);
        $offset = (!empty($param['offset']))? (int)$param['offset'] : 0;
        if (!empty($param['limit'])) {
            $query->setOffset($offset);
            $query->setLimit((int)$param['limit']);
        }
        $db_res = $query->exec();	
        $list = [];	
        $cnt = 0;		
        while ($item = $db_res->fetch()) {
			$list[] = $item;
			$cnt++;
		}
        return array('LIST' => $list, 'COUNT' => $cnt, 'ALL_COUNT' => $allCnt, 'OFFSET' => $offset);
    }

    public static function getItem($filter=array()) {
        $query = self::getQuery();
        $query->setSelect(Array(
            'ID',
            'EVENT_ID',
            'USER_ID',
            'EVENT_NAME' => 'EVENT.NAME',
            'USER_NAME' => 'USER.NAME',
            'USER_LAST_NAME' => 'USER.LAST_NAME',
            'USER_PERSONAL_CITY' => 'USER.PERSONAL_CITY',
            'USER_LOGIN' => 'USER.LOGIN'
        ));
        $query->setFilter($filter);
        $db_res = $query->exec();
        return $db_res->fetch();
    }

    public static function addOrder($param=array()) {
		$fields = $param;
		$fields['SITE_ID'] = SITE_ID;
		return OrdersTable::add($fields);
	}

    public static function deleteOrder($id) {
        return OrdersTable::delete($id);
    }

    private static function getQuery() {
        $query = new \Bitrix\Main\Entity\Query(OrdersTable::getEntity());
        $query->registerRuntimeField('ORDER', [
                'data_type' => 'Custom\Afficheorders\OrdersTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );

        $query->registerRuntimeField('EVENT', [
                'data_type' => 'Bitrix\Iblock\ElementTable',
                'reference' => array('=this.EVENT_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            ]
        );

        $query->registerRuntimeField("USER", array(
                "data_type" => "Bitrix\Main\UserTable",
                'reference' => array('=this.USER_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            )
        );
        return $query;
    }

    public static function getAfficheDetails($id) {
        $fields = array();
        if (!$id) return $fields;
        $query = \Bitrix\Iblock\ElementTable::query();
        $query->registerRuntimeField('ELEMENT', [
                'data_type' => '\Bitrix\Iblock\ElementTable',
                'reference' => [
                    '=this.CODE' => 'ref.CODE',
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ID' => 'ref.IBLOCK_ID',
                ],
            ]
        );
        $query->setFilter(array('IBLOCK_ID' => 21, 'ID' => (int)$id));
        $query->setSelect([
            'ID',
            'DETAIL_TEXT',
            'PREVIEW_TEXT',
            'NAME',
            'CODE',
            'DETAIL_PICTURE',
            'PREVIEW_PICTURE',
            'ACTIVE_FROM'
        ]);
        $db_res = $query->exec();
        if ($dbItem = $db_res->fetch()) {
            $fields['ELEMENT'] = $dbItem;
            $fields['ELEMENT']['IMAGE_SRC'] = (!empty($dbItem['DETAIL_PICTURE']))? \CFile::GetPath((int)$dbItem['DETAIL_PICTURE']) : '';
            if (($fields['ELEMENT']['IMAGE_SRC'] == '') && !empty($dbItem['PREVIEW_PICTURE'])) $fields['ELEMENT']['IMAGE_SRC'] = \CFile::GetPath((int)$dbItem['PREVIEW_PICTURE']);
            $queryProps = \Bitrix\Iblock\ElementPropertyTable::query();
            $queryProps->registerRuntimeField('PROPERTIES', array(
                "data_type" => '\Bitrix\Iblock\ElementPropertyTable',
                'reference' => array(
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ELEMENT_ID' => 'ref.IBLOCK_ELEMENT_ID'
                )
            ));

            $queryProps->registerRuntimeField('PROPERTIES_CODE', array(
                "data_type" => '\Bitrix\Iblock\PropertyTable',
                'reference' => array('=this.PROPERTIES.IBLOCK_PROPERTY_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            ));

            $queryProps->registerRuntimeField('PROPERTIES_BIND_ELEMS', [
                    'data_type' => '\Bitrix\Iblock\ElementTable',
                    'reference' => [
                        '=this.PROPERTIES.VALUE' => 'ref.ID',
                    ],
                    'join_type' => "LEFT"
                ]
            );

            $queryProps->registerRuntimeField('PROPERTIES_ENUM', [
                    'data_type' => '\Bitrix\Iblock\PropertyEnumerationTable',
                    'reference' => [
                        '=this.PROPERTIES.VALUE_ENUM' => 'ref.ID',
                    ],
                    'join_type' => "LEFT"
                ]
            );

            $queryProps->setFilter([
                'IBLOCK_ELEMENT_ID' => $dbItem['ID']
            ]);
            $queryProps->setSelect([
                'VALUE' => 'PROPERTIES.VALUE',
                'VALUE_TYPE' => 'PROPERTIES.VALUE_TYPE',
                'ENUM_' => 'PROPERTIES_ENUM',
                'VALUE_NUM' => 'PROPERTIES.VALUE_NUM',
                'VALUE_BIND_ELEMS' => 'PROPERTIES_BIND_ELEMS.NAME',
                'CODE' => 'PROPERTIES_CODE.CODE'
            ]);
            $db_res = $queryProps->exec();
            $fields['PROPERTIES'] = array();
            $ctypes = array();
            while ($dbItem2 = $db_res->fetch()) {
                if ($dbItem2['CODE'] == 'CULTURE_TYPES') {
                    $citem = array();
                    $citem['VALUE'] = $dbItem2['VALUE_BIND_ELEMS'];
                    $ctypes[] = $citem;
                }
                else $fields['PROPERTIES'][$dbItem2['CODE']] = $dbItem2;
            }
            $fields['PROPERTIES']['CULTURE_TYPES'] = $ctypes;
        }
        return $fields;
    }

}