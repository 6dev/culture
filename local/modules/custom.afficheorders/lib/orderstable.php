<?php
namespace Custom\Afficheorders;
 
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
  
class OrdersTable extends Entity\DataManager
{

   public static function getTableName()
   {
      return 'affiche_orders';
   }

   public static function getMap()
   {
      return array(
          'ID' => array(
              'data_type' => 'integer',
              'primary' => true,
              'autocomplete' => true,
              'title' => 'ID',
          ),
          'SITE_ID' => array(
              'data_type' => 'string',
              'required' => true,
              'title' => 'SITE_ID',
          ),
          'EVENT_ID' => array(
              'data_type' => 'integer',
              'title' => 'EVENT_ID',
          ),
          'USER_ID' => array(
              'data_type' => 'integer',
              'title' => 'USER_ID',
          ),
          'PAID' => array(
              'data_type' => 'integer',
              'title' => 'PAID',
          ),
          'STATUS' => array(
              'data_type' => 'integer',
              'title' => 'STATUS',
          ),
          'CREATED' => array(
              'data_type' => 'datetime',
              'title' => 'CREATED',
          ),
      );
   }
}