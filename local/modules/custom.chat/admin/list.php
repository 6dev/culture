<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/include.php"); // инициализация модуля
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/prolog.php"); // пролог модуля

// подключим языковой файл
IncludeModuleLangFile(__FILE__);

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("subscribe");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
?>
<?
// здесь будет вся серверная обработка и подготовка данных
$sTableID = "custom_chat"; // ID таблицы
$oSort = new CAdminSorting($sTableID, "ID", "desc"); // объект сортировки
$lAdmin = new CAdminList($sTableID, $oSort); // основной объект списка


$module_name = (!empty($_REQUEST['MODULE']))? $_REQUEST['MODULE'] : '';
$elementId = (!empty($_REQUEST['ID']))? (int)$_REQUEST['ID'] : 0;

if ($elementId) {
    if (!empty($_REQUEST['action_button'])) {
        if ($_REQUEST['action_button'] == 'delete') {
            Custom\Chat\MainChat::deleteMessage($elementId);
            LocalRedirect("chats.php?MODULE=".$module_name);
        }
    }
}

$rsData = array();
if ($module_name == 'mentors') $rsData = Custom\Chat\MainChat::getMessages()['LIST'];

$rsData = new CAdminResult($rsData, $sTableID);

// аналогично CDBResult инициализируем постраничную навигацию.
$rsData->NavStart();

// отправим вывод переключателя страниц в основной объект $lAdmin
$lAdmin->NavText($rsData->GetNavPrint("nav4"));


$lAdmin->AddHeaders(array(
    array(  "id"    =>"ID",
        "content"  =>"ID",
        "sort"     =>"id",
        "default"  =>true,
    ),
    array(  "id"    =>"MESSAGE",
        "content"  => "сообщение",
        "sort"     =>"MESSAGE",
        "default"  =>true,
    ),
    array(  "id"    =>"REF_ID",
        "content"  => "ответ на сообщение №",
        "sort"     =>"REF_ID",
        "default"  =>true,
    ),
    array(  "id"    =>"SRC_USER_LOGIN",
        "content"  => "отправитель",
        "sort"     =>"SRC_USER_LOGIN",
        "default"  =>true,
    ),
    array(
        "id"    =>"DST_USER_LOGIN",
        "content"  => "получатель",
        "sort"     =>"DST_USER_LOGIN",
        "default"  =>true,
    ),
    array(
        "id"    =>"MODERATED",
        "content"  => "Модерация",
        "sort"     =>"MODERATED",
        "default"  =>true,
    ),
    array(
        "id"    =>"SHOW_MAIN_PAGE",
        "content"  => "Отображать на главной",
        "sort"     =>"SHOW_MAIN_PAGE",
        "default"  =>true,
    )
));


while($arRes = $rsData->NavNext(true, "f_")):

    // создаем строку. результат - экземпляр класса CAdminListRow
    $row =& $lAdmin->AddRow($f_ID, $arRes);

    // далее настроим отображение значений при просмотре и редактировании списка

    // параметр NAME будет редактироваться как текст, а отображаться ссылкой
    //$row->AddInputField("MESSAGE", array());
    $row->AddViewField("MESSAGE", $f_MESSAGE);
    $row->AddViewField("REF_ID", $f_REF_ID);

    // $row->AddInputField("SRC_USER_LOGIN", array());
    $row->AddViewField("SRC_USER_LOGIN", $f_SRC_USER_LOGIN);
    $row->AddViewField("DST_USER_LOGIN", $f_DST_USER_LOGIN);

    if ($f_MODERATED) $row->AddViewField("MODERATED", "Да");
    else $row->AddViewField("MODERATED", "Нет");

    if ($f_SHOW_MAIN_PAGE) $row->AddViewField("SHOW_MAIN_PAGE", "Да");
    else $row->AddViewField("SHOW_MAIN_PAGE", "Нет");


    // флаги ACTIVE и VISIBLE будут редактироваться чекбоксами
    //$row->AddCheckField("ACTIVE");
    // $row->AddCheckField("VISIBLE");


    // сформируем контекстное меню
    $arActions = Array();

    // редактирование элемента
    $arActions[] = array(
        "ICON"=>"edit",
        "DEFAULT"=>true,
        "TEXT"=>"редактировать",
        "ACTION"=>$lAdmin->ActionRedirect("chat_edit.php?MODULE=".$module_name."&ID=".$f_ID)
    );
    $arActions[] = array(
        "ICON"=>"edit",
        "DEFAULT"=>true,
        "TEXT"=>"ответить на сообщение",
        "ACTION"=>$lAdmin->ActionRedirect("chat_edit.php?MODULE=".$module_name."&ACT=answer&REF_ID=".$f_ID)
    );
    // удаление элемента
    if ($POST_RIGHT>="W")
        $arActions[] = array(
            "ICON"=>"delete",
            "TEXT"=>"удалить",
            "ACTION"=>$lAdmin->ActionRedirect("chats.php?action_button=delete&MODULE=".$module_name."&ID=".$f_ID)
        );

    // вставим разделитель
    $arActions[] = array("SEPARATOR"=>true);

    // проверка шаблона для автогенерируемых рассылок
    if (strlen($f_TEMPLATE)>0 && $f_AUTO=="Y")
        $arActions[] = array(
            "ICON"=>"",
            "TEXT"=>"chat_check",
            "ACTION"=>$lAdmin->ActionRedirect("template_test.php?ID=".$f_ID)
        );

    // если последний элемент - разделитель, почистим мусор.
    if(is_set($arActions[count($arActions)-1], "SEPARATOR"))
        unset($arActions[count($arActions)-1]);

    // применим контекстное меню к строке
    $row->AddActions($arActions);

endwhile;

// резюме таблицы
$lAdmin->AddFooter(
    array(
        array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()), // кол-во элементов
        array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"), // счетчик выбранных элементов
    )
);

// групповые действия
$lAdmin->AddGroupActionTable(Array(
    "delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"), // удалить выбранные элементы
    "activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // активировать выбранные элементы
    "deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // деактивировать выбранные элементы
));


if ($module_name != '') {
    $aContext = array(
        array(
            "TEXT"=>"Создать сообщение",
            "LINK"=>"chat_edit.php?MODULE=".$module_name."&lang=".LANG,
            "TITLE"=>"Создать сообщение",
            "ICON"=>"btn_new",
        ),
    );
    $lAdmin->AddAdminContextMenu($aContext);
}


// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$title = 'Чаты';
if ($module_name == 'mentors') $title = 'Вопрос - ответ с наставником';
$APPLICATION->SetTitle($title);

?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); // второй общий пролог
?>
<?
// здесь будет вывод страницы
// создадим объект фильтра
$oFilter = new CAdminFilter(
    $sTableID."_filter",
    array(
        "ID",
    )
);


$lAdmin->DisplayList();
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>