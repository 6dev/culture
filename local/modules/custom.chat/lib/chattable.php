<?php
namespace Custom\Chat;
 
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
  
class ChatTable extends Entity\DataManager
{

   public static function getTableName()
   {
      return 'custom_chat';
   }

   public static function getMap()
   {
      return array(
        'ID' => array(
            'data_type' => 'integer',
            'primary' => true,
            'autocomplete' => true,
            'title' => 'ID',
        ),
        'SITE_ID' => array(
            'data_type' => 'string',
            'required' => true,
            'title' => 'SITE_ID',
        ),
		'MODULE' => array(
            'data_type' => 'string',
            'required' => true,
            'title' => 'MODULE',
        ),
		'SRC_USER_ID' => array(
            'data_type' => 'integer',
            'title' => 'SRC_USER_ID',
        ),
		'DST_USER_ID' => array(
            'data_type' => 'integer',
			'required' => true,
            'title' => 'DST_USER_ID',
        ),
        'REF_ID' => array(
              'data_type' => 'integer',
              'title' => 'REF_ID',
        ),
        'MESSAGE' => array(
            'data_type' => 'text',
            'required' => true,
            'title' => 'MESSAGE',
        ),
		'STATUS' => array(
            'data_type' => 'integer',
            'title' => 'STATUS',
        ),
		'MODERATED' => array(
            'data_type' => 'integer',
            'title' => 'MODERATED',
        ),
        'SHOW_MAIN_PAGE' => array(
            'data_type' => 'boolean',
            'title' => 'SHOW_MAIN_PAGE',
        ),
        'CREATED' => array(
            'data_type' => 'datetime',
            'title' => 'CREATED',
        ),
      );
   }
}