<?php
namespace Custom\Culture;

class CityBind
{
    public function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE"  => "S",
            "USER_TYPE"      => "CITY",
            "DESCRIPTION"    => "Привязка к городам",
			"GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
        );
    }
	
	
    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $value["DESCRIPTION"] = unserialize($value["DESCRIPTION"]);

        $cityList = CultureHelpers::getCities();

		$html = '';
        $html .= '<select name="'.$strHTMLControlName["VALUE"].'">';
        $html .= '<option value="" disabled selected>Город</option>';
        foreach($cityList as $item) {
            if (!empty($value["VALUE"]) && ($value["VALUE"] == $item['NAME'])) {
                $html .= '<option value="'.$item['NAME'].'" selected>'.$item['NAME'].'</option>';
            }
            else $html .= '<option value="'.$item['NAME'].'">'.$item['NAME'].'</option>';
        }
        $html .= '</select>';

		return  $html;
    }
	
}
