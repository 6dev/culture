<?php
namespace Custom\Culture;

use Bitrix\Main\Loader;
use Bitrix\Main\Config;
use Bitrix\Catalog\PriceTable;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\ElementPropertyTable;
use Bitrix\Iblock\IblockElementProperty;

use \Bitrix\Main\Dictionary;

use \Bitrix\Iblock\PropertyEnumerationTable;


use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\NotImplementedException;
use Bitrix\Main\ORM\EntityError;
use Bitrix\Main\ORM\Event;
use Bitrix\Main\ORM\Fields\BooleanField;
use Bitrix\Main\ORM\Fields\DatetimeField;
use Bitrix\Main\ORM\Fields\EnumField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Fields\TextField;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\Type\DateTime;



Loader::includeModule('iblock');
Loader::includeModule('catalog');
Loader::includeModule('forum');
Loader::IncludeModule('highloadblock');
Loader::IncludeModule("main");

class CultureHelpers
{
    const CULTURE_CATEGORY_IBLOCK_ID = 18;

    public static function setPageTitle($name='') {
        global $APPLICATION;
        if ($name != '') $APPLICATION->SetPageProperty("page_title", $name.' — ');
        return true;
    }


    public static function CheckSite($message) {

        $reg="(\.\s?(ua|net|рф|com|tk|su|org|info|name|pro|biz|tv|kz|am|mobi|me|us|ru)|www\s?\.|(http:|https:))";

        if (preg_match($reg, $message)) {
            return false;
        } else {
            return true;


        }

    }

    public static function CheckMate($message) {
        global $USER, $DB, $CACHE_MANAGER, $APPLICATION;
        static $arFilterPattern = array();
        $filter = array();
        $pattern = array();
        $replacement = array();
        $cenzura=0;
        $status_mate=0;

        $message_old=$message;

        if (!array_key_exists(LANGUAGE_ID, $arFilterPattern))
        {
            $cache_id = "b_forum_filter_".\Bitrix\Main\Config\Option::get("forum", "FILTER_DICT_W", false, LANGUAGE_ID);
            if (CACHED_b_forum_filter !== false && $CACHE_MANAGER->Read(CACHED_b_forum_filter, $cache_id, "b_forum_filter"))
            {
                $arFilterPattern[LANGUAGE_ID] = $CACHE_MANAGER->Get($cache_id);
            }
            else
            {
                $db_res = \CFilterUnquotableWords::GetList(array(),
                    array("USE_IT"=>"Y", "DICTIONARY_ID"=>\Bitrix\Main\Config\Option::get("forum", "FILTER_DICT_W", false, LANGUAGE_ID)));
                $replace = \Bitrix\Main\Config\Option::get("forum", "FILTER_RPL", "*");
                while ($res = $db_res->Fetch())
                {
                    if (trim($res["PATTERN"]) <> '' )
                    {
                        $arFilterPattern[LANGUAGE_ID]["pattern"][] = trim($res["PATTERN"]);
                        $status_mate=10;
                        $arFilterPattern[LANGUAGE_ID]["replacement"][] = $res["REPLACEMENT"] <> '' ? " ".$res["REPLACEMENT"]." " : " ".$replace." ";
                    }
                }
            }
        }

        $pattern = $arFilterPattern[LANGUAGE_ID]["pattern"];
        $replacement = $arFilterPattern[LANGUAGE_ID]["replacement"];

        ksort($pattern); ksort($replacement);
        $message = '  '.$message.'  ';
        switch (\Bitrix\Main\Config\Option::get("forum", "FILTER_ACTION", "rpl"))
        {
            case "rpl":
                $message = preg_replace($pattern, $replacement, $message);
                break;
            case "del":
                $message = preg_replace($pattern, '', $message);
                break;
        }


        $message=trim($message);
        $message_old=trim(trim($message_old));

        //echo $message_old.':'.strlen($message_old);
        //echo $message.':'.strlen($message);

        if (!CultureHelpers::CheckSite($message)) return false;
        if (strlen($message_old)>strlen($message)) return false;
        if (strlen($message_old)==strlen($message)) return true;



    }



    public static function showPageTitle() {
        global $APPLICATION;
        $APPLICATION->ShowProperty("page_title");
        echo 'Культура для школьников';
    }

    public static function getCities($param=array()) {
        return \Custom\GeoObjects\MainObjects::getCityList($param);
    }

    public static function getRegions($param=array()) {
        return \Custom\GeoObjects\MainObjects::getRegionList($param);
    }

    public static function getUserRights() {
        $data = [];

        $obEnum = new \CUserFieldEnum;
        $rsEnum = $obEnum->GetList(array(), array("USER_FIELD_ID" => 27));
        while($arEnum = $rsEnum->Fetch()) {
            $data[] = $arEnum;
        }

        return $data;
    }

    public static function getContacts() {
        $entityContacts = self::getHblocEntity(4);
        $resultContacts = $entityContacts::getList(array(
            "select" => array("*"),
            "order" => array("UF_NAME"=>"ASC"),
            "filter" => Array(),
        ));
        $list = array();
        while ($c = $resultContacts->Fetch()) {
            $list[$c['UF_NAME']] = array('ID' => $c['ID'], 'VALUE' => $c['UF_VALUE']);
        }
        return $list;
    }

    public static function saveContactItem($id, $val) {
        $entityContacts = self::getHblocEntity(4);
        return $entityContacts::update($id, array('UF_VALUE' => $val));
    }

    public static function getHblocEntity($id) {
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($id)->fetch();
        return \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
    }

    public static function getCultureCategoryName($id) {
        $name = '';
        $item = self::getCultureCategory($id);
        if (!empty($item['NAME'])) $name = $item['NAME'];
        return $name;
    }

    public static function getCultureCategory($id) {
        $item = array();
        $interests = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>self::CULTURE_CATEGORY_IBLOCK_ID, 'ID'=>(int)$id), false, Array(), Array("ID", "NAME", "PROPERTY_TAB_COLOR"));
        if(($ob = $interests->GetNextElement()) && ($fields = $ob->GetFields())) {
            $item = $fields;
        }
        return $item;
    }

    public static function getCultureCategoryList() {
        $list = array();
        $interests = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>self::CULTURE_CATEGORY_IBLOCK_ID, 'ACTIVE' => 'Y'), false, Array(), Array("ID", "NAME"));
        while($ob = $interests->GetNextElement()) {
            if ($fields = $ob->GetFields()) $list[] = $fields;
        }
        return $list;
    }

    public static function getUser($uid) {
        $query = new \Bitrix\Main\Entity\Query(\Bitrix\Main\UserTable::getEntity());
        $query->registerRuntimeField('USER', [
                'data_type' => 'Bitrix\Main\UserTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );

        $query->setSelect(Array(
            'NAME',
            'ID',
            'EMAIL',
            'PERSONAL_PHOTO',
            'SECOND_NAME',
            'LAST_NAME',
            'PERSONAL_STATE',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
            'WORK_PROFILE',
            'UF_SCHOOL',
            'UF_INTERESTS',
        ));
        $query->setFilter(array("ID"=>(int)$uid));
        $db_res = $query->exec();
        return $db_res->fetch();
    }

}