<?php
namespace Custom\Culture;

use Bitrix\Main\Loader;
use Bitrix\Catalog\PriceTable;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\ElementPropertyTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;

Loader::includeModule('iblock');
Loader::includeModule('catalog');
Loader::IncludeModule('highloadblock');

class CultureMentors
{

    public static function getList($param=array()) {
        $query = self::getQuery();

        $query->setSelect(Array(
            'NAME',
            'ID',
            'EMAIL',
            'PERSONAL_PHOTO',
            'SECOND_NAME',
            'LAST_NAME',
            'PERSONAL_STATE',
            'PERSONAL_CITY',
            'PERSONAL_STREET',
            'UF_SCHOOL',
            'UF_INTERESTS',
            'POST_COUNT',
            'LECTURE_COUNT'
        ));

        $filter = Array("@ID"=>new \Bitrix\Main\DB\SqlExpression(self::subQueryUserGroup(7)));
        if (!empty($param['FILTER'])) {
            foreach($param['FILTER'] as $k=>$v) $filter[$k] = $v;
        }
        $query->setFilter($filter);

        if (!empty($param['ORDER'])) $query->setOrder($param['ORDER']);
        //$query->setGroup(array());
        if (!empty($param['LIMIT'])) $query->setLimit((int)$param['LIMIT']);
        if (!empty($param['OFFSET'])) $query->setOffset((int)$param['OFFSET']);

        $db_res = $query->exec();

        $list = array();

        while ($dbItem = $db_res->fetch()) {
            $list[] = $dbItem;
        }
        return $list;
    }

    public static function getCount($param=array()) {
        $query = self::getQuery();
        $query->registerRuntimeField('ELEMENTS_COUNT', [
            'data_type'=>'integer',
            'expression' => ['COUNT(%s)', 'ID']
        ]);
        $query->setSelect([
            'ELEMENTS_COUNT'
        ]);
        $filter = Array("@ID"=>new \Bitrix\Main\DB\SqlExpression(self::subQueryUserGroup(7)));
        if (!empty($param['FILTER'])) {
            foreach($param['FILTER'] as $k=>$v) $filter[$k] = $v;
        }
        $query->setFilter($filter);
        $db_res = $query->exec();
        $result = 0;
        if ($res = $db_res->fetch()) $result = (!empty($res['ELEMENTS_COUNT']))? (int)$res['ELEMENTS_COUNT'] : 0;
        return $result;
    }

    private static function getQuery() {
        $query = new \Bitrix\Main\Entity\Query(\Bitrix\Main\UserTable::getEntity());
        $query->registerRuntimeField('USER', [
                'data_type' => '\Bitrix\Main\UserTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );
        $query->registerRuntimeField('POST_COUNT', [
            'expression' => ['(' . self::subQueryBlockCount(2) . ')', 'ID']
        ]);
        $query->registerRuntimeField('LECTURE_COUNT', [
            'expression' => ['(' . self::subQueryBlockCount(25) . ')', 'ID']
        ]);
        return $query;
    }

    private static function subQueryBlockCount($iblockId) {
        $query = \Bitrix\Iblock\ElementTable::query();
        $query->registerRuntimeField('ELEMENT', [
                'data_type' => '\Bitrix\Iblock\ElementTable',
                'reference' => [
                    '=this.CODE' => 'ref.CODE',
                    '=this.ID' => 'ref.ID',
                    '=this.IBLOCK_ID' => 'ref.IBLOCK_ID',
                ],
            ]
        );
        $query->registerRuntimeField('PROPERTIES', array(
            "data_type"   => '\Bitrix\Iblock\ElementPropertyTable',
            'reference'    => array('=this.ID' => 'ref.IBLOCK_ELEMENT_ID'),
        ));

        $query->registerRuntimeField('PROPERTIES_CODE', array(
            "data_type"   => '\Bitrix\Iblock\PropertyTable',
            'reference'    => array('=this.PROPERTIES.IBLOCK_PROPERTY_ID' => 'ref.ID'),
        ));
        $query->registerRuntimeField('ELEMENTS_COUNT', [
            'data_type'=>'integer',
            'expression' => ['COUNT(%s)', 'ID']
        ]);
        $query->setFilter([
            'IBLOCK_ID' => $iblockId,
            'PROPERTIES_CODE.CODE'=>'UID',
            'PROPERTIES.VALUE'=> new \Bitrix\Main\DB\SqlExpression('%s')
        ]);
        $query->setSelect([
            'ELEMENTS_COUNT'
        ]);
        return $query->getQuery();
    }

    private static function subQueryUserGroup($group) {
        $query = new \Bitrix\Main\Entity\Query(\Bitrix\Main\UserGroupTable::getEntity());
        $query->setSelect(Array("D_USER_ID"));
        $query->setFilter(Array("GROUP_ID"=>$group));
        $query->registerRuntimeField(0, new \Bitrix\Main\Entity\ExpressionField('D_USER_ID', 'DISTINCT(USER_ID)'));
        return $query->getQuery();
    }



}