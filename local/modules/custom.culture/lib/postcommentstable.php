<?php
namespace Custom\Culture;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

class PostCommentsTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'post_comments';
    }

    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => 'ID',
            ),
            'UF_COMMENT' => array(
                'data_type' => 'text',
                'required' => true,
                'title' => 'UF_COMMENT',
            ),
            'UF_POST' => array(
                'data_type' => 'integer',
                'title' => 'UF_POST',
            ),
            'UF_UID' => array(
                'data_type' => 'integer',
                'title' => 'UF_UID',
            ),
            'UF_ACTIVE' => array(
                'data_type' => 'boolean',
                'title' => 'UF_ACTIVE',
            ),
            'UF_CREATED' => array(
                'data_type' => 'datetime',
                'title' => 'UF_CREATED',
            )
        );
    }
}