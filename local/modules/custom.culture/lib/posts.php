<?php
namespace Custom\Culture;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');
Loader::includeModule('catalog');
Loader::IncludeModule('highloadblock');

class Posts
{
    const IBLOCK_CODE = "articles_posts";
    const COMMENTS_ENTITY_NAME = "PostComments";

    public static function getComments($param=array()) {
        $table = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('=NAME'=>self::COMMENTS_ENTITY_NAME)));
        if($row = $table->fetch())
        {
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($row["ID"])->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();

            $fields = array(
                '*',
            );
            $runtime = array();

            $filter = (!empty($param['filter']))? $param['filter'] : array();
            $order = (!empty($param['order']))? $param['order'] : array();

            if (!empty($param['USER'])) {
                $fields['USER_ID'] = 'USER.ID';
                $fields['USER_NAME'] = 'USER.NAME';
                $fields['USER_LAST_NAME'] = 'USER.LAST_NAME';
                $fields['USER_PERSONAL_CITY'] = 'USER.PERSONAL_CITY';
                $fields['USER_LOGIN'] = 'USER.LOGIN';
                $runtime['USER'] = array(
                    'data_type' => '\Bitrix\Main\UserTable',
                    'reference' => array(
                        '=this.UF_UID' => 'ref.ID'
                    ),
                    'join_type' => 'left'
                );
            }

            if (!empty($param['POST'])) {
                $fields['POST_ID'] = 'POST.ID';
                $fields['POST_NAME'] = 'POST.NAME';
                $runtime['POST'] = array(
                    'data_type' => '\Bitrix\Iblock\ElementTable',
                    'reference' => array(
                        '=this.UF_POST' => 'ref.ID'
                    ),
                    'join_type' => 'left'
                );
            }

            $result = $entity::getList(array(
                'select' => $fields,
                'runtime' => $runtime,
                'order' => $order,
                'filter' => $filter
            ));
            return $result->fetchAll();
        }
        else return array();
    }

    public static function getCommentsCount($filter=array()) {
        $table = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('=NAME'=>self::COMMENTS_ENTITY_NAME)));
        if($row = $table->fetch())
        {
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($row["ID"])->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
            return $entity::getCount($filter);
        }
        else return 0;
    }

    public static function addComment($fields=array()) {
        $table = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('=NAME'=>self::COMMENTS_ENTITY_NAME)));
        if($row = $table->fetch())
        {
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($row["ID"])->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
            $fields['UF_CREATED'] = new \Bitrix\Main\Type\DateTime();
            $result = $entity::add($fields);
            if ($result->isSuccess()) return $result->getId();
            else return 0;
        }
        else return 0;
    }


}