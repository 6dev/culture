<?php
namespace Custom\Favorite;

use Bitrix\Main\Loader;
use Bitrix\Catalog\PriceTable;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\ElementPropertyTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;

Loader::includeModule('iblock');
Loader::includeModule('catalog');

class Favorites
{

    public static function getFavorites($param=array()) {
        $filter = Array();
        if (!empty($param['filter'])) $filter = $param['filter'];

        $queryCnt = self::getQuery();
        $queryCnt->registerRuntimeField('ELEMENTS_COUNT', [
            'data_type'=>'integer',
            'expression' => ['COUNT(%s)', 'ID']
        ]);
        $queryCnt->setFilter($filter);
        $queryCnt->setSelect([
            'ELEMENTS_COUNT'
        ]);
        $allCntRes = $queryCnt->exec();
        $allCnt = 0;
        if ($item = $allCntRes->fetch()) $allCnt = (int)$item['ELEMENTS_COUNT'];

        $query = self::getQuery();
		$query->setSelect(Array(  
            'ID',
			'USER_ID',
            'IBLOCK_ID',
            'ELEMENT_ID',
        ));

        $query->setFilter($filter);		
		if (!empty($param['order'])) $query->setOrder($param['order']);
        $offset = (!empty($param['offset']))? (int)$param['offset'] : 0;
        if (!empty($param['limit'])) {
            $query->setOffset($offset);
            $query->setLimit((int)$param['limit']);
        }
        $db_res = $query->exec();	
        $list = [];	
        $cnt = 0;		
        while ($item = $db_res->fetch()) {
			$list[] = $item;
			$cnt++;
		}
        return array('LIST' => $list, 'COUNT' => $cnt, 'ALL_COUNT' => $allCnt, 'OFFSET' => $offset);
    }

    public static function addFavorite($param=array()) {
		$fields = $param;
		$fields['MODULE'] = 'mentors';
		$fields['SITE_ID'] = SITE_ID;
		return favoritestable::add($fields);
	}

    public static function deleteFavorite($id) {
        return favoritestable::delete($id);
    }

    private static function getQuery() {
        $query = new \Bitrix\Main\Entity\Query(favoritestable::getEntity());
        $query->registerRuntimeField('CHAT', [
                'data_type' => 'Custom\Favorite\favoritestable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );

        $query->registerRuntimeField("SRC_USER", array(
                "data_type" => "Bitrix\Main\UserTable",
                'reference' => array('=this.USER_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            )
        );
        $query->registerRuntimeField("DST_USER", array(
                "data_type" => "Bitrix\Main\UserTable",
                'reference' => array('=this.USER_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            )
        );
        return $query;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}