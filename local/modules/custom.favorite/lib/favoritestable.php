<?php
namespace Custom\Favorite;
 
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
  
class favoritestable extends Entity\DataManager
{

   public static function getTableName()
   {
      return 'custom_favorite';
   }

   public static function getMap()
   {
      return array(
        'ID' => array(
            'data_type' => 'integer',
            'primary' => true,
            'autocomplete' => true,
            'title' => 'ID',
        ),
        'USER_ID' => array(
            'data_type' => 'integer',
            'required' => true,
            'title' => 'USER_ID',
        ),
		 'IBLOCK_ID' => array(
            'data_type' => 'integer',
            'required' => true,
            'title' => 'IBLOCK_ID',
        ),
		 'ELEMENT_ID' => array(
            'data_type' => 'integer',
            'required' => true,
            'title' => 'ELEMENT_ID',
        ),
      );
   }
}