<?php
namespace Custom\GeoObjects;
 
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
  
class GeoRegionsTable extends Entity\DataManager
{

   public static function getTableName()
   {
      return 'geo_objects_t_region';
   }

   public static function getMap()
   {
      return array(
        'ID' => array(
            'data_type' => 'integer',
            'primary' => true,
            'autocomplete' => true,
            'title' => 'ID',
        ),
        'pk_i_id' => array(
            'data_type' => 'integer',
            'required' => true,
            'title' => 'pk_i_id',
        ),
        'fk_c_country_code' => array(
              'data_type' => 'string',
              'required' => true,
              'title' => 'fk_c_country_code',
        ),
        's_name' => array(
              'data_type' => 'string',
              'required' => true,
              'title' => 's_name',
        ),
        's_slug' => array(
              'data_type' => 'string',
              'required' => true,
              'title' => 's_slug',
        ),
        'b_active' => array(
              'data_type' => 'integer',
              'title' => 'b_active',
        ),
      );
   }
}