<?php
namespace Custom\GeoObjects;

use Bitrix\Main\Loader;


class MainObjects
{

    public static function getRegionList($param=array()) {
        $filter = array();
        if (!empty($param['ID'])) $filter['ID'] = $param['ID'];
        if (!empty($param['REGION_ID'])) $filter['pk_i_id'] = $param['REGION_ID'];
        if (!empty($param['NAME'])) $filter['s_name'] = $param['NAME'];
        $list = GeoRegionsTable::getList(
            array(
                'select' => array('ID' => 'ID', 'REGION_ID' =>'pk_i_id', 'NAME' => 's_name'),
                'filter' => $filter,
                'order' => array('s_name' => 'ASC')
            )
        );
        return $list->fetchAll();
    }

    public static function getCityList($param=array())
    {
        $filter = array();
        if (!empty($param['ID'])) $filter['ID'] = $param['ID'];
        if (!empty($param['CITY_ID'])) $filter['pk_i_id'] = $param['CITY_ID'];
        if (!empty($param['NAME'])) $filter['s_name'] = $param['NAME'];
        if (!empty($param['REGION_ID'])) $filter['fk_i_region_id'] = $param['REGION_ID'];
        $list = GeoCitiesTable::getList(
            array(
                'select' => array('ID' => 'ID', 'CITY_ID' => 'pk_i_id', 'REGION_ID' => 'fk_i_region_id', 'NAME' => 's_name'),
                'filter' => $filter,
                'order' => array('s_name' => 'ASC')
                )
        );
        return $list->fetchAll();
    }


}