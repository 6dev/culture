<?
// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/include.php"); // инициализация модуля
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/prolog.php"); // пролог модуля

// подключим языковой файл
IncludeModuleLangFile(__FILE__);
global $USER;
// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("subscribe");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

// сформируем список закладок
$aTabs = array(
    array("DIV" => "edit1", "TAB" => "Поля сообщения", "ICON"=>"main_user_edit", "TITLE"=>"")
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$ID = intval($ID);		// идентификатор редактируемой записи

$message = null;		// сообщение об ошибке
$bVarsFromForm = false; // флаг "Данные получены с формы", обозначающий, что выводимые данные получены с формы, а не из БД.

// ******************************************************************** //
//                ОБРАБОТКА ИЗМЕНЕНИЙ ФОРМЫ                             //
// ******************************************************************** //
$msItem = array();
if(
    $REQUEST_METHOD == "POST" // проверка метода вызова страницы
    &&
    ($save!="" || $apply!="") // проверка нажатия кнопок "Сохранить" и "Применить"
    &&
    $POST_RIGHT=="W"          // проверка наличия прав на запись для модуля
    &&
    check_bitrix_sessid()     // проверка идентификатора сессии
)
{
    $moderated = 0;
    if (!empty($_REQUEST['MODERATED']) && ($_REQUEST['MODERATED'] == 'on')) $moderated = 1;
    $show_main_page = 0;
    if (!empty($_REQUEST['SHOW_MAIN_PAGE']) && ($_REQUEST['SHOW_MAIN_PAGE'] == 'on')) $show_main_page = 1;

    if ($save!="") {
        if($ID>0)
        {
            Custom\Live\LiveTable::update($ID, array(
                'MESSAGE' => $_REQUEST['MESSAGE'],
                'MODERATED' => $moderated,
            ));
            LocalRedirect("live.php");
        }
        else {
            Custom\Live\LiveLenta::addMessage(array('SRC_USER_ID' => $USER->GetID(),'DST_USER_ID' => (int)$_REQUEST['DST_USER_ID'], 'MESSAGE' => $_REQUEST['MESSAGE'], 'MODERATED' => $moderated, 'SHOW_MAIN_PAGE' => $show_main_page));
            LocalRedirect("live.php");
        }
    }

}


if($ID>0)
{
    $msItem = Custom\Live\LiveTable::getRow(array('filter' => array('ID'=>$ID)));
    if (empty($msItem['ID'])) $message = new CAdminMessage("Сообщение не найдено");
}

// ******************************************************************** //
//                ВЫВОД ФОРМЫ                                           //
// ******************************************************************** //

// установим заголовок страницы
$APPLICATION->SetTitle(($ID>0? "Редактирование сообщения №".$ID : "Создание сообщения"));

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// конфигурация административного меню
$aMenu = array(
    array(
        "TEXT"=>"Живая лента",
        "TITLE"=>"Живая лента",
        "LINK"=>"live.php?lang=".LANG,
        "ICON"=>"btn_list",
    )
);

if($ID>0)
{
    $aMenu[] = array("SEPARATOR"=>"Y");
    $aMenu[] = array(
        "TEXT"=>"Удалить",
        "TITLE"=>"Удалить",
        "LINK"=>'live.php?action_button=delete&ID='.$ID,
        "ICON"=>"btn_delete",
    );
    $aMenu[] = array("SEPARATOR"=>"Y");
}

// создание экземпляра класса административного меню
$context = new CAdminContextMenu($aMenu);

// вывод административного меню
$context->Show();
?>

<?
// если есть сообщения об ошибках или об успешном сохранении - выведем их.
if($_REQUEST["mess"] == "ok" && $ID>0)
    echo "ok";

if($message)
    echo $message->Show();
elseif($rubric->LAST_ERROR!="")
    echo "errrr";
?>

<?
// далее выводим собственно форму
?>
<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form">
<?// проверка идентификатора сессии ?>
<?echo bitrix_sessid_post();?>
<?
// отобразим заголовки закладок
$tabControl->Begin();
?>
<?
//********************
// первая закладка - форма редактирования параметров рассылки
//********************
$tabControl->BeginNextTab();

?>
    <input type="hidden" name="SITE_ID" value="s1">
    <input type="hidden" name="SRC_USER_ID" value="<?=(!empty($msItem['SRC_USER_ID']))? $msItem['SRC_USER_ID'] : 0;?>">
    <input type="hidden" name="MODULE" value="<?=$module_name?>">
    <tr>
        <td>Модерация</td>
        <td><input type="checkbox" name="MODERATED"<?=(!empty($msItem['MODERATED']) && ((int)$msItem['MODERATED'] == 1))? ' checked' : ''?>></td>
    </tr>
    <tr>
        <td>Сообщение</td>
        <td><textarea name="MESSAGE"><?=(!empty($msItem['MESSAGE']))? $msItem['MESSAGE'] : ''?></textarea></td>
    </tr>

<?
// завершение формы - вывод кнопок сохранения изменений
$tabControl->Buttons(
    array(
        "disabled"=>($POST_RIGHT<"W"),
        "back_url"=>"chat_edit.php?lang=".LANG,

    )
);
?>
    <input type="hidden" name="lang" value="<?=LANG?>">
<?if($ID>0 && !$bCopy):?>
    <input type="hidden" name="ID" value="<?=$ID?>">
<?endif;?>
<?
// завершаем интерфейс закладок
$tabControl->End();
?>

<?
// дополнительное уведомление об ошибках - вывод иконки около поля, в котором возникла ошибка
$tabControl->ShowWarnings("post_form", $message);
?>

<?
// дополнительно: динамическая блокировка закладки, если требуется.
?>
    <script language="JavaScript">
        <!--
        if(document.post_form.AUTO.checked)
            tabControl.EnableTab('edit2');
        else
            tabControl.DisableTab('edit2');
        //-->
    </script>

<?
// информационная подсказка
echo BeginNote();
?>
    <span class="required">*</span><?echo GetMessage("REQUIRED_FIELDS")?>
<?echo EndNote();?>

<?
// завершение страницы
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>