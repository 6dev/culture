<?php
namespace Custom\Live;

use Bitrix\Main\Loader;
use Bitrix\Catalog\PriceTable;
use Bitrix\Main\Entity\Query;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\ElementPropertyTable;
use Bitrix\Iblock\IblockElementProperty;
use \Bitrix\Iblock\PropertyEnumerationTable;

Loader::includeModule('iblock');
Loader::includeModule('catalog');

class LiveLenta
{

    public static function getMessages($param=array()) {
        $filter = Array();
        if (!empty($param['filter'])) $filter = $param['filter'];
        $allCnt = self::getCount($filter);
        $query = self::getQuery();
		$query->setSelect(Array(  
            'ID',
			'MODULE',
            'MESSAGE',
            'REF_ID',
			'SRC_USER_ID',
			'DST_USER_ID',
            'SHOW_MAIN_PAGE',
            'MODERATED',
            'SRC_USER_NAME' => 'SRC_USER.NAME',
            'SRC_USER_LAST_NAME' => 'SRC_USER.LAST_NAME',
            'SRC_USER_PERSONAL_CITY' => 'SRC_USER.PERSONAL_CITY',
			'SRC_USER_LOGIN' => 'SRC_USER.LOGIN',
			'SRC_USER_PERSONAL_PHOTO' => 'SRC_USER.PERSONAL_PHOTO',
            'DST_USER_NAME' => 'DST_USER.NAME',
            'DST_USER_LAST_NAME' => 'DST_USER.LAST_NAME',
            'DST_USER_PERSONAL_CITY' => 'DST_USER.PERSONAL_CITY',
            'DST_USER_LOGIN' => 'DST_USER.LOGIN',
            'DST_USER_PERSONAL_PHOTO' => 'DST_USER.PERSONAL_PHOTO',
        ));

        $query->setFilter($filter);		
		if (!empty($param['order'])) $query->setOrder($param['order']);
        $offset = (!empty($param['offset']))? (int)$param['offset'] : 0;
        if (!empty($param['limit'])) {
            $query->setOffset($offset);
            $query->setLimit((int)$param['limit']);
        }
        $db_res = $query->exec();	
        $list = [];	
        $cnt = 0;		
        while ($item = $db_res->fetch()) {
			$list[] = $item;
			if (!empty($param['answer'])) {
                $answer = self::getMessageItem(array('MODERATED' => 1, 'REF_ID'=>$item['ID']));
                if ($answer) $list[] = $answer;
            }
			$cnt++;
		}
        return array('LIST' => $list, 'COUNT' => $cnt, 'ALL_COUNT' => $allCnt, 'OFFSET' => $offset);
    }

    public static function getCount($filter = Array()) {
        $queryCnt = self::getQuery();
        $queryCnt->registerRuntimeField('ELEMENTS_COUNT', [
            'data_type'=>'integer',
            'expression' => ['COUNT(%s)', 'ID']
        ]);
        $queryCnt->setFilter($filter);
        $queryCnt->setSelect([
            'ELEMENTS_COUNT'
        ]);
        $allCntRes = $queryCnt->exec();
        $allCnt = 0;
        if ($item = $allCntRes->fetch()) $allCnt = (int)$item['ELEMENTS_COUNT'];
        return $allCnt;
    }

    public static function getMessageItem($filter=array()) {
        $query = self::getQuery();
        $query->setSelect(Array(
            'ID',
            'MODULE',
            'MESSAGE',
            'REF_ID',
            'SRC_USER_ID',
            'DST_USER_ID',
            'SHOW_MAIN_PAGE',
            'MODERATED',
            'SRC_USER_NAME' => 'SRC_USER.NAME',
            'SRC_USER_LAST_NAME' => 'SRC_USER.LAST_NAME',
            'SRC_USER_PERSONAL_CITY' => 'SRC_USER.PERSONAL_CITY',
            'SRC_USER_LOGIN' => 'SRC_USER.LOGIN',
            'SRC_USER_PERSONAL_PHOTO' => 'SRC_USER.PERSONAL_PHOTO',
            'DST_USER_NAME' => 'DST_USER.NAME',
            'DST_USER_LAST_NAME' => 'DST_USER.LAST_NAME',
            'DST_USER_PERSONAL_CITY' => 'DST_USER.PERSONAL_CITY',
            'DST_USER_LOGIN' => 'DST_USER.LOGIN',
            'DST_USER_PERSONAL_PHOTO' => 'DST_USER.PERSONAL_PHOTO',
        ));

        $query->setFilter($filter);
        $db_res = $query->exec();
        return $db_res->fetch();
    }

    public static function addMessage($param=array()) {
		$fields = $param;
		$fields['SITE_ID'] = SITE_ID;
		return LiveTable::add($fields);
	}

    public static function deleteMessage($id) {
        return LiveTable::delete($id);
    }
	
	  public static function updateMessage($id,$arF) {
        return LiveTable::Update($id,$arF);
    }

    private static function getQuery() {
        $query = new \Bitrix\Main\Entity\Query(LiveTable::getEntity());
        $query->registerRuntimeField('CHAT', [
                'data_type' => 'Custom\Live\LiveTable',
                'reference' => [
                    '=this.ID' => 'ref.ID',
                ],
            ]
        );

        $query->registerRuntimeField("SRC_USER", array(
                "data_type" => "Bitrix\Main\UserTable",
                'reference' => array('=this.SRC_USER_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            )
        );
        $query->registerRuntimeField("DST_USER", array(
                "data_type" => "Bitrix\Main\UserTable",
                'reference' => array('=this.DST_USER_ID' => 'ref.ID'),
                'join_type' => "LEFT"
            )
        );
        return $query;
    }

}