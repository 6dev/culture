<?php
use Bitrix\Main,
    Bitrix\Main\Entity,
    Bitrix\Main\Localization\Loc;

/**
 * Class MapCommentsTable
 */
class MapCommentsTable extends Entity\DataManager {

    /**
     * @return string
     */
    public static function getTableName() {
        return 'map_comments';
    }

    public static function getUfId()
    {
        return 'MAP_COMMENTS';
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    public static function getMap() {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\IntegerField('UF_USER_ID', array(
                'column_name' => 'UF_USER_ID',
                'required' => true
            )),
            new Entity\IntegerField('UF_PLACEMARK_ID', array(
                'column_name' => 'UF_PLACEMARK_ID',
                'required' => true
            )),
            new Entity\StringField('UF_COMMENT', array(
                'column_name' => 'UF_COMMENT',
                'required' => true
            )),
            new Entity\IntegerField('UF_ACTIVE', array(
                'column_name' => 'UF_ACTIVE',
            )),
        );
    } 

    public static function getCommentsByPlacemark($id) {
        $data = [];

        if (!$id) return $data;

        $arGetList = [
            'filter'  => ['UF_PLACEMARK_ID' => $id, 'UF_ACTIVE' => true],
            'order'   => ['ID' => 'ASC'],
            "cache"   => [
                "ttl"         => 3600,
                "cache_joins" => true
            ]
        ];

        $dbResult = MapCommentsTable::getList($arGetList);
        while ($arResult = $dbResult->Fetch()) {
            $data[] = $arResult;
        }

        return $data;
    }

}