<?php
use Bitrix\Main,
    Bitrix\Main\Entity,
    Bitrix\Main\Localization\Loc;

/**
 * Class MapLikesTable
 */
class MapLikesTable extends Entity\DataManager {

    /**
     * @return string
     */
    public static function getTableName() {
        return 'map_likes';
    }

    public static function getUfId()
    {
        return 'MAP_LIKES';
    }

    /**
     * @return array
     * @throws \Bitrix\Main\SystemException
     */
    public static function getMap() {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\IntegerField('UF_USER_ID', array(
                'column_name' => 'UF_USER_ID',
                'required' => true
            )),
            new Entity\IntegerField('UF_PLACEMARK_ID', array(
                'column_name' => 'UF_PLACEMARK_ID',
                'required' => true
            )),
        );
    } 

    public static function changeLike($user_id, $placemark_id) {
        if (!$user_id || !$placemark_id) return false;

        $arGetList = [
            'filter'  => ['UF_PLACEMARK_ID' => $placemark_id, 'UF_USER_ID' => $user_id],
            'order'   => ['ID' => 'ASC'],
            "cache"   => [
                "ttl"         => 3600,
                "cache_joins" => true
            ]
        ];

        $dbResult = self::getList($arGetList);
        if ($arResult = $dbResult->Fetch()) {
            self::delete($arResult['ID']);
        }
        else {
            self::add([
                'UF_USER_ID' => $user_id,
                'UF_PLACEMARK_ID' => $placemark_id
                ]);
        }        

        return true;
    }

    public static function getCountByPlacemark($placemark_id) {
        if (!$placemark_id) return 0;

        $count = 0;

        $arGetList = self::getList([
            'select' => ['CNT'],
            'filter' => [
                '=UF_PLACEMARK_ID' => $placemark_id,
            ],
            'runtime' => [new Entity\ExpressionField('CNT', 'COUNT(*)')],
        ]);
        if ($arResult = $arGetList->Fetch()) {
            $count = $arResult['CNT'];
        }

        return $count;
    }

    public static function checkLike($user_id, $placemark_id) {
        if (!$user_id || !$placemark_id) return false;

        $arGetList = [
            'filter'  => ['UF_PLACEMARK_ID' => $placemark_id, 'UF_USER_ID' => $user_id],
            'order'   => ['ID' => 'ASC'],
            "cache"   => [
                "ttl"         => 3600,
                "cache_joins" => true
            ]
        ];

        $dbResult = self::getList($arGetList);
        if ($arResult = $dbResult->Fetch()) {
            return true;
        }
        else {
            return false;
        }

    }

}