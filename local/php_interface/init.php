<?
if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
    define("SITE_BASE_URL", 'https://'.$_SERVER[HTTP_HOST]);
}
else define("SITE_BASE_URL", 'http://'.$_SERVER[HTTP_HOST]);

CModule::IncludeModule("custom.culture");
CModule::IncludeModule("custom.chat");
CModule::IncludeModule("custom.afficheorders");
CModule::IncludeModule("custom.geoobjects");

CModule::IncludeModule("custom.favorite");
CModule::IncludeModule("custom.live");
CModule::IncludeModule("custom.claim");

CModule::IncludeModule("forum");

Custom\Culture\CultureHelpers::setPageTitle('');

AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");

function OnBeforeUserUpdateHandler(&$arFields)
{

    if ($arFields["UF_CLS"] && (intval($arFields["UF_CLS"]) < 1 || intval($arFields["UF_CLS"]) > 11))
    {
        global $APPLICATION;
        $APPLICATION->throwException("Поле класс должно быть в промежутке от 1 до 11");
        return false;
    }
}


AddEventHandler("main", "OnBuildGlobalMenu", "CustomOnBuildGlobalMenu");
function CustomOnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
{
    $aMenu = array(
        "parent_menu" => "global_menu_services",
        "sort"        => 100,
        "url"         => "contacts_edit.php?",
        "text"        => "Контакты",
        "title"       => "Контакты",
        "icon"        => "form_menu_icon",
        "page_icon"   => "form_page_icon",
        "items_id"    => "culture_contacts",
        "items"       => array(),
    );
    $aModuleMenu[] = $aMenu;
}


if ((COption::GetOptionString("webprostor.smtp", "USE_MODULE") == "Y") && (CModule::IncludeModule("webprostor.smtp")))
{
    function send_email_html($to, $subj, $html) {
        $smtp = new CWebprostorSmtp("s1");
        $headers = "MIME-Version: 1.0\r\nContent-Type: text/html; charset=UTF-8\r\n";
        $headers .= "From: ".COption::GetOptionString("webprostor.smtp", "S1_FROM")." <".COption::GetOptionString("webprostor.smtp", "S1_LOGIN").">\r\n";
        $headers .= "To: ".$to."\r\n\r\n";
        return $smtp->SendMail($to, $subj, $html, $headers, '');
    }
}
else {
    function send_email_html($to, $subj, $html) {
        return false;
    }
}


function NumberWordEndingsEx($num, $arEnds = false) {
   $lang = LANGUAGE_ID;
   if ($arEnds===false) {
      $arEnds = array('ов', 'ов', '', 'а');
   }
   if ($lang=='ru') {
      if (strlen($num)>1 && substr($num, strlen($num)-2, 1)=='1') {
         return $arEnds[0];
      } else {
         $c = IntVal(substr($num, strlen($num)-1, 1));
         if ($c==0 || ($c>=5 && $c<=9)) {
            return $arEnds[1];
         } elseif ($c==1) {
            return $arEnds[2];
         } else {
            return $arEnds[3];
         }
      }
   } elseif ($lang=='en') {
      if (IntVal($num)>1) {
         return 's';
      }
      return '';
   } else {
      return '';
   }
}

?>