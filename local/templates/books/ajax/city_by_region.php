<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html; charset=utf-8');

require($_SERVER["DOCUMENT_ROOT"]. "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

Loader::includeModule('iblock');
Loader::IncludeModule("form");

global $APPLICATION;

if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest' || !$_POST['region']) {
	die;
}

$data = [];

$cityList = \Custom\Culture\CultureHelpers::getCities(['REGION_ID' => $_POST['region']]);
foreach ($cityList as $city) {
	$data[] = $city['NAME'];
}

echo json_encode($data);
die;