<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html; charset=utf-8');

require($_SERVER["DOCUMENT_ROOT"]. "/bitrix/modules/main/include/prolog_before.php");
define("NO_KEEP_STATISTIC", true);

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

Loader::includeModule('iblock');
Loader::IncludeModule("form");

global $APPLICATION;

if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest' || !$_POST['email'] || !$_POST['message']) {
	die;
}

$arData = array();

$arData['EMAIL'] 	= $_POST['email'];
$arData['MESSAGE']	= $_POST['message'];

$event = new CEvent;
$event->Send('FORM_SUPPORT', SITE_ID, $arData);

$arValues = array (
    "form_text_31"                  => $arData['EMAIL'],   
    "form_text_32"                  => $arData['MESSAGE']   
);

$error = CForm::Check(3, $arValues);
 
if ($RESULT_ID = CFormResult::Add(3, $arValues)) {
    echo "Результат #".$RESULT_ID." успешно создан";
}
else {
    global $strError;
    echo $strError;
}

 