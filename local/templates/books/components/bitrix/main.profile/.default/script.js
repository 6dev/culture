$(document).ready(function() {

	$('.change-password').click(function() {

		location.href = '/personal/change_password/';

		return false;

	});

	$('#signup-region').change(function() {

		var value = $(this).find('option:selected').data('id');

     	$.ajax({
            type: "POST",
            cache: false,
            url: '/local/templates/books/ajax/city_by_region.php',
            data: 'region=' + value,
            dataType: 'JSON',
            success: function (data) {

            	$('#signup-town').empty().trigger("change");

            	var newOption = new Option('Населённый пункт', '', false, false);
				$('#signup-town').append(newOption).trigger('change');

            	$.each(data, function(i, v) {
					var newOption = new Option(v, v, false, false);
					$('#signup-town').append(newOption).trigger('change');

            	});
            }
        });		

		return false;

	});

	$('.interest__item').click(function() {

		$(this).toggleClass('active');
		$(this).find('input').prop("checked", !$(this).find('input').prop("checked"));

		return false;

	});

	$('input[name="PERSONAL_BIRTHDAY"]').click(function() {

		if (!$(this).is("[readonly]")) {
			BX.calendar({node: this, field: this, bTime: false});
		}

	});


});