<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
  die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
  CJSCore::Init('phone_auth');
}

?>

<main class="main settings">
  <section class="header--personal-area">
    <h2>
      Настройки
    </h2>

  <?ShowError($arResult["strProfileError"]);?>
  <?
  if ($arResult['DATA_SAVED'] == 'Y')
    ShowNote(GetMessage('PROFILE_DATA_SAVED'));
  ?>

  <form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
  <?=$arResult["BX_SESSION_CHECK"]?>
  <input type="hidden" name="lang" value="<?=LANG?>" />
  <input type="hidden" name="ID" value=<?=$arResult["ID"]?> />

      <div class="info--user">
        <div class="info--user__avatar">
          <?php if ($photo = $arResult["arUser"]["PERSONAL_PHOTO"]) : ?>
              <img src="<?=CFile::GetPath($photo)?>"/>
          <?php else : ?>
              <img src="/local/templates/books/img/no-photo.png"/>
          <?php endif; ?>
        </div>
        <div class="info--user__awards">
          <div class="awards--el transparent">
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
              <path
                d="M15.4167 8.33333H14.1667V5C14.1667 4.08333 13.4167 3.33333 12.5 3.33333H9.16667V2.08333C9.16667 0.933333 8.23333 0 7.08333 0C5.93333 0 5 0.933333 5 2.08333V3.33333H1.66667C0.75 3.33333 0.00833333 4.08333 0.00833333 5V8.16667H1.25C2.49167 8.16667 3.5 9.175 3.5 10.4167C3.5 11.6583 2.49167 12.6667 1.25 12.6667H0V15.8333C0 16.75 0.75 17.5 1.66667 17.5H4.83333V16.25C4.83333 15.0083 5.84167 14 7.08333 14C8.325 14 9.33333 15.0083 9.33333 16.25V17.5H12.5C13.4167 17.5 14.1667 16.75 14.1667 15.8333V12.5H15.4167C16.5667 12.5 17.5 11.5667 17.5 10.4167C17.5 9.26667 16.5667 8.33333 15.4167 8.33333Z"
                fill="white"
              />
            </svg>
          </div>
          <div class="awards--el silver">
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
              <path
                d="M15.4167 8.33333H14.1667V5C14.1667 4.08333 13.4167 3.33333 12.5 3.33333H9.16667V2.08333C9.16667 0.933333 8.23333 0 7.08333 0C5.93333 0 5 0.933333 5 2.08333V3.33333H1.66667C0.75 3.33333 0.00833333 4.08333 0.00833333 5V8.16667H1.25C2.49167 8.16667 3.5 9.175 3.5 10.4167C3.5 11.6583 2.49167 12.6667 1.25 12.6667H0V15.8333C0 16.75 0.75 17.5 1.66667 17.5H4.83333V16.25C4.83333 15.0083 5.84167 14 7.08333 14C8.325 14 9.33333 15.0083 9.33333 16.25V17.5H12.5C13.4167 17.5 14.1667 16.75 14.1667 15.8333V12.5H15.4167C16.5667 12.5 17.5 11.5667 17.5 10.4167C17.5 9.26667 16.5667 8.33333 15.4167 8.33333Z"
                fill="white"
              />
            </svg>
          </div>
          <div class="awards--el gold">
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
              <path
                d="M15.4167 8.33333H14.1667V5C14.1667 4.08333 13.4167 3.33333 12.5 3.33333H9.16667V2.08333C9.16667 0.933333 8.23333 0 7.08333 0C5.93333 0 5 0.933333 5 2.08333V3.33333H1.66667C0.75 3.33333 0.00833333 4.08333 0.00833333 5V8.16667H1.25C2.49167 8.16667 3.5 9.175 3.5 10.4167C3.5 11.6583 2.49167 12.6667 1.25 12.6667H0V15.8333C0 16.75 0.75 17.5 1.66667 17.5H4.83333V16.25C4.83333 15.0083 5.84167 14 7.08333 14C8.325 14 9.33333 15.0083 9.33333 16.25V17.5H12.5C13.4167 17.5 14.1667 16.75 14.1667 15.8333V12.5H15.4167C16.5667 12.5 17.5 11.5667 17.5 10.4167C17.5 9.26667 16.5667 8.33333 15.4167 8.33333Z"
                fill="white"
              />
            </svg>
          </div>
        </div>
        <div class="replacement-avatar">
          <label>
            <span class="load-img__avatar">Изменить отображение аватара</span>
            <input style="position: absolute; opacity: 0" hidden class="file-input" type="file" />
          </label>
        </div> 
        <div class="input-container">
          <label data-text="Логин">
            <input type="text" name="LOGIN" required readonly maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
          </label>
          <label data-text="Почта">
            <input required readonly type="email" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>" />
            <p>Изменить</p></label
          >

          <label data-text="Пароль">
                  <input required readonly type="password" />
                  <a href="/personal/change_password/">Изменить пароль</a>
                </label>

          <hr />
          <label data-text="Имя"> 
            <input required readonly type="text" name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>" />
            <p>Изменить</p>
          </label>
      <label data-text="Фамилия"> 
            <input required readonly type="text" name="LAST_NAME" maxlength="50" value="<?=$arResult["arUser"]["LAST_NAME"]?>" />
            <p>Изменить</p>
          </label>
      <label data-text="Отчество"> 
            <input readonly type="text" name="SECOND_NAME" maxlength="50" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" />
            <p>Изменить</p>
          </label>

          <label data-text="Дата рождения">
            <input required readonly placeholder="ДД/ММ/ГГГГ" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" type="text" name="PERSONAL_BIRTHDAY" />
            <p>Изменить</p></label
          >
          <label data-text="Регион"
            >
            <select id="signup-region" class="location-control" name="PERSONAL_STATE" type="text" required>
        <option value="" disabled selected>Регион</option>
        <?
        $regionList = \Custom\Culture\CultureHelpers::getRegions();
        $region_id = null;
        ?>              
        <?foreach($regionList as $region):?>
          <?php
          if ($region['NAME'] == $arResult["arUser"]['PERSONAL_STATE']) {
            $selected = 'selected';
            $region_id = $region['REGION_ID'];
          }
          else {
            $selected = '';
          }
          ?>
          <option value="<?=$region['NAME']?>" data-id="<?=$region['REGION_ID']?>" <?=$selected?>><?=$region['NAME']?></option>
        <?endforeach;?>
      </select>

            <p>Изменить</p>
          </label>
          <label data-text="Населенный пункт">
            <select id="signup-town" class="location-control" name="PERSONAL_CITY" type="text" required>
        <option value="" disabled selected>Населённый пункт</option>
        <?$cityList = \Custom\Culture\CultureHelpers::getCities(['REGION_ID' => $region_id]);?>
        <?foreach($cityList as $city):?>
        <?php
        if ($city['NAME'] == $arResult["arUser"]['PERSONAL_CITY']) {
          $selected = 'selected';
        }
        else {
          $selected = '';
        }
        ?>
        <option value="<?=$city['NAME']?>" <?=$selected?>><?=$city['NAME']?></option>
        <?endforeach;?>
      </select>
            <p class="false-true">Изменить</p>
          </label>
          <label data-text="Школа">
            <input required readonly type="text" name="UF_SCHOOL" maxlength="50" value="<?=$arResult["arUser"]["UF_SCHOOL"]?>" />
            <p>Изменить</p>
          </label>
        </div>
        <div class="last-input-row">
          <label data-text="Класс">
            <input required readonly type="number" min="1" max="11" name="UF_CLS" value="<?=$arResult["arUser"]["UF_CLS"]?>" />
            <p>Изменить</p>
          </label>
        </div>
        <div class="interests--content" data-text="Интересы">
      <ul class="tab__interests">
          <?$iterests = Custom\Culture\CultureHelpers::getCultureCategoryList();?>
          <?foreach ($iterests as $item):?>
            <?php if (in_array($item['ID'], $arResult["arUser"]['UF_INTERESTS'])) {
              $class = 'active';
              $checked = 'checked';
            }
            else {
              $class = '';
              $checked = '';
            }
            ?>
              <li class="interest__item <?=$class?>" value="<?=$item['ID']?>">
                <?=$item['NAME']?>
                <input type="checkbox" style="display: none;" <?=$checked?> name="UF_INTERESTS[]" value="<?=$item['ID']?>">
              </li>
          <?endforeach;?>
      </ul>
          <p>Изменить</p>
        </div>
        <hr />
        <div class="checked-variable">
          <div class="label-container" data-text="Что видят другие пользователи">

          <?php foreach (Custom\Culture\CultureHelpers::getUserRights() as $right) : ?>
          <? $checked = (in_array($right['ID'], $arResult["arUser"]['UF_RIGHT'])) ? 'checked' : ''; ?>
            <div class="label-el">
              <input type="checkbox" name="UF_RIGHT[]" id="right_<?=$right['ID']?>" <?=$checked?> value="<?=$right['ID']?>" />
              <label for="right_<?=$right['ID']?>"><?=$right['VALUE']?></label>
            </div> 
          <?php endforeach; ?>

          </div>
        </div>
        <button type="submit">Сохранить изменения</button>
        <input type="hidden" name="save" value="Y">
      </div>
    </form>
  </section>
</main>
