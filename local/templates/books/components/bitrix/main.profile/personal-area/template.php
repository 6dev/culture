<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

use Custom\Claim\MainClaim;

use Custom\Live\LiveLenta;
use Custom\Favorite\Favorites;
use Custom\Afficheorders\Orders;
use Custom\Culture\CultureHelpers;

	global $USER;
	
	
	$this->addExternalJS(SITE_TEMPLATE_PATH."/js/claim.js");
	
	

if ($_REQUEST['delete']>0)
{
	$param = array();
        $param['filter'] = array(
            'SRC_USER_ID'=>$USER->GetID(),
			'ID'=>$_REQUEST['delete'],
			'MODULE'=>'users'
        );
		
$lifes =  LiveLenta::getMessages($param);			
		
foreach ($lifes['LIST'] as $del)
{		
	LiveLenta::deleteMessage($del['ID']);

		

}	
	
	
}

if ($_REQUEST['user']>0)
{
	
$rsUser = CUser::GetByID($_REQUEST['user']);
$arUser = $rsUser->Fetch();
	
$arResult["arUser"]=$arUser;

if ($_REQUEST['user']!=$USER->GetID()) define('HIDEBLOCKS','Y');

}





 $param = array();
        $param['filter'] = array(
            'SRC_USER_ID'=>$arResult["arUser"]['ID'],
			'MODULE'=>'mentors'
        );
     
$items =  LiveLenta::getMessages($param);	 
	 
$iblocks=array(15,2,25,26,21);

 $param = array();
        $param['filter'] = array(
            'USER_ID'=>$arResult["arUser"]['ID'],
			
        );
$favorites =  Favorites::getFavorites($param);		

// $param2=array();
// $life =  Life::getMessages($param2);		



 $param = array();
        $param['filter'] = array(
            'USER_ID'=>$arResult["arUser"]['ID']
        );
     
$afisha =  Orders::getList($param);	 


// комментарии 
global $DB;
$arComment['ALL_COUNT']=0;
$results=$DB->Query('SELECT * FROM map_comments WHERE UF_USER_ID='.$arResult["arUser"]['ID']);
while ($row = $results->Fetch())
{
	$arComment['ALL_COUNT']=$arComment['ALL_COUNT']+1;
}


$img='/img/svg/account-default.svg';
if ($arResult["arUser"]["PERSONAL_PHOTO"]>0) $img=CFile::GetPath($arResult["arUser"]["PERSONAL_PHOTO"]);



?>

<main class="main personal-area">
      <div class="container">
        <section class="header--personal-area">
		
		<? if (HIDEBLOCKS!='Y') { ?>
		
          <a href="/personal/settings/" class="settings">
            <i class="material-icons settings__icon"><?=GetMessage('SETTING');?>
			


		
			</i>
          </a>
		  
		<? } ?>
		
		
		
		
		
		
		
          <div class="info--user">
		  
		  <? if ((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('1',$arParams['BLOCKS_FOR_PUBLIC']))) { ?> 
            <div class="info--user__avatar">
              <img src="<?=$img;?>" alt="avatar" />
            </div>
			
		  <? } ?>
            <div class="info--user__awards">
              <div class="awards--el">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
                  <path
                    d="M15.4167 8.33333H14.1667V5C14.1667 4.08333 13.4167 3.33333 12.5 3.33333H9.16667V2.08333C9.16667 0.933333 8.23333 0 7.08333 0C5.93333 0 5 0.933333 5 2.08333V3.33333H1.66667C0.75 3.33333 0.00833333 4.08333 0.00833333 5V8.16667H1.25C2.49167 8.16667 3.5 9.175 3.5 10.4167C3.5 11.6583 2.49167 12.6667 1.25 12.6667H0V15.8333C0 16.75 0.75 17.5 1.66667 17.5H4.83333V16.25C4.83333 15.0083 5.84167 14 7.08333 14C8.325 14 9.33333 15.0083 9.33333 16.25V17.5H12.5C13.4167 17.5 14.1667 16.75 14.1667 15.8333V12.5H15.4167C16.5667 12.5 17.5 11.5667 17.5 10.4167C17.5 9.26667 16.5667 8.33333 15.4167 8.33333Z"
                    fill="white"
                  />
                </svg>
              </div>
              <div class="awards--el">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
                  <path
                    d="M15.4167 8.33333H14.1667V5C14.1667 4.08333 13.4167 3.33333 12.5 3.33333H9.16667V2.08333C9.16667 0.933333 8.23333 0 7.08333 0C5.93333 0 5 0.933333 5 2.08333V3.33333H1.66667C0.75 3.33333 0.00833333 4.08333 0.00833333 5V8.16667H1.25C2.49167 8.16667 3.5 9.175 3.5 10.4167C3.5 11.6583 2.49167 12.6667 1.25 12.6667H0V15.8333C0 16.75 0.75 17.5 1.66667 17.5H4.83333V16.25C4.83333 15.0083 5.84167 14 7.08333 14C8.325 14 9.33333 15.0083 9.33333 16.25V17.5H12.5C13.4167 17.5 14.1667 16.75 14.1667 15.8333V12.5H15.4167C16.5667 12.5 17.5 11.5667 17.5 10.4167C17.5 9.26667 16.5667 8.33333 15.4167 8.33333Z"
                    fill="white"
                  />
                </svg>
              </div>
              <div class="awards--el">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
                  <path
                    d="M15.4167 8.33333H14.1667V5C14.1667 4.08333 13.4167 3.33333 12.5 3.33333H9.16667V2.08333C9.16667 0.933333 8.23333 0 7.08333 0C5.93333 0 5 0.933333 5 2.08333V3.33333H1.66667C0.75 3.33333 0.00833333 4.08333 0.00833333 5V8.16667H1.25C2.49167 8.16667 3.5 9.175 3.5 10.4167C3.5 11.6583 2.49167 12.6667 1.25 12.6667H0V15.8333C0 16.75 0.75 17.5 1.66667 17.5H4.83333V16.25C4.83333 15.0083 5.84167 14 7.08333 14C8.325 14 9.33333 15.0083 9.33333 16.25V17.5H12.5C13.4167 17.5 14.1667 16.75 14.1667 15.8333V12.5H15.4167C16.5667 12.5 17.5 11.5667 17.5 10.4167C17.5 9.26667 16.5667 8.33333 15.4167 8.33333Z"
                    fill="white"
                  />
                </svg>
				
				
				
	
				
				
				
				
				
				
				
				
              </div>
            </div>
			<? if ((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('1',$arParams['BLOCKS_FOR_PUBLIC']))) { ?> 
            <div class="info--user__name">

              <p><?=$arResult["arUser"]["LOGIN"];?></p>   
			  
			  	  <div class="menu-control">
                    <i class="material-icons complaint claim" type-data="user" data-id="<?=$arResult["arUser"]["ID"];?>">
                      error
                    </i>
					</div>
			  
			  
            </div>
			<? } ?>
			
			<? if ((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('2',$arParams['BLOCKS_FOR_PUBLIC']))) { ?> 
			
            <div class="info--user__lvl">
              Lvl. 32 333
            </div>
			
			<? } ?>
			
			
			<? if ((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('1',$arParams['BLOCKS_FOR_PUBLIC']))) { ?> 
			
            <div class="info--user__progress">
              <div class="header--progress">
                <p>
                  <i class="material-icons">grade</i>
                  Рейтинг — 64
                </p>
                <a href="#">
                  Что такое рейтинг?
                </a>
              </div>
              <div class="progress">
                <span style="width: 64%;"></span>
              </div>
            </div>
			<? } ?>
            <div class="info--user__footer">
			
			<? if ((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('3',$arParams['BLOCKS_FOR_PUBLIC']))) { ?> 
			
              <div class="footer--el">
                <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" fill="none">
                  <path
                    d="M32.0003 48.3556L30.9123 47.36C30.2368 46.7342 14.3008 31.8649 14.3008 17.6996C14.3008 13.0053 16.1655 8.50339 19.4849 5.18408C22.8042 1.86477 27.3061 0 32.0003 0C36.6945 0 41.1965 1.86477 44.5158 5.18408C47.8351 8.50339 49.6999 13.0053 49.6999 17.6996C49.6999 31.8649 33.7639 46.7342 33.0883 47.36L32.0003 48.3556ZM32.0003 3.2C28.1565 3.20564 24.4718 4.73508 21.7538 7.45305C19.0359 10.171 17.5064 13.8558 17.5008 17.6996C17.5008 28.4444 28.4448 40.3769 32.0003 43.9467C35.5559 40.3911 46.4999 28.4516 46.4999 17.6996C46.4942 13.8558 44.9648 10.171 42.2468 7.45305C39.5289 4.73508 35.8441 3.20564 32.0003 3.2Z"
                    fill="#8C57FC"
                  />
                  <path
                    d="M32.0001 26.6098C30.2378 26.6098 28.5151 26.0872 27.0498 25.1081C25.5845 24.1291 24.4425 22.7375 23.7681 21.1094C23.0937 19.4812 22.9173 17.6897 23.2611 15.9613C23.6049 14.2328 24.4535 12.6452 25.6996 11.3991C26.9457 10.153 28.5334 9.30434 30.2618 8.96054C31.9902 8.61674 33.7817 8.79319 35.4099 9.46758C37.038 10.142 38.4296 11.284 39.4086 12.7493C40.3877 14.2146 40.9103 15.9373 40.9103 17.6996C40.9084 20.0621 39.9691 22.3274 38.2985 23.998C36.6279 25.6685 34.3626 26.6079 32.0001 26.6098ZM32.0001 11.9964C30.8749 11.9964 29.775 12.3301 28.8395 12.9552C27.904 13.5803 27.1748 14.4688 26.7442 15.5083C26.3136 16.5478 26.201 17.6916 26.4205 18.7952C26.64 19.8987 27.1818 20.9124 27.9774 21.708C28.773 22.5036 29.7867 23.0454 30.8902 23.2649C31.9938 23.4844 33.1376 23.3718 34.1771 22.9412C35.2166 22.5106 36.1051 21.7814 36.7302 20.8459C37.3553 19.9104 37.689 18.8105 37.689 17.6853C37.689 16.1765 37.0896 14.7296 36.0227 13.6627C34.9558 12.5958 33.5089 11.9964 32.0001 11.9964Z"
                    fill="#8C57FC"
                  />
                  <path
                    d="M43.8756 40.0213C43.072 41.1022 42.2684 42.112 41.4933 43.0507C46.1369 44.1884 48.5689 45.9876 48.5689 47.2391C48.5689 49.3724 42.112 52.4942 32 52.4942C21.888 52.4942 15.4311 49.3867 15.4311 47.2391C15.4311 45.9876 17.8631 44.1884 22.5422 43.0507C21.7671 42.112 20.9636 41.1022 20.16 40.0213C7.82222 41.8276 0 46.2649 0 51.5911C0 59.648 16.4907 64 32 64C47.5093 64 64 59.648 64 51.5911C64 46.2649 56.1778 41.8276 43.8756 40.0213ZM32 60.8C15.0258 60.8 3.2 55.9502 3.2 51.5556C3.2 49.3653 6.56356 46.6987 12.9422 44.8C12.4824 45.5177 12.2358 46.3512 12.2311 47.2036C12.2311 52.7573 22.1867 55.6587 32 55.6587C41.8133 55.6587 51.7689 52.7573 51.7689 47.2036C51.7642 46.3512 51.5176 45.5177 51.0578 44.8C57.4578 46.6987 60.8 49.3653 60.8 51.5556C60.8 55.9502 48.9742 60.8 32 60.8Z"
                    fill="#8C57FC"
                  />
                </svg>
                <p>
                  14
                  <span>
                    мест посещено
                  </span>
                </p>
              </div>
			  
			<? } ?>
			
			
			<? if ((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('4',$arParams['BLOCKS_FOR_PUBLIC']))) { ?> 
			  
              <div class="footer--el">
                <svg xmlns="http://www.w3.org/2000/svg" width="64" height="66" viewBox="0 0 64 66" fill="none">
                  <path
                    d="M52.5701 0.989753H11.4172C8.3892 0.989753 5.48518 2.19264 3.34403 4.33379C1.20288 6.47493 0 9.37896 0 12.407V29.9388C0 32.9668 1.20288 35.8709 3.34403 38.012C5.48518 40.1532 8.3892 41.3561 11.4172 41.3561H21.9465L9.8442 53.5218C9.31719 54.0453 8.89894 54.6678 8.61353 55.3537C8.32812 56.0395 8.18118 56.775 8.18118 57.5178C8.18118 58.2606 8.32812 58.9961 8.61353 59.6819C8.89894 60.3677 9.31719 60.9903 9.8442 61.5138L12.0896 63.6704C13.1519 64.7255 14.5884 65.3177 16.0856 65.3177C17.5829 65.3177 19.0194 64.7255 20.0817 63.6704L42.4341 41.3561H52.5828C55.6108 41.3561 58.5148 40.1532 60.656 38.012C62.7971 35.8709 64 32.9668 64 29.9388V12.407C64 10.9066 63.7043 9.42091 63.1297 8.03487C62.5551 6.64884 61.713 5.38965 60.6515 4.3293C59.59 3.26894 58.3298 2.42822 56.9432 1.8552C55.5565 1.28217 54.0705 0.988086 52.5701 0.989753ZM46.6838 33.5162L46.3033 33.8841L42.396 29.9642C42.1572 29.7253 41.8332 29.5911 41.4953 29.5911C41.1575 29.5911 40.8335 29.7253 40.5946 29.9642C40.3558 30.2031 40.2216 30.527 40.2216 30.8649C40.2216 31.2027 40.3558 31.5267 40.5946 31.7656L44.5146 35.6855L41.0133 39.1868L21.4644 58.7229L14.817 52.0756L40.0238 26.8815L49.462 24.1033L46.6838 33.5162ZM13.8783 61.9071L11.6963 59.649C11.4071 59.3614 11.1776 59.0194 11.021 58.6428C10.8644 58.2661 10.7838 57.8623 10.7838 57.4544C10.7838 57.0465 10.8644 56.6426 11.021 56.266C11.1776 55.8893 11.4071 55.5474 11.6963 55.2597L12.9649 53.8643L19.625 60.5243L18.2295 61.9071C17.6486 62.4766 16.8674 62.7955 16.0539 62.7955C15.2404 62.7955 14.4593 62.4766 13.8783 61.9071ZM61.4501 29.9388C61.4501 32.294 60.5146 34.5526 58.8492 36.218C57.1839 37.8833 54.9252 38.8189 52.5701 38.8189H44.9586L48.6882 35.0766C48.8403 34.9238 48.9534 34.7367 49.018 34.5311L51.8723 24.801C51.9997 24.3644 52.0073 23.9016 51.8945 23.461C51.7816 23.0204 51.5523 22.6182 51.2307 22.2966C50.9091 21.975 50.5069 21.7457 50.0663 21.6329C49.6257 21.52 49.1629 21.5276 48.7263 21.655L38.9962 24.5093C38.7906 24.5739 38.6035 24.687 38.4507 24.8391L24.4963 38.7935H11.4172C9.0621 38.7935 6.80342 37.8579 5.13808 36.1926C3.47274 34.5273 2.53716 32.2686 2.53716 29.9134V12.407C2.53716 10.0519 3.47274 7.79317 5.13808 6.12783C6.80342 4.46249 9.0621 3.52692 11.4172 3.52692H52.5701C54.9252 3.52692 57.1839 4.46249 58.8492 6.12783C60.5146 7.79317 61.4501 10.0519 61.4501 12.407V29.9388ZM53.6737 11.9122C53.6737 12.2487 53.5401 12.5714 53.3022 12.8093C53.0643 13.0472 52.7416 13.1808 52.4052 13.1808H20.4622C20.1258 13.1808 19.8031 13.0472 19.5652 12.8093C19.3273 12.5714 19.1937 12.2487 19.1937 11.9122C19.1937 11.5758 19.3273 11.2531 19.5652 11.0152C19.8031 10.7773 20.1258 10.6437 20.4622 10.6437H52.4052C52.7416 10.6437 53.0643 10.7773 53.3022 11.0152C53.5401 11.2531 53.6737 11.5758 53.6737 11.9122ZM35.9136 20.5386C35.9136 20.8751 35.7799 21.1977 35.542 21.4356C35.3041 21.6735 34.9814 21.8072 34.645 21.8072H11.5822C11.2457 21.8072 10.923 21.6735 10.6851 21.4356C10.4472 21.1977 10.3136 20.8751 10.3136 20.5386C10.3136 20.2022 10.4472 19.8795 10.6851 19.6416C10.923 19.4037 11.2457 19.27 11.5822 19.27H34.645C34.9814 19.27 35.3041 19.4037 35.542 19.6416C35.7799 19.8795 35.9136 20.2022 35.9136 20.5386ZM28.3021 29.1777C28.3021 29.5141 28.1684 29.8368 27.9305 30.0747C27.6926 30.3126 27.3699 30.4462 27.0335 30.4462H11.5822C11.2457 30.4462 10.923 30.3126 10.6851 30.0747C10.4472 29.8368 10.3136 29.5141 10.3136 29.1777C10.3136 28.8412 10.4472 28.5185 10.6851 28.2806C10.923 28.0427 11.2457 27.9091 11.5822 27.9091H27.0335C27.2012 27.9091 27.3672 27.9423 27.5219 28.0068C27.6767 28.0714 27.8171 28.166 27.935 28.2851C28.053 28.4043 28.1462 28.5456 28.2092 28.701C28.2722 28.8564 28.3038 29.0227 28.3021 29.1903V29.1777Z"
                    fill="#E5A52C"
                  />
                </svg>
                <p>
                  <?=$arComment['ALL_COUNT'];?>
                  <span>
                    комментар<?=NumberWordEndingsEx($arComment['ALL_COUNT'],array('ий','иев','ий','ия'));?> оставлен<?=NumberWordEndingsEx($items['ALL_COUNT'],array('','о','','о'));?>  
                  </span>
                </p>
              </div>
			<? } ?>
			
			<? if ((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('5',$arParams['BLOCKS_FOR_PUBLIC']))) { ?> 
			
              <div class="footer--el">
                <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" fill="none">
                  <path
                    d="M31.5779 49.4979C31.9585 49.8822 32.1736 50.4003 32.177 50.9413C32.1855 51.2167 32.1366 51.4908 32.0334 51.7464C31.9303 52.0019 31.7752 52.2331 31.5779 52.4255C31.3897 52.6145 31.1681 52.7669 30.9243 52.8749C30.6721 52.9716 30.4046 53.0223 30.1345 53.0247C29.8687 53.0237 29.6054 52.9729 29.3583 52.8749C29.1104 52.767 28.8843 52.6147 28.6911 52.4255C28.3086 52.0425 28.0937 51.5234 28.0937 50.9821C28.0937 50.4408 28.3086 49.9217 28.6911 49.5387C28.7892 49.4452 28.8939 49.3587 29.0043 49.28L29.3583 49.0894L29.7396 48.9804C30.0649 48.9092 30.4028 48.9191 30.7234 49.0093C31.044 49.0996 31.3375 49.2674 31.5779 49.4979ZM28.3915 12.7319C26.1904 13.2514 24.1987 14.4263 22.6792 16.1013C21.1597 17.7764 20.184 19.8729 19.8809 22.114C19.8375 22.4752 19.9394 22.8388 20.1642 23.1248C20.3889 23.4108 20.718 23.5958 21.0792 23.6391C21.4403 23.6825 21.8039 23.5806 22.0899 23.3559C22.3759 23.1311 22.5609 22.802 22.6043 22.4408C22.8357 20.7326 23.587 19.1369 24.7563 17.8702C25.9255 16.6035 27.4561 15.7272 29.1404 15.36C31.0361 14.796 33.0553 14.7984 34.9496 15.3669C36.8439 15.9355 38.5308 17.0453 39.8026 18.56C40.7003 19.626 41.3533 20.876 41.7156 22.2217C42.0779 23.5675 42.1407 24.9763 41.8996 26.3489C41.4553 28.1608 40.6434 29.862 39.5144 31.3471C38.3854 32.8322 36.9632 34.0695 35.3362 34.9821C31.0604 37.5557 28.8 40.8102 28.8 44.3779C28.8 44.739 28.9435 45.0854 29.1988 45.3407C29.4542 45.5961 29.8006 45.7396 30.1617 45.7396C30.5229 45.7396 30.8692 45.5961 31.1246 45.3407C31.3799 45.0854 31.5234 44.739 31.5234 44.3779C31.5234 41.777 33.28 39.394 36.7387 37.3106C38.697 36.1997 40.4045 34.6961 41.7542 32.8941C43.1039 31.0922 44.0667 29.0307 44.5821 26.8391C44.8902 25.0908 44.8147 23.2963 44.361 21.58C43.9072 19.8637 43.086 18.2664 41.9541 16.8987C40.3735 14.9217 38.2356 13.465 35.8174 12.7172C33.3992 11.9694 30.8122 11.965 28.3915 12.7047V12.7319ZM64 32C64 38.329 62.1232 44.5159 58.607 49.7782C55.0908 55.0406 50.0931 59.1421 44.2459 61.5641C38.3986 63.9861 31.9645 64.6198 25.7571 63.3851C19.5497 62.1504 13.8479 59.1027 9.3726 54.6274C4.89732 50.1521 1.84961 44.4503 0.614885 38.2429C-0.619842 32.0355 0.013865 25.6014 2.43587 19.7541C4.85787 13.9069 8.95939 8.90918 14.2218 5.39297C19.4841 1.87677 25.671 0 32 0C40.4847 0.00721183 48.6199 3.38095 54.6195 9.38055C60.6191 15.3801 63.9928 23.5153 64 32ZM61.2766 32C61.2766 26.2096 59.5596 20.5493 56.3426 15.7348C53.1257 10.9203 48.5533 7.16783 43.2037 4.95195C37.8541 2.73608 31.9675 2.1563 26.2884 3.28595C20.6093 4.41559 15.3927 7.20391 11.2983 11.2983C7.20393 15.3927 4.4156 20.6093 3.28596 26.2884C2.15632 31.9675 2.73609 37.8541 4.95197 43.2037C7.16784 48.5533 10.9203 53.1256 15.7348 56.3426C20.5493 59.5595 26.2096 61.2766 32 61.2766C39.7624 61.2694 47.2049 58.1826 52.6937 52.6937C58.1826 47.2048 61.2694 39.7624 61.2766 32Z"
                    fill="#5CADFF"
                  />
                </svg>
                <p>
                  <?=$items['ALL_COUNT'];?>
                  <span>
                    вопрос<?=NumberWordEndingsEx($items['ALL_COUNT']);?> задан<?=NumberWordEndingsEx($items['ALL_COUNT'],array('','о','','о'));?> наставнику
                  </span>
                </p>
              </div>
			  
			<? } ?>
			  
            </div>
          </div>
        </section>
		
		<? if (((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && (in_array('7',$arParams['BLOCKS_FOR_PUBLIC'])) && (in_array(1,$arResult["arUser"]['UF_RIGHT']))))&& (in_array('7',$arParams['BLOCKS_SHOW']))) { ?> 
		
		
        <section class="attainment--personal-area">
          <div class="title-pages--personal-area">
            <h2>Достижения</h2>
            <span>12 / 32</span>
          </div>
          <div class="attainment-list">
            <ul>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
              <li>
                <img src="<?=SITE_TEMPLATE_PATH;?>/img/event-image.png" alt="attainment" />
                <div class="attainment-list__text">
                  <p>Название достижения</p>
                  <span>Краткое описание достижения</span>
                </div>
              </li>
            </ul>
            <p class="news__load">
              Показать все достижения
              <i class="material-icons arrow_downward__icon">arrow_downward</i>
            </p>
          </div>
        </section>
		
		<? } ?>
		
		
		<? if ((((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('8',$arParams['BLOCKS_FOR_PUBLIC']))&& (in_array(2,$arResult["arUser"]['UF_RIGHT']))))&& (in_array('8',$arParams['BLOCKS_SHOW']))) { ?> 
		
        <section class="places-visited">
          <div class="title-pages--personal-area">
            <h2>Посещенные места</h2>
            <span>14</span>
          </div>
          <div class="places-visited__list">
            <ul>
              <li>
                <a href="#">Название культурного памятника которое очень длинное в три строки</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника которое очень длинное в три строки</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
            </ul>
            <p class="news__load">
              Показать все посещенные места
              <i class="material-icons arrow_downward__icon">arrow_downward</i>
            </p>
          </div>
        </section>
		
		<? } ?>
		
		<? if ((((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('9',$arParams['BLOCKS_FOR_PUBLIC']))&& (in_array(3,$arResult["arUser"]['UF_RIGHT'])))) && (in_array('9',$arParams['BLOCKS_SHOW']))) { ?> 
		
        <section class="routes--personal-area">
          <div class="title-pages--personal-area">
            <h2>Маршруты</h2>
            <span>2</span>
          </div>
          <div class="routes--list">
            <ul>
              <li>
                <a href="#">
                  <p>Название маршрута</p>
                </a>
              </li>
              <li>
                <a href="#">
                  <p>Название маршрута</p>
                </a>
              </li>
            </ul>
          </div>
        </section>
		
		<? } ?>
		
		<? if ((((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('10',$arParams['BLOCKS_FOR_PUBLIC']))&& (in_array(4,$arResult["arUser"]['UF_RIGHT'])))) && (in_array('10',$arParams['BLOCKS_SHOW']))) { ?> 
		
        <section class="cultural-monuments--personal-area">
          <div class="title-pages--personal-area">
            <h2>Культурные памятники</h2>
            <span>12</span>
          </div>
          <div class="cultural-monuments--list">
            <ul>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
              <li>
                <a href="#">Название культурного памятника</a>
              </li>
            </ul>
            <p class="news__load">
              Показать все культурные памятники
              <i class="material-icons arrow_downward__icon">arrow_downward</i>
            </p>
          </div>
        </section>
		
		<? } ?>
		
<? if ((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('11',$arParams['BLOCKS_FOR_PUBLIC']))) { ?> 		
		
<? if ($afisha['ALL_COUNT']>0) { ?>		
		
		
        <section class="upcoming-events--personal-area">
          <div class="title-pages--personal-area">
            <h2>Ближайшие мероприятия</h2>
            <span><?=$afisha['ALL_COUNT'];?></span>
          </div>
		  
		      <div class="upcoming-events--list">
            <ul>
		  
		  
		  
		  
		  
	<? 
	
$filter=array();
	
	foreach ($afisha['LIST'] as $arItem)
	{
$filter['ID'][]=$arItem['EVENT_ID'];
	}

		?>
		
	<?	
	

    $APPLICATION->IncludeComponent(
            "culture:affiche",
            "list_ajax",
            Array(
                'TYPE'=> 'LIST',
                'FILTER' => $filter,
                'SORT' => $sort,
            )
        );
?>		
		
	

		  
	  
		  
		  
      
             
             
            </ul>
          </div>
        </section>

<? } ?>	
<? } ?>	
		
		


<? if (((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('12',$arParams['BLOCKS_FOR_PUBLIC']))&& (in_array(5,$arResult["arUser"]['UF_RIGHT'])))) { ?> 

<?
 $param = array();
        $param['filter'] = array(
            'USER_ID'=>$arResult["arUser"]['ID'],
			
        );			
$favorites =  Favorites::getFavorites($param);		

$nofav=0;
foreach ($favorites['LIST'] as $arItem)
{
	// Проверка на существование
	
$resNEW = CIBlockElement::GetByID( $arItem['ELEMENT_ID']);
$ar_resNEW = $resNEW->GetNext();
if ($ar_resNEW['ID']>0)$nofav=$ar_resNEW['ID'];
	
}

	if (($favorites['ALL_COUNT']>0) && ($nofav>0)) {
?>	
        <section class="favorites--personal-area">
          <div class="favorites--title">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px">
              <path d="M0 0h24v24H0V0z" fill="none"></path>
              <path
                fill="#5CADFF"
                d="M19 18l2 1V3c0-1.1-.9-2-2-2H8.99C7.89 1 7 1.9 7 3h10c1.1 0 2 .9 2 2v13zM15 5H5c-1.1 0-2 .9-2 2v16l7-3 7 3V7c0-1.1-.9-2-2-2z"
              ></path>
            </svg>
            <h2>Избранное</h2>
          </div>
          <div class="favorites--tab-container">
            <div class="tab-container--list">
			
		<ul>	
		<? 
$i=0;
foreach ($iblocks as $iblock) { ?>
<?

$res = CIBlock::GetByID($iblock);
$ar_res_IBLOCK = $res->GetNext();
$cl='';

if ($i==0) $cl=' class="is-active"';

 $param = array();
        $param['filter'] = array(
            'USER_ID'=>$arResult["arUser"]['ID'],
			'IBLOCK_ID'=>$iblock
        );	 
	 
$favorites =  Favorites::getFavorites($param);
$no3=0;
foreach ($favorites['LIST'] as $arNEW)
{
	
$resNEW = CIBlockElement::GetByID($arNEW['ID']);
$ar_resNEW = $resNEW->GetNext();
if ($arNEW['ID']>0) $no3=$arNEW['ID'];
}

if (($favorites['ALL_COUNT']>0) && ($ar_res_IBLOCK['ID']>0) && ($no3>0)) {
	$i=$i+1;

?>
                <li<?=$cl;?>>
                  <?=$ar_res_IBLOCK['NAME'];?>
                </li>

<? } ?>				                
				
<? } ?>				
				
              </ul>
            </div>
	



		<? 
$i=0;
foreach ($iblocks as $iblock) { ?>
<?
$res = CIBlock::GetByID($iblock);
$ar_res_IBLOCK = $res->GetNext();
$cl='';
if ($i==0) $cl=' is-active';

 $param = array();
        $param['filter'] = array(
            'USER_ID'=>$arResult["arUser"]['ID'],
			'IBLOCK_ID'=>$iblock
        );	 
	 
$favorites =  Favorites::getFavorites($param);

$res = CIBlock::GetByID($iblock);
$ar_res_IBLOCK = $res->GetNext();

if (($favorites['ALL_COUNT']>0)&& ($ar_res_IBLOCK['ID']>0)) {
$i=$i+1;


?>
			
            <div class="tab-container--content">
              <div class="tab--content-el<?=$cl;?>">
			  <div id="post_list" style="width: 100%;">  
			  <?
foreach ($favorites['LIST'] as $arNEW)
{

$arNEWE=$arNEW['ELEMENT_ID'];
$resNEW = CIBlockElement::GetByID($arNEWE);
$ar_resNEW = $resNEW->GetNext();

$img=SITE_TEMPLATE_PATH.'/img/event-image.png';
if ($ar_resNEW['DETAIL_PICTURE']>0) $img=CFIle::getPath($ar_resNEW['DETAIL_PICTURE']);
if ($ar_resNEW['PREVIEW_PICTURE']>0) $img=CFIle::getPath($ar_resNEW['PREVIEW_PICTURE']);

$text='';
if ($ar_resNEW['DETAIL_TEXT']!='') $text=$ar_resNEW['DETAIL_TEXT'];
if ($ar_resNEW['PREVIEW_TEXT']!='') $text=$ar_resNEW['PREVIEW_TEXT'];
		
if (strlen($text)>450)
{
	$text=substr(strip_tags($text),0,450);
    
	$id=strripos($text,' ');
	$text=substr($text,0,$id);
	
	$id=strripos($text,'[');
	if ($id>0) $text=substr($text,0,$id);
	
	
	$text=$text.'...';
}
	if ($ar_resNEW['ID']>0) {
			  ?>
         
				
				
		<section class="post-pages" style="margin-bottom: 60px;">		
				 <div class="post-pages--content">
                  <div class="post-pages-left">
                    <span class="label">
                      <?=$ar_res_IBLOCK['NAME'];?>
                    </span>
                    <h3 class="name-post">
                   <?=$ar_resNEW['NAME'];?>
                    </h3>
                    <div class="show-text">
                      <p><?=$text;?></p>
                    </div>
                    <span><a class="link" href="<?=$ar_resNEW['DETAIL_PAGE_URL'];?>">Читать далее </a><i class="material-icons keyboard_arrow_right__icon">keyboard_arrow_right</i></span>
                  </div>
<div class="post-pages-right">
					<? if ($img!='') { ?>
                    <img src="<?=$img;?>"  alt=" <?=$ar_resNEW['NAME'];?>" />
				  <? } ?>
                  </div>
			
			</section>	
				
				
				
	<? } ?>			
				
				
				
				
<? } ?>				
				
              </div></div>
			  
			  
			  
<? } ?>				                
				
<? } ?>				  
			  
              
			  
             
			 
              
              
            </div>
          </div>
        </section>
		
	<? } ?>		
	
	
<? } ?>

<?

 if ($_REQUEST['action'] == "life") {
	 

  
  
  
  
 if (!CultureHelpers::CheckMate($_REQUEST['message'])) {
	 
	
	


    }	 else {
	 
	 
	 
	 
	 
	 
        $src_user_id = 0;
        if ($USER->IsAuthorized()) $src_user_id = $USER->GetID();
		$add=array('MODULE'=>'users','MODERATED' => 0, 'SRC_USER_ID' => $src_user_id,'DST_USER_ID' => (int)$_REQUEST['dst_user_id'], 'MESSAGE' => $_REQUEST['message']);
		$res = LiveLenta::addMessage($add);
		
	

$_REQUEST['message']='';	
		
		
	}
	
 echo 'Сообщение появится на сайте, сразу после ручной модерации.';	
	
	
    }
	
 if (($_REQUEST['action'] == "edit") && ($_REQUEST['elementedit']>0)) {
	 
// Проверка на легальность запроса
	$param = array();
        $param['filter'] = array(
            'SRC_USER_ID'=>$USER->GetID(),
			'ID'=>$_REQUEST['delete'],
			'MODULE'=>'users',
			'MODERATED'=>0
        );
		
$lifes =  LiveLenta::getMessages($param);	

 if (CultureHelpers::CheckMate($_REQUEST['message'])) {

$paramD = array('MESSAGE' => $_REQUEST['message']);
$lifes =  LiveLenta::updateMessage($_REQUEST['elementedit'],$paramD);	

$_REQUEST['message']='';
 } else {
	// echo 'Материться не хорошо!!!'; 
	$_REQUEST['message']='';
 }
 
 echo 'Сообщение появится на сайте, сразу после ручной модерации.';


 } 
	

		
		
?>

<? if ((HIDEBLOCKS!='Y') || ((HIDEBLOCKS=='Y') && in_array('13',$arParams['BLOCKS_FOR_PUBLIC']))) { ?> 

<section class="personal-wall">
          <h2><?=GetMessage('STENA_TITLE');?></h2>
		  <a name="life-lenta"></name>
          <div class="personal-wall--content">
            <form action="#life-lenta" method="POST">
              <textarea name="message" placeholder="<?=GetMessage('WRITE_MESSAGE');?>"><?=$_REQUEST['message'];?></textarea>

			  <input type='hidden' id="life" value='life' name='action'>
			  <input type='hidden' value='' name='elementedit'id="elementedit">

			  <input type='hidden' value='<?=$arResult["arUser"]['ID'];?>' name='dst_user_id''>
              <button type="submit">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="13" viewBox="0 0 15 13" fill="none">
                  <path
                    d="M0.113082 4.14044C-0.0908484 3.90515 -0.0111819 3.65266 0.289452 3.57689L14.4027 0.0199586C14.7041 -0.0559868 14.8065 0.0915402 14.6325 0.34813L6.46306 12.3937C6.28863 12.6509 6.02471 12.6391 5.87317 12.3667L3.69338 8.44784C3.54202 8.17573 3.25426 7.76469 3.05008 7.52911L0.113082 4.14044ZM3.73936 6.17022C3.84156 6.28815 4.04184 6.33797 4.18795 6.28101L7.32291 5.05886C7.6129 4.94581 7.68245 5.0426 7.47605 5.27757L5.25538 7.80546C5.15229 7.92282 5.12973 8.12767 5.20606 8.2649L6.12871 9.92365C6.20457 10.06 6.33678 10.0663 6.42289 9.93935L11.5451 2.38686C11.6317 2.25916 11.5797 2.18642 11.431 2.22391L2.58206 4.45408C2.43244 4.49179 2.39336 4.61723 2.4962 4.73589L3.73936 6.17022Z"
                    fill="#101010"
                  />
                </svg>
              </button>
            </form>
            <div class="comment-container">
			
			
			

			
<?


$param = array();
        $param['filter'] = array(
            'DST_USER_ID'=>$arResult["arUser"]['ID'],
			"MODULE"=>'users'
        );

$orderBy=array();		
$orderBy['CREATED'] = 'DESC';		
$param['order']=$orderBy;

 $life =  LiveLenta::getMessages($param);		


$CO=10;
$i=0;
$po=0;
foreach ($life['LIST'] as $arItem) {
	$i=$i+1;
$rsUser = CUser::GetByID( $arItem['SRC_USER_ID']);
$arUser = $rsUser->Fetch();

$img='/img/svg/account-default.svg';
if ($arUser["PERSONAL_PHOTO"]>0) $img=CFile::GetPath($arUser["PERSONAL_PHOTO"]);

if (($arItem[MODERATED]==1) || (($arItem[MODERATED]==0) && ($arUser['ID']==$USER->GetID())))
{
	
?>



 

              <div class="comment-item" data-id="<?=$arItem['ID'];?>">
                <div class="comment-item--header">
                  <div class="avatar">
                  <a href="/personal/<?=$arUser['ID'];?>/"> <img src="<?=$img;?>" alt="<?=$arUser['NAME'];?>" /> </a> 
                  </div>
				  
				  
				  
                  <span><a href="/personal/<?=$arUser['ID'];?>/" class="link"><?=$arUser['LOGIN'];?></a></span>

<div class="menu-control">


  <? if (($arUser['ID']!=$USER->GetID()) && ($arItem[MODERATED]==1)) { ?>

                    <i class="material-icons complaint claim" type-data="custom.live" data-id="<?=$arItem['ID'];?>">
                      error
                    </i>
					
  <? } ?>	

<? if (($arUser['ID']==$USER->GetID())) { ?>  
					
                    <i class="close2 material-icons delete2" OnClick="$(this).closest('.comment-item').addClass('is-active');
					$('.deletepop').find('.delete').attr('data-id','<?=$arItem['ID'];?>');
        $('.popup-delete--comment').closest('.popup-bg').addClass('is-active'); $('body').addClass('no-scroll');"  data-id="<?=$arItem['ID'];?>">
                      close
                    </i>
					
<? } ?>					
					
</div>


				  <? if (($arUser['ID']==$USER->GetID()) && ($arItem[MODERATED]==0) && (1>2)) echo ' <span class="edit" data-id="'.$arItem['ID'].'">('.GetMessage('EDIT').')</span>';?>
  
			 
				  
				  
                </div>
                <p><?=$arItem['MESSAGE'];?></p>
              </div>
              
			  
<?

if ($i>=$CO) {
	$po=$arItem['ID'];
	break;
}


 }	
 }		   
			  
	?>		  
             
			  
              
            </div>
			
			<? if ($po>0) { ?>
			
            <p class="news__load loadlife" data-id="<?=$po;?>" all="<?=$life['ALL_COUNT'];?>" user-id="<?=$arResult["arUser"]['ID'];?>">
             <?=GetMessage('DOWNLOAD');?>
              <i class="material-icons arrow_downward__icon">arrow_downward</i>
            </p>
			
			<? } ?>
			
          </div>
        </section>	
	
<? } ?>
		
      </div>
    </main>
	
	 <style>
	 

	 
   a.link { 
      color: #5cadff;
    font: 14px/1 OpenSans-Regular;
    align-items: center;
    cursor: pointer;
    margin-top: auto;
    text-decoration: none; /* Отменяем подчеркивание у ссылки */
   } 
   
     a.link:hover{ 
      color: #5cadff;
    font: 14px/1 OpenSans-Regular;
    align-items: center;
    cursor: pointer;

    margin-top: auto;
    text-decoration: none; /* Отменяем подчеркивание у ссылки */
   } 
   
   span.edit
   {
	         color: #fff;
    font: 14px/1 OpenSans-Regular;
    align-items: center;
	   cursor:pointer;
   }
   
   span.delete
   {
	         color: #fff;
    font: 14px/1 OpenSans-Regular;
    align-items: center;
	   cursor:pointer;
   }
   
    span.claim
   {
	         color: #fff;
    font: 14px/1 OpenSans-Regular;
    align-items: center;
	   cursor:pointer;
   }

   
   .main.personal-area .upcoming-events--personal-area .upcoming-events--list ul li {
    width: calc(50% - 55px);
    position: relative;
    margin-bottom: 64px;
    max-width: 286px;
    margin-right: 55px;
}


  </style>
  
