$(document).ready(function() {
	
	$("#register_btn").on("click", function() {
		let UF_INTERESTS = [];
		$('.interest__item.active').each(function(){
			UF_INTERESTS.push(parseInt($(this).attr('value'), 10));
        });
		
		let data = {};
		$('._input_req').each(function(){
			data[$(this).attr('name')] = $(this).val();
        });
		data['UF_INTERESTS'] = UF_INTERESTS;

        if (!data['LOGIN']) {
            alert("заполните LOGIN");
            return;
        }
        if (data['LOGIN'].length < 6) {
			alert("LOGIN должен быть не менее 6 имволов");
			return;
		}
        if (!data['EMAIL']) {
            alert("заполните Email");
            return;
        }
		if (!data['EMAIL'].match(/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,}$/g)) {
			 alert("Email некорректен");
			 return;
		}
        if (!data['PASSWORD']) {
            alert("заполните пароль");
            return;
        }
        if (data['PASSWORD'].length < 6) {
            alert("Пароль должен быть не менее 6 имволов");
            return;
        }
        if (!data['LAST_NAME'] || !data['SECOND_NAME'] || !data['NAME']) {
            alert("заполните поля ФИО");
            return;
        }
        if (!data['PERSONAL_BIRTHDAY']) {
            alert("заполните дату рождения");
            return;
        }
        if (!data['PERSONAL_STATE']) {
            alert("укажите регион");
            return;
        }
        if (!data['PERSONAL_CITY']) {
            alert("укажите населённый пункт");
            return;
        }
        if (!data['UF_SCHOOL']) {
            alert("укажите школу");
            return;
        }
        if (!data['UF_CLS']) {
            alert("укажите класс");
            return;
        }
        if (!$('#consentProcessing').is(':checked')) {
            alert("укажите согласие на обработку персональных данных");
            return;
		}
		
		$.ajax({
		    type: 'POST',
		    url: '/registration/ajax.php',
			dataType: "JSON",
            data: data,		
		    success: function(data) {
				console.log(data);
				if (data.hasOwnProperty('status') && (data.status == 'success')) {
					if (data.hasOwnProperty('redirect') && (data.redirect != '')) location.href = data.redirect;
					else {
						if (data.hasOwnProperty('message') && (data.message != '')) alert(data.message);
					}
				}
				else {
					if (data.hasOwnProperty('message') && (data.message != '')) alert(data.message);
					else alert('Ошибка запроса');
				}
		    },
		    error:  function(xhr, str){
			    alert('Ошибка запроса2');
		    }
	    });
		
		
    });

    $('#signup-region').change(function() {

        var value = $(this).find('option:selected').data('id');

        $.ajax({
            type: "POST",
            cache: false,
            url: '/local/templates/books/ajax/city_by_region.php',
            data: 'region=' + value,
            dataType: 'JSON',
            success: function (data) {

                $('#signup-town').empty().trigger("change");

                var newOption = new Option('Все города', '', false, false);
                $('#signup-town').append(newOption).trigger('change');

                $.each(data, function(i, v) {
                    var newOption = new Option(v, v, false, false);
                    $('#signup-town').append(newOption).trigger('change');

                });
            }
        });

        return false;

    });

});