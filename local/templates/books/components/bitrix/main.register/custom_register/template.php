<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$regFields = array();
if (!empty($_REQUEST['REGISTER'])) {
	$regFields = $_REQUEST['REGISTER'];
}
?>
<main class="main signup-pages">
    <div class="signup__wrapper">
        <h1 class="signup__title">Регистрация</h1>

        <div class="signup__tab  tab-1">
            <p class="tab__step">Шаг 1 из 3</p>
            <form class="tab__form" id="req_step_1" method="POST">
                <div class="form__input">
                    <label for="signup-login">Логин</label>
                    <input
                            id="signup-login"
                            class="input__login _input_req"
                            name="LOGIN"
                            type="text"
                            minlength="6"
                            placeholder="Логин"
                            required
                    />
                </div>
                <p class="form__note">- минимум 6 символов <br />- только латиница</p>
                <div class="form__input">
                    <label for="signup-email">Почта</label>
                    <input
                            id="signup-email"
                            class="input__email _input_req"
                            name="EMAIL"
                            type="email"
                            placeholder="example@mail.ru"
                            required
                    />
                </div>
                <div class="form__input">
                    <label for="signup-password">Пароль</label>
                    <input
                            id="signup-password"
                            class="input__password _input_req"
                            name="PASSWORD"
                            type="text"
                            placeholder="Пароль"
                            required
                    />
                </div>
                <div class="form__input">
                    <input
                            id="signup-password-confirmation"
                            class="input__password-confirmation _input_req"
                            name="CONFIRM_PASSWORD"
                            type="text"
                            placeholder="Повторите пароль"
                            required
                    />
                </div>
                <div class="form__input">
                    <span class="explanation-filling">* — поля, обязательные для заполнения</span>
                </div>
            </form>
            <div class="tab__navigation">
                <button class="tab__button signup__button signup__next">
                    Далее <i class="material-icons keyboard_arrow_right__icon">keyboard_arrow_right</i>
                </button>
            </div>
        </div>

        <div class="signup__tab tab-2 hidden ">
            <p class="tab__step">Шаг 2 из 3</p>
            <form class="tab__form" id="req_step_2" method="POST">
                <div class="form__input">
                    <label for="signup-surname">ФИО</label>
                    <input
                            id="signup-surname"
                            class="input__surname _input_req"
                            name="LAST_NAME"
                            type="text"
                            placeholder="Фамилия"
                            required
                    />
                </div>
                <div class="form__input">
                    <input id="signup-name" class="input__name _input_req" name="NAME" type="text" placeholder="Имя" required />
                </div>
                <div class="form__input">
                    <input
                            id="signup-patronymic"
                            class="input__patronymic _input_req"
                            name="SECOND_NAME"
                            type="text"
                            placeholder="Отчество"
                            required
                    />
                </div>
                <div class="form__input">
                    <label for="signup-birthday">Дата рождения</label>
                    <input
                            id="signup-birthday"
                            class="input__birthday _input_req"
                            name="PERSONAL_BIRTHDAY"
                            type="text"
                            maxlength="10"
                            placeholder="__.__.____"
                            required
                    />
                </div>
                <div class="form__input">
                    <label for="signup-region">Регион</label>
                    <select id="signup-region" class="input__region no-plugin _input_req" name="PERSONAL_STATE" type="text" required>
                        <option value="" disabled selected>Регион</option>
                        <?$regionList = \Custom\Culture\CultureHelpers::getRegions();?>
                        <?foreach($regionList as $region):?>
                            <option data-id="<?=$region['REGION_ID']?>" value="<?=$region['NAME']?>"><?=$region['NAME']?></option>
                        <?endforeach;?>
                    </select>
                    <i class="material-icons keyboard_arrow_down__icon">keyboard_arrow_down</i>
                </div>
                <div class="form__input">
                    <label for="signup-town">Населённый пункт</label>
                    <select id="signup-town" class="input__town no-plugin _input_req" name="PERSONAL_CITY" type="text" required>
                        <option value="" selected>Все города</option>
                        <?$sityList = \Custom\Culture\CultureHelpers::getCities();?>
                        <?foreach($sityList as $city):?>
                            <option value="<?=$city['NAME']?>"><?=$city['NAME']?></option>
                        <?endforeach;?>
                    </select>
                    <i class="material-icons keyboard_arrow_down__icon">keyboard_arrow_down</i>
                </div>
                <div class="form__input">
                    <label for="signup-school">Школа</label>
                    <select id="signup-school" class="input__school no-plugin _input_req" name="UF_SCHOOL" type="text" required>
                        <option value="" disabled selected>Школа</option>
                        <option value="Лицей №2">Лицей №2</option>
                        <option value="Школа №15">Школа №15</option>
                        <option value="Частная школа 'Ромашка'">Частная школа "Ромашка"</option>
                        <option value="Лицей №1">Лицей №1</option>
                    </select>
                    <i class="material-icons keyboard_arrow_down__icon">keyboard_arrow_down</i>
                </div>
                <div class="form__input">
                    <label for="signup-class">Класс</label>
                    <select id="signup-class" class="input__class no-plugin _input_req" name="UF_CLS" type="text" required>
                        <option value="" disabled selected>Класс</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                    </select>
                    <i class="material-icons keyboard_arrow_down__icon">keyboard_arrow_down</i>
                </div>
                <div class="form__input">
                    <div class="input-consent--processing">
                        <input type="checkbox" id="consentProcessing" />
                        <label for="consentProcessing"
                        >Я согласен на <a class="explanation-filling" href="/consent_processing" target="_blank">обработку персональных данных</a>
                        </label>
                        <span class="explanation-filling">* — поля, обязательные для заполнения</span>
                    </div>
                </div>
                <div id="interest_hidden_fields"></div>
            </form>
            <div class="tab__navigation">
                <button class="tab__button signup__button signup__back">
                    <i class="material-icons keyboard_arrow_left__icon">keyboard_arrow_left</i>Назад
                </button>
                <button class="tab__button signup__button signup__next">
                    Далее <i class="material-icons keyboard_arrow_right__icon">keyboard_arrow_right</i>
                </button>
            </div>
        </div>

        <div class="signup__tab tab-3 hidden">
            <p class="tab__step">Шаг 3 из 3</p>
            <p class="tab__title">Интересы</p>
            <ul class="tab__interests">
                <?$iterests = Custom\Culture\CultureHelpers::getCultureCategoryList();?>
                <?foreach ($iterests as $item):?>
                    <li class="interest__item" value="<?=$item['ID']?>"><?=$item['NAME']?></li>
                <?endforeach;?>
            </ul>
            <div class="tab__navigation">
                <button class="tab__button signup__button signup__back">
                    <i class="material-icons keyboard_arrow_left__icon">keyboard_arrow_left</i>Назад
                </button>
                <button id="register_btn" class="tab__button signup__button">
                    Завершить <i class="material-icons keyboard_arrow_right__icon">keyboard_arrow_right</i>
                </button>
            </div>
        </div>
    </div>
</main>