<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="news-list">


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
				<li class="news-list__item news-list__item--purple">
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="news-list__image">
					<span class="news-list__date">25 октября 2020</span>
					<p class="news-list__description">
						<?=$arItem["NAME"]?>
					</p>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>/" class="news-list__link news-list__link--purple" target="_blank">
						Читать далее
						<i class="material-icons keyboard_arrow_right__icon">keyboard_arrow_right</i>
					</a>
				</li>			
	<?endforeach;?>			
</ul>