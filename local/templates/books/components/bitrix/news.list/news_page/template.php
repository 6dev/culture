<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>/" class="news__item">
        <div class="item__cover">
            <img class="item__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" />
            <p class="item__category purple">Театр</p>
        </div>
        <div class="item__description item__description--opened">
            <h3 class="item__title"><?=$arItem["NAME"]?></h3>
            <p class="item__text">
                <?=$arItem["PREVIEW_TEXT"];?>
            </p>
            <p class="item__data"><?=$arItem['ACTIVE_FROM']?></p>
        </div>
    </a>
	<?endforeach;?>