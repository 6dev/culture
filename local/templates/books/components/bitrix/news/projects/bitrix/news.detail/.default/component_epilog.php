<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
Custom\Culture\CultureHelpers::setPageTitle($arResult['NAME']);
$APPLICATION->SetPageProperty("og:type", "article");

$APPLICATION->SetPageProperty("og:url", $arResult['PAGE_URL']);
$APPLICATION->SetPageProperty("og:title", $arResult['NAME']);
$APPLICATION->SetPageProperty("og:description", $arResult['PREVIEW_TEXT']);
$APPLICATION->SetPageProperty("og:image", SITE_BASE_URL.$arResult['IMAGE_SRC']);
?>