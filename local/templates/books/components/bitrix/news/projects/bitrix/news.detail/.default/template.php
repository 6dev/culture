<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<main class="main news-page">
    <div class="container">
        <div class="news-page--content">
            <div class="header--news--content">
			<? global $USER; if ($USER->GetID()>0) {
            $this->addExternalJS(SITE_TEMPLATE_PATH."/js/favorite.js");
				?>
          <span class="bookmark--news" data-id="<?=$arResult['ID'];?>" data-iblockid="<?=$arResult['IBLOCK_ID'];?>">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px">
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path
                        fill="#BDBDBD"
                        d="M19 18l2 1V3c0-1.1-.9-2-2-2H8.99C7.89 1 7 1.9 7 3h10c1.1 0 2 .9 2 2v13zM15 5H5c-1.1 0-2 .9-2 2v16l7-3 7 3V7c0-1.1-.9-2-2-2z"
                />
              </svg>
            </span>
			<? } ?>
                <span class="data--news">
              <?=FormatDate("d F Y", MakeTimeStamp($arResult["ACTIVE_FROM"]))?>
            </span>
            </div>
            <h1><?=$arResult["NAME"];?></h1>

            <div class="block-content">
                <p>
                    <?=$arResult["PREVIEW_TEXT"];?>
                </p>
                <hr />
                <div class="img-box">
                    <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="event-image" />
                    <span><?=$arResult["DETAIL_PICTURE"]['DESCRIPTION']?></span>
                </div>

                <?=$arResult["DETAIL_TEXT"];?>


            </div>

            <div class="footer--news--content">
                <div class="user--box">
                    <i class="material-icons">
                        person
                    </i>
                    <p>
                        <?=$arResult['PROPERTIES']['AUTHOR_FIO']['VALUE']?>
                        <?if (!empty($arResult['PROPERTIES']['CITY']['VALUE'])):?>
                        , г. <?=$arResult['PROPERTIES']['CITY']['VALUE']?>
                        <?endif;?>
                    </p>
                </div>




                <div class="share">
                    <p><i class="material-icons">reply</i>Поделиться</p>
                    <div class="social-links">



                        <?$APPLICATION->IncludeComponent(
                            "culture:share",
                            "buttons",
                            Array(
                                'PAGE_URL' => $arResult['PAGE_URL']
                            )
                        );?>



                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
