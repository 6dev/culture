$( document ).ready(function() {

    const limitNews = 7;

    function newsRequest() {
        this.state = {send: 0, clearHtml: 0, page_count: 1};
        this.param = {
            data: {
                action: 'news_list',
                offset: 1,
                limit: limitNews
            }
        };
        this.callback_success;
        this.callback_error;
        this.request = function (param, state) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: 'POST',
                    url: '/projects/ajax.php',
                    dataType: "json",
                    data: param.data,
                    success: (function (response) {
                        //resolve(response);
                        if (response.hasOwnProperty('data') && response.data.hasOwnProperty('list_html')) {
                            if (response.data.hasOwnProperty('page_count')) {
                                state.page_count = parseInt(response.data.page_count, 10);
                            }
                            let h = $("#news_list").html();
                            if (state.clearHtml == 1) h = response.data.list_html;
                            else h += response.data.list_html;
                            $(".listprojects").html(h);
                            //param.data.offset += limitNews;
                            if (param.data.offset == state.page_count)  {
                                $("#news__load_btn").hide();
                            }
                            param.data.offset++;
                        }
                        state.clearHtml = 0;
                        state.send = 0;
                    }),
                    error: (function (error) {
                        state.send = 0;
                        state.clearHtml = 0;
                        reject(error);
                    })
                });
            });
        };
        this.getList = function (param) {
            if (this.state.send == 0) {
                this.state.send = 1;
                if (param.hasOwnProperty('offset')) this.param.data.offset = param.offset;
                if (param.hasOwnProperty('clearHtml')) this.state.clearHtml = param.clearHtml;
                this.request(this.param, this.state)
                    .then(this.callback_success,this.callback_error);
            }
            else console.log('send proccess');
        };
    };

    let news = new newsRequest();

    news.callback_error = function (e) {
        console.log("err");
        console.log(e);
    };

    $("body").on("click", "#news__load_btn", function() {
        news.getList({});
    });


    news.getList({offset: 1, clearHtml: 1});

});