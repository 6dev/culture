<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;

$cp = $this->__component; // объект компонента

if (is_object($cp))
{
    $cp->arResult['USER'] = array();
    if (!empty($cp->arResult['PROPERTIES']['UID']['VALUE'])) {
        $cp->arResult['USER'] = \Custom\Culture\CultureHelpers::getUser($cp->arResult['PROPERTIES']['UID']['VALUE']);
    }
    $cp->arResult['IMAGE_SRC'] = (!empty($cp->arResult['PREVIEW_PICTURE']))? $cp->arResult['PREVIEW_PICTURE']['SRC'] : $cp->arResult['DETAIL_PICTURE']['SRC'];
    $cp->arResult['PAGE_URL'] = SITE_BASE_URL.explode('?', $_SERVER[REQUEST_URI])[0];
    $cp->SetResultCacheKeys(array('PREVIEW_TEXT','IMAGE_SRC', 'PAGE_URL'));
    $arResult['PREVIEW_TEXT'] = $cp->arResult['PREVIEW_TEXT'];
    $arResult['IMAGE_SRC'] = $cp->arResult['IMAGE_SRC'];
    $arResult['PAGE_URL'] = $cp->arResult['PAGE_URL'];
}
?>