<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<main class="main news-pages">
    <div class="news__wrapper">
        <h1 class="news__title">Новости</h1>
        <div class="container">
            <div id="news_list" class="news__list">



            </div>
            <p id="news__load_btn" class="news__load">
                Загрузить ещё
                <i class="material-icons arrow_downward__icon">arrow_downward</i>
            </p>
        </div>
    </div>
</main>