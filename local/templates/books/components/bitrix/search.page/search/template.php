<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Custom\Afficheorders\Orders;

?>

<?
$arMassIblocks=array('iblock_news','iblock_articles','iblock_culture_types');

$arIb["arrFILTER_iblock_news"]= array(
			0 => "15",
	);


$arIb["arrFILTER_iblock_articles"]= array(
			0 => "2",
			1 => "25",
	);
	
$arIb["arrFILTER_iblock_culture_types"]= array(
			1 => "21",
	);

$arrFilterSearchS=array();
	
foreach ($arMassIblocks as $k=>$aIblocks) {
	
	foreach ($arIb["arrFILTER_".$aIblocks] as $kk=>$arFid) {
		
$APPLICATION->IncludeComponent("bitrix:search.page", "json", array(
	"RESTART" => "N",
	"CHECK_DATES" => "N",
	"USE_TITLE_RANK" => "N",
	"DEFAULT_SORT" => "rank",
	"arrFILTER" => $aIblocks,
	"arrFILTER_".$aIblocks=>$arFid,
	"SHOW_WHERE" => "N",
	"SHOW_WHEN" => "N",
		"NAME_JSON"=>'search_json'.$k,
	"PAGE_RESULT_COUNT" => "100",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_SHADOW" => "Y",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "36000000",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Результаты поиска",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "arrows",
	"USE_SUGGEST" => "N",
	"SHOW_ITEM_TAGS" => "N",
	"SHOW_ITEM_DATE_CHANGE" => "N",
	"SHOW_ORDER_BY" => "N",
	"SHOW_TAGS_CLOUD" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);

$arResult=json_decode($APPLICATION ->GetViewContent('search_json'.$k),true);


$ids=array();
foreach ($arResult['SEARCH'] as $arItem)
{
$ids[]=$arItem['ITEM_ID'];
// $arrFilterSearch[]['ID']=$arItem['ITEM_ID'];
}

$arrFilterSearchS[$arFid]=$ids;





}}


?>

 <main class="main search-result">
      <section class="top-search">
        <div class="container">
          <form>
            <input type="search" name="q" value="<?=$_REQUEST['q'];?>" placeholder="Текст запроса" />
            <button type="submit">Найти</button>
          </form>
        </div>
      </section>
	  
	  
	<? if (count($arrFilterSearchS[15])>0) { ?>  
	  
      <section class="search-result--news">
        <div class="container">
          <h2>Новости</h2>

          <div class="search-result--news__container ajaxresult15">
		  
	<?

$filterN['ID']=$arrFilterSearchS[15];
	
	 $APPLICATION->IncludeComponent(
            "culture:news",
            "list",
            Array(
                'TYPE' => 'LIST',
                'FILTER' => $filterN,
                'PAGE' => (int)$_REQUEST['offset'],
                'SORT' => $sort,
                'nPageSize' =>3,
            )
        );	  
		  
		?>  
		  
		  
		  
		  
		  
		  
            
         
          </div>
          <p class="news__load" iblockid="15" page="1" pages="<?=ceil(count($arrFilterSearchS[15])/3);?>" data='<?=json_encode($arrFilterSearchS[15]);?>'>
            Показать еще
            <i class="material-icons arrow_downward__icon">arrow_downward</i>
          </p>
        </div>
      </section>
	  
	  
	<? } ?>	  
	  
	  
	 <? if (count($arrFilterSearchS[2])>0)  { ?>
	  
      <section class="search-result--post">
        <div class="container">
          <h2>Публикации</h2>
<div class="search-result--post__container ajaxresult2">	
<?	

$filterPost['ID']=$arrFilterSearchS[2];


           $APPLICATION->IncludeComponent(
            "culture:posts",
            "list_result_search",
            Array(
                'TYPE'=> 'LIST',
                'FILTER' => $filterPost,
                'SORT' => $sort,
                'nPageSize' => 1,
            )
        );
		  
	?>	  
	
	
	
	
		  
        </div>
		
		
		
		
		 </div>
		 
<section class="post-video">
	 	   <p class="news__load" iblockid="2" page="1" pages="<?=ceil(count($arrFilterSearchS[2])/1);?>" data='<?=json_encode($arrFilterSearchS[2]);?>'>
            Загрузить ещё
            <i class="material-icons arrow_downward__icon">arrow_downward</i>
          </p>
		 

    </section>
		 
      </section>

<? } ?>


	 <? if (count($arrFilterSearchS[25])>0)  { ?>

      <section class="post-video">
        <div class="container ajax ajaxresult2">
		
          
		  
	<?	

$filterPost['ID']=$arrFilterSearchS[25];


           $APPLICATION->IncludeComponent(
            "culture:posts",
            "list_result_search",
            Array(
                'TYPE'=> 'LIST',
                'FILTER' => $filterPost,
                'SORT' => $sort,
                'nPageSize' => 1,
            )
        );
		  
	?>		  
		  
		  
		  



	  
    </div>
	
	   <p class="news__load" iblockid="25" page="1" pages="<?=ceil(count($arrFilterSearchS[25])/3);?>" data='<?=json_encode($arrFilterSearchS[25]);?>'>
            Загрузить ещё
            <i class="material-icons arrow_downward__icon">arrow_downward</i>
          </p>
	
	
	
      </section>
	  
	  	 <? } ?> 
	  
	  
<?		  
 $param = array();
        $param['filter'] = array(
            'EVENT_ID'=>$arrFilterSearchS[21]
        );
     
$afisha =  Orders::getList($param);	 


$filterS=array("ID"=>$arrFilterSearchS[21]);

?>	  

<? if ($afisha['ALL_COUNT']>0) { ?>		  
	  
      <section class="post-container">
        <div class="container">
          <h2>Мероприятия</h2>
          <ul class="ajaxresult21">

	<?	
	

    $APPLICATION->IncludeComponent(
            "culture:affiche",
            "list_ajax",
            Array(
                'TYPE'=> 'LIST',
                'FILTER' => $filterS,
                'SORT' => $sort,
				'nPageSize' => 3,
				
				));
?>			  
 
        
		
          </ul>
          <p class="news__load" iblockid="21" page="1" pages="<?=ceil(count($arrFilterSearchS[21])/3);?>" data='<?=json_encode($arrFilterSearchS[21]);?>'>
            Загрузить ещё
            <i class="material-icons arrow_downward__icon">arrow_downward</i>
          </p>
        </div>
      </section>
	  
<? } ?>	 



<?

if ((count($arrFilterSearchS[21])<=0) && (count($arrFilterSearchS[25])<=0) && (count($arrFilterSearchS[2])<=0) && (count($arrFilterSearchS[15])<=0)){ ?>




<section class="nothing-found">
        <div class="container">
          <div class="nothing-found__content">
            <div class="img-box">
              <svg xmlns="http://www.w3.org/2000/svg" width="53" height="53" viewBox="0 0 53 53" fill="none">
                <path
                  d="M42.049 21.5619C42.049 32.9196 32.8582 42.1239 21.5245 42.1239C10.1908 42.1239 1 32.9196 1 21.5619C1 10.2043 10.1908 1 21.5245 1C32.8582 1 42.049 10.2043 42.049 21.5619Z"
                  stroke="#8C57FC"
                  stroke-width="2"
                  stroke-miterlimit="10"
                />
                <path d="M52 52.0002L36.1111 36.1113" stroke="#8C57FC" stroke-width="2" stroke-miterlimit="10" />
                <path
                  d="M26.7222 17.6297L25.7039 16.6113L21.6666 20.6486L17.6294 16.6113L16.6111 17.6297L20.6483 21.6669L16.6111 25.7041L17.6294 26.7224L21.6666 22.6852L25.7039 26.7224L26.7222 25.7041L22.685 21.6669L26.7222 17.6297Z"
                  fill="#F1795C"
                />
              </svg>
            </div>
            <p>К сожалению, по Вашему запросу ничего не найдено</p>
          </div>
        </div>
      </section> 
	  
<? } ?>	  
	  
    </main>