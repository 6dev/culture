<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
$arAuthServices = $arPost = array();
if(is_array($arParams["~AUTH_SERVICES"]))
{
	$arAuthServices = $arParams["~AUTH_SERVICES"];
}
if(is_array($arParams["~POST"]))
{
	$arPost = $arParams["~POST"];
}
?>
<ul class="login__socials">
<?foreach($arAuthServices as $service):?>
	<?if(($arParams["~FOR_SPLIT"] != 'Y') || (!is_array($service["FORM_HTML"]))):?>
	<li class="socials__item">
		<a class="socials__link" href="javascript:void(0)" onclick="<?=$service["ONCLICK"]?>">
		<?if ($service['ID'] == 'VKontakte'):?><img class="socials__image" src="/img/svg/auth-vk.svg"><?endif;?>
		<?if ($service['ID'] == 'Facebook'):?>
            <img class="socials__image" src="/img/svg/auth-fb.svg">
        <?endif;?>
         <?if ($service['ID'] == 'GoogleOAuth'):?>
             <img class="socials__image" src="/img/svg/auth-google.svg">
         <?endif;?>
         <?if ($service['ID'] == 'YandexOAuth'):?>
                <img class="socials__image" src="/img/svg/auth-yandex.svg">
         <?endif;?>
         <?if ($service['ID'] == 'MailRu2'):?>
                <img class="socials__image" src="/img/svg/auth-mail.svg">
         <?endif;?>
         </a>
	</li>
	<?endif;?>
<?endforeach?>

</ul>