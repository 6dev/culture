$(document).ready(function() {

	$("#login_form").on("submit", function() {
		$.ajax({
		    type: 'POST',
		    url: '/login/ajax.php',
		    dataType: "json",
		    data: $(this).serialize(),
		    success: function(data) {
				if (data.hasOwnProperty('status') && data.hasOwnProperty('redirect') && (data.redirect != '')) {
					location.href = data.redirect;
				}
				else {
					if (data.hasOwnProperty('message')) alert(data.message);
					else alert('Ошибка запроса');
				}
		    },
		    error:  function(xhr, str){
			    alert('Ошибка запроса');
		    }
	    });
	    return false;
    });

});