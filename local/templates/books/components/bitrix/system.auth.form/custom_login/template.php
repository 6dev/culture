<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
	<main class="main">
		<div class="login__wrapper">
			<h1 class="login__title">Вход в систему</h1>
			<form class="login__form" id="login_form">
				<label>
					<img class="input__image image-login" src="/img/svg/auth-login.svg">
				    <input class="input__login" name="LOGIN" type="text" placeholder="Логин" required>
				</label>
				<label>
					<img class="input__image image-password" src="/img/svg/auth-password.svg">
				    <input class="input__password" name="PASSWORD" type="text" placeholder="Пароль" required>
				</label>
				<a class="form__link" href="/forgot_password">Забыли логин или пароль?</a>
				<button class="form__button" type="submit">Войти</button>
			</form>
			<a class="login__link" href="/registration">Регистрация</a>
			
			
			
			
			
			
			<p class="login__other">или войдите через</p>
            <?if($arResult["AUTH_SERVICES"]):?>
            <?$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "custom", 
                array(
                    "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                    "CURRENT_SERVICE"=>$arResult["CURRENT_SERVICE"],
                    "AUTH_URL"=>$arResult["AUTH_URL"],
                    "POST"=>$arResult["POST"],
                ), 
                $component, 
                array("HIDE_ICONS"=>"Y")
            );?>
            <?endif?> 
		</div>
	</main>