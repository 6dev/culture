<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
$contactList = Custom\Culture\CultureHelpers::getContacts();
?>
<footer class="footer">
    <div class="footer-overflow">
        <ul class="footer__content">
            <li class="footer__item">
                <a href="/">
                    <img class="footer__logo" src="/img/svg/footer-logo.svg" />
                </a>
                <?if (!empty($contactList['ADDRESS']) && !empty($contactList['ADDRESS']['VALUE'])):?>
                    <span class="footer__info">
                    <i class="material-icons place__icon">place</i> <?=$contactList['ADDRESS']['VALUE']?>
                </span>
                <?endif;?>
                <?if (!empty($contactList['PHONE']) && !empty($contactList['PHONE']['VALUE'])):?>
                    <a href="tel:+<?=str_replace(['(', ')', ' ', '+', '-'], '', $contactList['PHONE']['VALUE'])?>" class="footer__info">
                        <i class="material-icons phone__icon">phone</i><?=$contactList['PHONE']['VALUE']?>
                    </a>
                <?endif;?>
                <?if (!empty($contactList['EMAIL']) && !empty($contactList['EMAIL']['VALUE'])):?>
                    <a href="mailto:<?=$contactList['EMAIL']['VALUE']?>" class="footer__info">
                        <i class="material-icons email__icon">email</i><?=$contactList['EMAIL']['VALUE']?>
                    </a>
                <?endif;?>

                <span class="footer__support">
              <a href="#">Написать в поддержку</a>
            </span>
            </li>

            <li class="footer__item">
                <a href="/news" class="footer__link">Новости</a>
                <a href="/html_files/my_russia.html" class="footer__link">Моя Россия</a>
                <a href="/cultural-education" class="footer__link">Культурное образование</a>
                <a href="/affiche" class="footer__link">Культурная афиша</a>
                <a href="/projects" class="footer__link">Спецпроекты</a>
                <a href="/mentors" class="footer__link">Наставники</a>
            </li>

            <li class="footer__item">
                <img src="/img/svg/eagle-white.svg" alt="Министерство культуры" />
                <img src="/img/svg/eagle-color.svg" alt="Министерство просвящения" />

                <p class="footer__annotation">
                    <a href="/consent_processing" class="footer__annotation__item">
                        Согласие об обработке персональных данных
                    </a>
                    <span class="footer__annotation__item">
                         <?if (!empty($contactList['COPY']) && !empty($contactList['COPY']['VALUE'])):?>
                             <?=$contactList['COPY']['VALUE']?>
                         <?endif;?>
                    </span>
                </p>
            </li>
        </ul>
    </div>
</footer>


<div class="popup-bg send-popup-claim">
      <span class="overlay-background"></span>
      <div class="complaint-popup__sent">
        <i class="close material-icons">
          close
        </i>
        <h2>Спасибо за обращение!</h2>
        <p>Наши модераторы рассмотрят Вашу жалобу</p>
        <span class="close purple def-button">Понятно</span>
      </div>
    </div>
	
<div class="popup-bg deletepop">
<span class="overlay-background"></span>
          <div class="popup-delete--comment " style="z-index:10000;">
        <h2>Вы уверены, что хотите удалить сообщение?</h2>
        <div class="button-box">
          <span class="close">
            Нет
          </span>
          <span class="delete">
            Удалить
          </span>
        </div>
      </div>
    </div>


<div class="popup-bg popup-claim">
      <span class="overlay-background"></span>
      <div class="complaint-popup__content">
        <i class="close material-icons">
          close
        </i>
        <h2>Пожаловаться на сообщение</h2>
        <form action="/"> 
		  
		  <form name='formclaim' action="/">
      <p>Пожалуйста, напишите причину Вашей жалобы</p>
      <textarea name="messageclaim" placeholder="Введите сообщение..."></textarea>
	  <input type="hidden" name="" id="newidpost">
	  <input type="hidden" name="" id="newtype">
      <button type="button" class="purple" name="sendclaim">Отправить</button>
    </form>
      </div>
    </div>




<div class="popup-support popup-bg">
    <div class="popup-content">
        <span class="close"></span>
        <h3>Написать в поддержку</h3>
        <form action="/">
            <p>Почта</p>
            <input type="email" name="email" required placeholder="example@mail.ru" />
            <p>Сообщение</p>
            <textarea name="message" required placeholder="Введите сообщение..."></textarea>
            <div class="confirmation-component">
                <div class="input-row">
                    <input type="checkbox" id="confirmationThis" />
                    <label for="confirmationThis">Я согласен на обработку <a href="/consent_processing/"> персональных данных</a></label>
                    <span>* — поля, обязательные для заполнения</span>
                </div>
            </div>
            <button type="submit">Отправить</button>
        </form>
    </div>
</div>

<div class="poster-popup--details popup-bg">
</div>

<div class="send-message popup-bg">
    <div class="register-event--content">
        <span class="close"></span>
        <span class="title">
          Сообщение отправлено
        </span>
        <p>
            Спасибо за обратную связь!
            Мы ответим Вам на указанный адрес почты
        </p>
        <span class="button">
          Понятно
        </span>
    </div>
</div>

<div class="register-event popup-bg">
    <div class="register-event--content">
        <span class="close"></span>
        <span class="title">
          Спасибо! Вы успешно зарегестрировались на мероприятие
        </span>
        <p>
            Ожидайте подтверждение на Вашу почту
        </p>
        <span class="button">
          Понятно
        </span>
    </div>
</div>

<div class="buy-ticket popup-bg">
    <div class="register-event--content">
        <span class="close"></span>
        <span class="title">
          Регистрация на мероприятие
        </span>
        <p>
            Для посещения данного мероприятия, необходимо приобрести билет
        </p>
        <a href="javascript:void(0);" id="buy_ticket_btn" class="button">
            Купить билет
        </a>
    </div>
    <form id="buy_ticket_form"></form>
</div>
</body>
</html>