<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?
    global $APPLICATION;
    ?>

    <meta property="og:url" content="<?$APPLICATION->ShowProperty("og:url")?>" />
    <meta property="og:type" content="<?$APPLICATION->ShowProperty("og:type")?>" />
    <meta property="og:title" content="<?$APPLICATION->ShowProperty("og:title")?>" />
    <meta property="og:description" content="<?$APPLICATION->ShowProperty("og:description")?>" />
    <meta property="og:image" content="<?$APPLICATION->ShowProperty("og:image")?>" />


    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="<?$APPLICATION->ShowProperty("og:title")?>">
    <meta name="twitter:description" content="<?$APPLICATION->ShowProperty("og:description")?>">
    <meta name="twitter:image" content="<?$APPLICATION->ShowProperty("og:image")?>">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <?
    use Bitrix\Main\Page\Asset;


    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/fonts/iconfont/material-icons.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.bundle.css");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bundle.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/custom.js");

    ?>
    <?$APPLICATION->ShowHead()?>
    <link rel="icon" href="/img/favicon.ico" />
    <title><?Custom\Culture\CultureHelpers::showPageTitle();?></title>
    <script type="text/javascript" src="//vk.com/js/api/share.js?93" charset="windows-1251"></script>
</head>

<body>
<div id="fb-root"></div><script src="https://connect.facebook.net/en_US/sdk.js" nonce="RnfmF1R4"></script>
<div id="panel" style="z-index: 5007;"><?$APPLICATION->ShowPanel();?></div>
<?
$arUser = array();
if ($USER->IsAuthorized()) {
    $rsUser = CUser::GetByID($USER->GetID());
    $arUser = $rsUser->Fetch();
}
?>
<nav class="visually-impaired-navigation">
    <div class="vertical-wrap">
        <span class="visually-impaired-title">
          Размер шрифта
        </span>
        <ul class="visually-impaired-font">
            <li class="visually-impaired-font__item f-s-14">
                А
            </li>
            <li class="visually-impaired-font__item f-s-18">
                А
            </li>
            <li class="visually-impaired-font__item f-s-22">
                А
            </li>
        </ul>
    </div>

    <div class="vertical-wrap">
        <span class="visually-impaired-title">
          Цвет сайта
        </span>
        <ul class="visually-impaired-color">
            <li class="visually-impaired-color__item b-c-w" data-color="b-c-w">
                Ц
            </li>
            <li class="visually-impaired-color__item b-c-b" data-color="b-c-b">
                Ц
            </li>
            <li class="visually-impaired-color__item b-c-bl" data-color="b-c-bl">
                Ц
            </li>
            <li class="visually-impaired-color__item b-c-y" data-color="b-c-y">
                Ц
            </li>
            <li class="visually-impaired-color__item b-c-br" data-color="b-c-br">
                Ц
            </li>
        </ul>
    </div>

    <div class="vertical-wrap">
        <span class="visually-impaired-title">
          Изображения
        </span>
        <ul class="visually-impaired-image">
            <li class="visually-impaired-image__item on-images--visible is-active" data-image="">
                <i class="material-icons done__icon">done</i> Вкл.
            </li>
            <li class="visually-impaired-image__item off-images--visible">
                <i class="material-icons clear__icon">clear</i> Выкл.
            </li>
        </ul>
    </div>
</nav>

<header class="header">
    <nav class="navigation">
        <a href="/" class="logo-wrap">
            <img src="/img/svg/header-logo.svg" alt="Логотип культуры для школьников" class="logo-image" />
        </a>

        <ul class="main-menu">
            <li class="navigation__item">
                <a href="/news" class="navigation__link">Новости</a>
            </li>
            <li class="navigation__item">
                Культура
                <i class="material-icons keyboard_arrow_down__icon">keyboard_arrow_down</i>

                <ul class="sub-menu sub-menu--culture">
                    <li class="sub-menu__item">
                        <a href="/cultural-education" class="sub-menu__link">
                            <i class="material-icons school__icon">school</i>
                            Культурное образование
                        </a>
                    </li>
                    <li class="sub-menu__item">
                        <a href="/affiche" class="sub-menu__link">
                            <i class="material-icons school__icon">event</i>
                            Культурная афиша
                        </a>
                    </li>
                    <li class="sub-menu__item">
                        <a href="/html_files/my_russia.html" class="sub-menu__link">
                            <i class="material-icons school__icon">flag</i>
                            Моя Россия
                        </a>
                    </li>
                    <li class="sub-menu__item">
                        <a href="/projects" class="sub-menu__link">
                            <i class="material-icons">
                                account_balance
                            </i>
                            Спецпроекты
                        </a>
                    </li>
                </ul>
            </li>
            <li class="navigation__item">
                <a href="/mentors" class="navigation__link">Наставники</a>
            </li>
        </ul>

        <button class="visually-impaired-menu-button">
            <i class="material-icons school__icon">remove_red_eye</i>
            <p>Версия для слабовидящих</p>
        </button>

        <div class="search">
            <svg
                    class="search__icon"
                    width="15"
                    height="15"
                    viewBox="0 0 15 15"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
            >
                <path
                        d="M6.49615 12.5556C9.68491 12.5556 12.2699 9.96875 12.2699 6.77778C12.2699 3.5868 9.68491 1 6.49615 1C3.3074 1 0.722412 3.5868 0.722412 6.77778C0.722412 9.96875 3.3074 12.5556 6.49615 12.5556Z"
                        stroke="#fff"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                />
                <path d="M13.7135 14L10.574 10.8583" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" />
            </svg>
            <input type="text" class="search__input" placeholder="Поиск" />
            <!-- <i class="material-icons search__icon">search</i> -->
        </div>
        <?if ($USER->IsAuthorized()):?>
        <div class="icon-users--elements">
            <ul class="account">
                <li class="account__item">
                    <?=(!empty($arUser['LOGIN']))? $arUser['LOGIN'] : ''?>
                    <ul class="sub-account">
                        <li class="triangle"></li>
                        <li class="sub-account__item">
                            <a href="/personal/profile" class="sub-account__link">
                                <i class="material-icons face__icon">face</i> Личный кабинет
                            </a>
                        </li>
                        <li class="sub-account__item">
                            <a href="/users" class="sub-account__link">
                                <i class="material-icons face__icon">star_rate</i> Рейтинг пользователей
                            </a>
                        </li>
                        <li class="sub-account__item">
                            <a href="/personal/settings" class="sub-account__link">
                                <i class="material-icons settings__icon">settings</i> Настройки
                            </a>
                        </li>
                        <?if (in_array(7, $USER->GetUserGroupArray())):?>
                            <li class="sub-account__item">
                                <a href="/personal/admin/mentor_chat_list.php?" class="sub-account__link">
                                    <i class="material-icons settings__icon">settings</i> Ответы на вопросы
                                </a>
                            </li>
                            <li class="sub-account__item">
                                <a href="/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=2&type=articles&lang=ru&find_section_section=0&SECTION_ID=0&apply_filter=Y" class="sub-account__link">
                                    <i class="material-icons settings__icon">settings</i> Создание публикации
                                </a>
                            </li>
                        <?endif;?>
                        <li class="sub-account__item">
                            <a href="/logout" class="sub-account__link">
                                <i class="material-icons login__icon">keyboard_return</i> Выйти
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="account__item account__item--rate">
                    <i class="material-icons star_rate__icon">star_rate</i>
                    4,9
                </li>
            </ul>
            <?if (!empty($arUser['PERSONAL_PHOTO'])):?>
                <img class="account__image" width="40" src="<?=CFile::GetPath((int)$arUser['PERSONAL_PHOTO'])?>">
            <?else:?>
                <img class="account__image" src="/img/svg/account-default.svg">
            <?endif;?>
            <i class="material-icons keyboard_arrow_down__icon">keyboard_arrow_down</i>
        </div>
        <?endif;?>
        <i class="material-icons menu__burger-open__icon">menu</i>
        <?if (!$USER->IsAuthorized()):?>
            <div class="login">
                <a class="login__link" href="/login">
                    <img class="login__icon" src="/img/svg/auth-login-icon.svg" />
                    <p class="login__name" href="auth.html">Войти</p>
                </a>
            </div>
        <?endif;?>
        <div class="burger__navigation hidden">
            <img class="burger__logo" src="/img/svg/header-logo.svg" />

            <ul class="burger__list">
                <li class="burger__item">
                    <a href="/" class="burger__link">Главная</a>
                </li>
                <li class="burger__item">
                    <a href="/news" class="burger__link">Новости</a>
                </li>
                <li class="burger__item burger__dropdown">
                    Культура
                    <i class="material-icons keyboard_arrow_down__icon">keyboard_arrow_down</i>
                    <ul class="burger__submenu hidden">
                        <li class="burger__item">
                            <a href="/cultural-education" class="burger__link">
                                Культурное образование
                            </a>
                        </li>
                        <li class="burger__item">
                            <a href="/" class="burger__link">
                                Культурная карта
                            </a>
                        </li>
                        <li class="burger__item">
                            <a href="/affiche" class="burger__link">
                                Культурная афиша
                            </a>
                        </li>
                        <li class="burger__item">
                            <a href="/html_files/my_russia.html" class="burger__link">
                                Моя Россия
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="burger__item">
                    <a href="/mentors" class="burger__link">
                        Наставники
                    </a>
                </li>
            </ul>
            <i class="material-icons menu__burger-close__icon">close</i>
        </div>
    </nav>
</header>
