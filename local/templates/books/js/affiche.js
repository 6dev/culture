$( document ).ready(function() {

    $('body').on('click', '.affiche_item', function() {
        let id = $(this).attr('item_id');
        $.ajax({
            type: 'POST',
            url: '/affiche/ajax.php',
            dataType: "json",
            data: {ID: id, action: 'detail_popup'},
            success: (function (response) {
                if (response.hasOwnProperty('data') && response.data.hasOwnProperty('popup_html')) {
                    $(".poster-popup--details.popup-bg").html(response.data.popup_html);
                    $(".poster-popup--details.popup-bg").addClass('is-active');
                }
            }),
            error: (function (error) {
                console.log(error);
            })
        });
    });

    $('body').on('click', '.popup-bg .poster-popup--content .close', function() {
        $(".poster-popup--details.popup-bg").removeClass('is-active');
        $(".poster-popup--details.popup-bg").html("");
    });

    $('body').on('click', '#buy_ticket', function() {
        let id = parseInt($(this).attr('item_id'), 10);
        /*if (id > 0) {
            $("#buy_ticket_form").html('<input type="hidden" name="item_id" value="' + id + '">');
            $(".buy-ticket.popup-bg").addClass('is-active');
        }*/
    });

    $('body').on('click', '.buy-ticket.popup-bg .close', function() {
        $(".buy-ticket.popup-bg").removeClass('is-active');
        $("#buy_ticket_form").html("");
    });

    $('body').on('click', '#agreement_btn', function() {
        let id = parseInt($(this).attr('item_id'), 10);
        $.ajax({
            type: 'POST',
            url: '/affiche/ajax.php',
            dataType: "json",
            data: {ID: id, action: 'sign'},
            success: (function (response) {
                console.log(response);
                let status = false;
                if (response.hasOwnProperty('status') && (response.status == 'success')) {
                    status = true;
                }
                if (status) {

                    if (response.hasOwnProperty('data') && response.data.hasOwnProperty('PRICE')) {
                        if (parseInt(response.data.PRICE, 10) > 0) {
                            $("#buy_ticket_form").html('<input type="hidden" name="item_id" value="' + id + '">');
                            $(".buy-ticket.popup-bg").addClass('is-active');
                        }
                        else $(".register-event.popup-bg").addClass('is-active');
                    }
                    else $(".register-event.popup-bg").addClass('is-active');
                }
                else {
                    if (response.hasOwnProperty('message') && (response.message != '')) {
                        alert(response.message);
                    }
                    else alert('Ошибка запроса');
                }
            }),
            error: (function (error) {
                console.log(error);
                alert('Ошибка запроса');
            })
        });
    });

    $('body').on('click', '.popup-bg .register-event--content .button', function() {
        $(".register-event.popup-bg").removeClass('is-active');
    });

    $('body').on('click', '.popup-bg .register-event--content .close', function() {
        $(".register-event.popup-bg").removeClass('is-active');
    });


    $('body').on('click', '#buy_ticket_btn', function() {
        console.log($("#buy_ticket_form").serialize());
    });

});