$(document).ready(function() {

    $('.popup-support form').submit(function() {

        var form = $(this);

        if (form.find('#confirmationThis').is(':checked')) {
            $.ajax({
                type: "POST",
                cache: false,
                url: '/local/templates/books/ajax/form/support.php',
                data: form.serialize(),
                success: function (data) {
                    $('.popup-support').removeClass('is-active')
                    $('.send-message').addClass('is-active')

                    $(".popup-support form")[0].reset();
                }
            });
        }

        return false;

    });

    $('.send-message .button').click(function() {

        $('.send-message').removeClass('is-active')
        $('body').removeClass('no-scroll')

    });

    $("body").on("click", function (e) {
        let el = $(".search");
        if (!el.is(e.target) && el.has(e.target).length === 0) {
            if ($(".visually-impaired-menu-button").hasClass('is-active')) {
                $(".visually-impaired-menu-button").removeClass('is-active')
            }
            if ($(".search").hasClass('is-active')) {
                $(".search").removeClass('is-active')
            }
        }
    });

});