$( document ).ready(function() {
	
// burger menu handler
(function () {
    const burger = document.querySelector('.burger__navigation');
    const burgerOpen = document.querySelector('.menu__burger-open__icon');
    const burgerClose = document.querySelector('.menu__burger-close__icon');
    const burgerLinks = document.querySelectorAll('.burger__link');

    burgerOpen.addEventListener('click', () => {
        burger.classList.remove('hidden');
    })
    burgerClose.addEventListener('click', () => {
        burger.classList.add('hidden');
    });

    // if (window.innerWidth < 768) {
    for (let i = 0; i < burgerLinks.length; i += 1) {
        burgerLinks[i].addEventListener('click', () => {
            burger.classList.add('hidden');
        });
    };
}());

// burger open submenu
const burgerDropdown = document.querySelectorAll('.burger__dropdown');
const burgerSubmenu = document.querySelectorAll('.burger__submenu');
const arrowOpen = document.querySelectorAll('.burger__navigation .keyboard_arrow_down__icon');
const arrowClose = document.querySelectorAll('.burger__navigation .keyboard_arrow_up__icon');

for (let i = 0; i < burgerDropdown.length; i++) {
    burgerDropdown[i].addEventListener('click', function() {
        burgerDropdown[i].classList.toggle('opened');
        arrowOpen[i].classList.toggle('hidden');
        arrowClose[i].classList.toggle('hidden');
        burgerSubmenu[i].classList.toggle('hidden');
    });
}

// date mask  
function dateInputMask(element) {
    element.addEventListener('keypress', function(e) {
    if (e.keyCode < 47 || e.keyCode > 57) {
      e.preventDefault();
    }
    
    var valueLength = element.value.length;
    
    // If we're at a particular place, let the user type the slash
    // i.e., 12/12/1212
    if(valueLength !== 1 || valueLength !== 3) {
      if(e.keyCode == 47) {
        e.preventDefault();
      }
    }
    
    // If they don't add the slash, do it for them...
    if(valueLength === 2) {
        element.value += '.';
    }

    // If they don't add the slash, do it for them...
    if(valueLength === 5) {
        element.value += '.';
    }
  });
};
  
dateInputMask(document.querySelector('.input__birthday'));

// tab interests click interest
const interests = document.querySelectorAll('.interest__item');

interests.forEach(interest => 
    interest.addEventListener('click', () => {
        interest.classList.toggle('active');
    })
)

// signup tabs next-previous steps
const tabs = document.querySelectorAll('.signup__tab');
const buttonNext = document.querySelectorAll('.signup__next');
const buttonBack = document.querySelectorAll('.signup__back');
let currentTab = 0;

buttonNext.forEach(button => 
    button.addEventListener('click', () => {
        tabs[currentTab].classList.add('hidden');
        currentTab += 1;
        tabs[currentTab].classList.remove('hidden');
    })
)

buttonBack.forEach(button => 
    button.addEventListener('click', () => {
        tabs[currentTab].classList.add('hidden');
        currentTab -= 1;
        tabs[currentTab].classList.remove('hidden');
    })
)

});